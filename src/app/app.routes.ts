import { Routes } from '@angular/router';

export const routes: Routes = [
    {
      path: 'login',
      loadComponent: () => import('./pages/login/login.component').then(m => m.LoginComponent)
    },
    {
        path: 'app',
        loadChildren: () => import('./pages/main/main.module').then(m => m.MainModule),  
      },
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      }
  ];