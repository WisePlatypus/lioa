/***
 ** ****************************
 **        JS ODITIES
 ** ****************************
 *
 * This file is here to help with things that are javascript related
 * 
 * Anytime there are potentially a useful exotic js manipulation (objects, event loop ....) not specific enough  or too specific to be in it's own helper category file it will be here
 * Rejected stackoverflow functions, exotics Data Types you are welcome as you are, the more of you there are the more this requiem of bad code is gonna be powerfull.
 * 
 * 
 * Messages to developper that dare to adventure themself to these wild territories. Be prepared, one does simply walk into js-oddities.js...
 * You are gonna face hope, despair and confusion. You have been warned
 */

import { FormGroup } from "@angular/forms";




export function setValueFormGroup(value: Object, group: FormGroup)
{
  for (let [k, v] of Object.entries(value))
  {
    const control = group.controls[k];

    if (control && !('controls' in control))
    {
      control.setValue(v)
    }
    else if (control && ('controls' in control) && typeof v === 'object')
    {
      setValueFormGroup(v, <FormGroup>control)
    }
  }
}



/**
* return a nested value with a given path of key array
* @param obj object
* @param path nested path
* @returns any
*/
export function getValueWithPath(obj: any, path: string[]): any
{
  let value = obj;

  for (const key of path)
  {
    value = value[key]
    if (value === undefined)
    {
      break;
    }
  }

  return value;
}



/**
 * return a deep cloned copy of object
 * Deep cloned means that sub object will be cloned recusively
 * Supports Object Array and basic type (any data strucure), Will not work with classes
 * @param obj 
 * @returns 
 */
export function cloneObject<T = any>(obj): T 
{
  let clone;
  if (obj !== null && obj !== undefined && typeof obj === 'object')
  {
    if (Array.isArray(obj))
    {
      clone = [];
      for (const el of obj)
      {
        clone.push(cloneObject(el))
      }
    }
    else if (obj instanceof Date)
    {
      clone = new Date(obj);
    }
    else
    {
      clone = {}
      for (const [key, value] of Object.entries(obj))
      {
        clone[key] = cloneObject(value);
      }
    }
  }
  else
  {
    clone = obj;
  }

  return <T>clone;
}


/**
 ** Merge two objects and theirs elements 
 * in case of conflict o2 crush o1
 * @param o1 
 * @param o2 
 * @returns 
 */
export function mergeObject(o1: object, o2: object): any
{
  // define merge object
  let mo = { ...o1 };

  // for each entries of o2
  for (const [key, value] of Object.entries(o2))
  {
    //- if entry exists in merged object
    if (mo[key] !== undefined)
    {
      // if both etries are objects
      if (typeof value === 'object' && typeof mo[key] === 'object')
      {
        // if they are arrays
        if (Array.isArray(mo[key]) && Array.isArray(value))
        {
          // concat arrays
          mo[key] = [...mo[key], ...value]
        }
        // if both entry are not array and objects, merge them
        else if (!Array.isArray(mo[key]) && !Array.isArray(value))
        {
          mo[key] = mergeObject(mo[key], value);
        }
        // if they are different crush mo entry
        else
        {
          mo[key] = value;
        }
      }
      // if not both obj crush mo entry
      else
      {
        mo[key] = value;
      }
    }
    // else add entry
    else
    {
      mo[key] = value;
    }
  }

  return mo;
}



/**
 * Compare value and structure of objects
 * @param o1 
 * @param o2 
 * @returns 
 */
export function deepCompare(o1: any, o2: any): boolean 
{
  if (o1 !== null && o1 !== undefined && typeof o1 === 'object' && o2 !== null && o2 !== undefined && typeof o2 === 'object')
  {
    if (Array.isArray(o1))
    {
      if (!Array.isArray(o2) || o1.length !== o2.length)
      {
        return false;
      }

      for (let i = 0; i < o1.length; i++)
      {
        if (!deepCompare(o1[i], o2[i]))
        {
          return false;
        }
      }
    }
    else
    {
      const keys1 = Object.keys(o1);
      if (keys1.length !== Object.keys(o2).length)
      {
        return false;
      }

      for (const key of keys1)
      {
        if (!deepCompare(o1[key], o2[key]))
        {
          return false;
        }
      }
    }
  }
  else
  {
    return o1 === o2;
  }
  return true;
}

/**
 ** Compare if two array contains the same elements regardless of orders
 * @param a1 
 * @param a2 
 */
export function deepElementCompare(a1: any[], a2: any[])
{
  const a = [...a1];
  const b = [...a2];

  if (a.length === 0 && b.length !== 0)
  {
    return false;
  }

  while (a.length !== 0)
  {
    let match = false;

    for (const [i, bel] of b.entries())
    {
      if (deepCompare(a[a.length - 1], bel))
      {
        match = true;
        b.splice(i, 1);
        break;
      }
    }

    if (match)
    {
      a.pop();
    }
    else
    {
      return false;
    }
  }

  return true;
}



/**
 * Compare value and structure of objects if o2 has everything o1 has return true 
 * (o2 can have more elements)
 * @param o1 partial element
 * @param o2 full element
 * @returns 
 */
export function partialDeepCompare(o1: any, o2: any): boolean 
{
  if (o1 !== null && o1 !== undefined && typeof o1 === 'object' && o2 !== null && o2 !== undefined && typeof o2 === 'object')
  {
    if (Array.isArray(o1))
    {
      if (!Array.isArray(o2) || o1.length > o2.length)
      {
        return false;
      }

      for (let i = 0; i < o1.length; i++)
      {
        if (!deepCompare(o1[i], o2[i]))
        {
          return false;
        }
      }
    }
    else
    {
      const keys1 = Object.keys(o1);

      for (const key of keys1)
      {
        if (!deepCompare(o1[key], o2[key]))
        {
          return false;
        }
      }
    }
  }
  else
  {
    return o1 === o2;
  }
  return true;
}



/**
 * return a deep cloned copy of object following th structure of an object reference
 * Deep cloned means that sub object will be cloned recusively (only in accordance to ref)
 * Supports Object Array and basic type (any data strucure), Will not work with classes
 * 
 * In arrays follows the strucure of the first element of the array
 * 
 * @param ref 
 * @returns 
 */
export function cloneObjectWithRef(obj, ref, config: { defaultValue?: boolean; ignoreArrays?: boolean, keepRefValue?: boolean, keyTrans?: (key: string) => string } = {}) 
{
  const { defaultValue = false, ignoreArrays = false, keyTrans = undefined, keepRefValue = false } = config;

  let clone;

  if (ref !== null && ref !== undefined && typeof ref === 'object')
  {
    if (Array.isArray(ref) && Array.isArray(obj))
    {
      if (!ignoreArrays)
      {
        clone = [];
        for (const el of obj)
        {
          clone.push(cloneObjectWithRef(el, ref[0], config))
        }
      }
      else
      {
        clone = obj;
      }
    }
    else
    {
      clone = {}
      for (const key of Object.keys(ref))
      {
        clone[key] =
          cloneObjectWithRef(
            obj ? obj[keyTrans ? keyTrans(key) : key] : undefined,
            ref[key],
            config);
      }
    }
  }
  else
  {
    if (defaultValue && obj === undefined)
    {
      clone = getDefaultTypeValueOf(ref);
    }
    else if (obj === undefined)
    {
      clone = ref;
    }
    else
    {
      clone = obj;
    }
  }

  return clone;
}





/**
 * take an object and past the key and value of another object in it, crushing/replacing the value of the crush object in case of conflict
 * 
 * Everything that is not an object will be crushed.
 * 
 * we suggest to work with c
 * 
 * @param obj 
 * @param crush 
 * @returns 
 */
export function crushObject(obj: object, crush: object, clean: boolean = false)
{
  if (clean)
  {
    deleteProperties(obj)
  }
  if (crush !== null && crush !== undefined && typeof crush === 'object' && obj !== null && obj !== undefined && typeof obj === 'object' && !Array.isArray(crush) && !Array.isArray(obj))
  {
    for (const key of Object.keys(crush))
    {
      obj[key] = crushObject(obj[key], crush[key]);
    }
  }
  else
  {
    obj = crush;
  }

  return obj;
}



/**
 * take an object and past the key and value of another object in it, crushing/replacing the value of the crush object in case of conflict
 * 
 * Everything that is not an object will be crushed.
 * 
 * we suggest to work with c
 * 
 * @param obj 
 * @param crush 
 * @returns 
 */
export function CopyWithRefTransf(obj: any, crush: any, tran: (key) => string)
{
  if (crush !== null && crush !== undefined && typeof crush === 'object' && obj !== null && obj !== undefined && typeof obj === 'object')
  {
    for (const key of Object.keys(obj))
    {
      if (tran(key) in crush)
      {

        obj[key] = crushObject(obj[key], crush[tran(key)]);
      }
    }
  }
  else
  {
    obj = crush;
  }

  return obj;
}



/**
 * return a valid 
 * @param st 
 * @returns 
 */
export function getDefaultTypeValueOf(st)
{
  switch (typeof st)
  {
    case 'number':
      return 0;
    case 'string':
      return '';
    case 'boolean':
      return false;
    case 'object':
      if (Array.isArray(st))
      {
        return [];
      }
      else
      {
        return {};
      }
    case 'function':
      return () => { };
  }
}



/**
 * Skip a number of event loop. To postpone an operation.
 * 
 * Use Case a ressource need to skip X event loop before initialisation (something you see often with angular change detector)
 * 
 * You know this ressource will be ready in X tick and you cannot access it's "event code" (the code when you actually assign the value)
 * 
 * you can skip X + 1 event loop and the ressource will be initialised
 * 
 * @param fun function / code to execute after skip tick
 * @param skip the number of tick to wait before executeing fun
 */
export function skipEventLoops(fun: Function, skip: number = 1)
{
  if (skip === 0)
  {
    fun();
  }
  else if (skip > 0)
  {
    setTimeout(() =>
    {
      skipEventLoops(fun, skip - 1);
    });
  }
}



/**
 * return a hash unique to the values specified of an object
 * @param obj 
 * @param keyPaths paths leading to a value in an object
 * @returns 
 */
export function hashObjectValue(obj: any, keyPaths: string[][]): string
{
  let hash = '';

  for (const keyPath of keyPaths)
  {
    hash = `${hash} / ${encodeURIComponent(JSON.stringify(getValueWithPath(obj, keyPath)))}`
  }

  return hash;
}


/**
 * For some reason I need this
 * don't ask why
 * you don't to know why
*. I didn't ask why
*. I would not have wanted to know why
 * But yet here we are
 * @param obj 
 * @returns 
 */
export function undefinedIsString(obj)
{
  return mapObjectProp(obj, v => { return v === undefined ? "" : v });
}


/**
 ** apply a function on each key of an object that are not an object
 * Goes in sub object recusively
 * @param obj 
 * @param fu 
 * @returns 
 */
export function mapObject(obj: any, fu: Function)
{
  if (obj !== undefined && typeof obj === 'object')
  {
    for (const key of Object.keys(obj))
    {
      obj[key] = mapObject(obj[key], fu);
    }

    fu(obj);
  }

  return obj;
}

/**
 ** apply a function on each key of an object that are not an object
 * Goes in sub object recusively
 * @param obj 
 * @param fu 
 * @returns 
 */
export function mapObjectProp(obj: any, fu: (obj: any, key?: string) => any, key = undefined)
{
  if (obj !== undefined && typeof obj === 'object')
  {
    for (const key of Object.keys(obj))
    {
      obj[key] = mapObjectProp(obj[key], fu, key);
    }
  }
  else
  {
    obj = fu(obj, key);
  }

  return obj;
}


/**
 ** apply a function on each key of an object that are not an object
 * Goes in sub object recusively
 * @param obj 
 * @param fu 
 * @returns 
 */
export async function asyncMapObjectProp(obj: any, fu: (obj: any, key?: string, parent?: string, parentkey?: string) => Promise<any>, key = undefined, parent = undefined, parentkey = undefined)
{
  if (obj !== undefined && typeof obj === 'object')
  {
    for (const ckey of Object.keys(obj))
    {
      obj[ckey] = await asyncMapObjectProp(obj[ckey], fu, ckey, obj, key);
    }
  }
  else
  {
    obj = await fu(obj, key, parent, parentkey);
  }

  return obj;
}



export function transformKeys(obj: any, condition: (key: string) => boolean, transformName: (key: string) => string)
{
  mapObject(obj, sobj =>
  {
    for (const [key, value] of Object.entries(sobj))
    {
      if (condition(key))
      {
        sobj[transformName(key)] = value;
        delete (sobj[key]);
      }
    }
  });

  return obj
}




/**
 ** Extract number out of a string that would have had (pre/su)ffixes
 * Exemple '53.23px' string will return 53.23 in number
 * Should not work with serverals numbers
 * @param str
 * @returns 
 */
export function extractNumber(str: string): number
{
  // @ts-ignore for some reason replaceAll (compatible with everything but "IE" and "Samsung internet" mobile) is not repertoried
  return Number(str.replaceAll(/[^\.0-9]*/g, ''));
}



/**
 ** return a promise of nothing after timout.
 * Makes it easy to wait synced time to test event related f4eature
 * 
 * @param timout 
 * @returns 
 */
export function timeoutPromise(timout: number, log: string = undefined)
{
  return new Promise<void>((resolve, reject) =>
  {
    setTimeout(() =>
    {

      if (log !== undefined)
      {
        console.log(log);
      }

      resolve();
    }, timout);
  })
}



/**
 ** Add pointless 0 if necessary to a number in string
 * @param nbformat 
 * @param nb 
 * @param toFixed 
 * @returns 
 */
export function pointless0(nbformat: number, nb: number, toFixed = 0): string
{
  return nb < nbformat && nbformat > 1 ? `0${pointless0(nbformat / 10, nb, toFixed)}` : nb.toFixed(toFixed);
}



/**
 ** used to unnest an object. 
 * The key are constructed with the keyOf the nested object concatened with its sub keys.
 * Mainly used on local template to transorm data that are logically not ordered in a way that is useful for template ergonomy and logic
 * @param obj 
 * @param key 
 * @param sep 
 */
export function unnest(obj: any, key: string, sep: string = '.')
{
  if (key in obj && typeof obj[key] === 'object')
  {
    const nested = obj[key];

    delete (obj[key]);

    for (const [nkey, value] of Object.entries(nested))
    {
      obj[`${key}${sep}${nkey}`] = value;
    }
  }
}


/**
 ** used to nest key that follow a certain logic or have been unnested with nest
 * @param obj 
 * @param unkey 
 * @param sep 
 */
export function nest(obj: any, unkey: string, sep: string = '.')
{
  // create nest object if not aldrady there or replace current if current is not an object
  if (obj[unkey] === undefined || typeof obj[unkey] !== 'object')
  {
    obj[unkey] = {};
  }

  for (const [key, value] of Object.entries(obj))
  {
    const split = key.split(sep)
    if (split.length > 1 && split[0] === unkey)
    {
      split.shift();
      obj[unkey][split.join(sep)] = value;

      delete (obj[key]);
    }
  }
}







/**
 * https://stackoverflow.com/questions/14934089/convert-iso-8601-duration-with-javascript/29153059
 */
const iso8601DurationRegex = /(-)?P(?:([.,\d]+)Y)?(?:([.,\d]+)M)?(?:([.,\d]+)W)?(?:([.,\d]+)D)?T(?:([.,\d]+)H)?(?:([.,\d]+)M)?(?:([.,\d]+)S)?/;


export function parseISO8601Duration(iso8601Duration: string): ISO8601Duration
{
  var matches = iso8601Duration.match(iso8601DurationRegex);

  if (matches !== null) 
  {
    return {
      sign: matches[1] === undefined ? '+' : '-',
      years: matches[2] === undefined ? 0 : Number(matches[2]),
      months: matches[3] === undefined ? 0 : Number(matches[3]),
      weeks: matches[4] === undefined ? 0 : Number(matches[4]),
      days: matches[5] === undefined ? 0 : Number(matches[5]),
      hours: matches[6] === undefined ? 0 : Number(matches[6]),
      minutes: matches[7] === undefined ? 0 : Number(matches[7]),
      seconds: matches[8] === undefined ? 0 : Number(matches[8])
    };
  }
  else
  {
    return undefined;
  }

};

// export function ISO8601DurationToString

// interface duration for decoded ISO8601Duration
export interface ISO8601Duration
{
  sign: string,
  years: number,
  months: number,
  weeks: number,
  days: number,
  hours: number,
  minutes: number,
  seconds: number
}

/**
 ** fetch a fields in an object given a path pointing to it
 * @param obj 
 * @param path 
 * @param separator 
 * @returns 
 */
function fetchFields(obj: any, path: string | string[], separator: string = '.', option: { stackArray?: boolean } = {}, stacked = [], nest = false): any | any[]
{
  const {
    stackArray = false
  } = option;

  if (obj !== undefined)
  {
    //i split string if not an array
    if (typeof path === 'string') path = path.split(separator);

    //- if stack array and obj is an array
    if (stackArray && Array.isArray(obj))
    {
      // manage each els
      for (const el of obj)
      {
        // fetch field
        const field = fetchFields(el, path, separator, option, stacked, true)

        // if there is a field push it into the array
        if (field !== undefined)
        {
          stacked.push(field)
        }

        // if we are not into a nested array (nested into an array) return it
        if (nest === false)
        {
          return stacked
        }
      }
    }
    else
    {
      // if an array take the first el
      if (Array.isArray(path))
      {
        if (path.length === 1)
        {
          return obj[path[0]];
        }
        else
        {
          const lp = path.shift();

          return fetchFields(obj[lp], path, separator, option, stacked, nest)
        }
      }
    }
  }
  return undefined;
}

// export function fetchFields(obj: any, path: string | string[], separator: string = '.', option: {stackArray?: boolean} = {}): any | any[]
// {
//   if(obj !== undefined)
//   {
//     if(typeof path === 'string')
//     {
//       path = path.split(separator);
//     }
//     if(option.stackArray && Array.isArray(obj))
//     {
//       return obj.map(el => fetchFields(el, path, separator, option))
//     }
//     else
//     {
//       if(Array.isArray(path))
//       {
//         if(path.length === 1)
//         {
//           return obj[path[0]];
//         }
//         else
//         {
//           const lp = path.shift();

//           return fetchFields(obj[lp], path, separator, option)
//         }
//       }
//     }
//   }
//   return undefined;
// }



/**
 ** Fetch fields with several paths
 * value will be returned in an array
 * array will be empty if no fields are founds and value duplication will be removed
 * @param obj 
 * @param paths 
 * @param separator 
 * @returns 
 */
export function fetchFieldsWithMP(obj, paths: string | string[], separator: string = '.', option: { stackArray?: boolean } = {}): any[]
{
  const datas = [];

  if (typeof paths === 'string')
  {
    paths = [paths];
  }

  for (const path of paths)
  {
    const data = fetchFields(obj, path, separator, option);

    // console.log(data)

    if (data !== undefined)
    {
      if (option.stackArray && Array.isArray(data))
      {
        for (const el of data)
        {
          if (el !== undefined && !inArray(datas, el))
          {
            datas.push(el);
          }
        }
      }
      else
      {
        if (!inArray(datas, data))
        {
          datas.push(data);
        }
      }
    }
    // else
    // {

    //   datas.push(' ');
    // }
  }

  return datas;
}


/**
 ** Quick transform list of string in hmap with an predicate function to assign a boolean value to an element
 * @param list 
 * @param checkFn 
 * @returns 
 */
export function stringListAsHmap(list: (string | number)[], checkFn?: (string) => boolean)
{
  const hmap: { [key: string]: boolean } = {};

  for (const el of list)
  {
    hmap[el] = checkFn ? checkFn(el) : true;
  }

  return hmap;
}


/**
 ** Transform an array into a hmap given the path of the key and optionnally the value
 * @param array 
 * @param hkey 
 * @param vkey 
 */
export function asHMap<T = Object>(array: T[], hkey: string = undefined, vkey: string = undefined): { [key: string]: T }
{
  const hmap: { [key: string]: any } = {};

  for (const el of array)
  {
    switch (typeof el)
    {
      case 'string':
      case 'number':
        hmap[el] = true;
        break;
      case 'object':
        if (el instanceof Date)
        {
          hmap[el.toUTCString()] = true;
        }
        else
        {
          hmap[el[hkey]] = vkey !== undefined ? el[vkey] : el;
        }
        break;
    }
  }

  return hmap;
}


/**
 ** transform hmap in array
 * @param hmap 
 * @param keyKey 
 * @param valueKey 
 */
export function hMapAsArray<T = any>(hmap: { [key: string]: T }, keyKey: string = undefined, valueKey: string = undefined): T[]
{
  const array: any[] = [];

  for (const [key, value] of Object.entries(hmap))
  {
    if (keyKey !== undefined)
    {
      if (valueKey !== undefined)
      {
        const val = {};

        val[valueKey] = value;

        val[keyKey] = key;

        array.push(val)
      }
      else
      {
        value[keyKey] = key;

        array.push(value);
      }

    }
    else
    {
      array.push(value);
    }
  }

  return array;
}


/**
 ** return  a data as a date object
 * @param date
 * @returns 
 */
export function asDate(date: Date | number | string) 
{
  switch (typeof date)
  {
    case 'object':
      return date instanceof Date ? date : undefined;
    case 'number':
    case 'string':
      return new Date(date);
    default:
      break;
  }
  return undefined
}


/**
 ** Use when something should be a number but sometime isn't
 */
export function asNumber(nb: any): number
{
  switch (typeof nb)
  {
    case 'number':
      return nb;
    case 'string':
      return extractNumber(nb);
    case 'object':
      if (nb instanceof Date)
      {
        return nb.getTime();
      }
  }

  return NaN;
}


/**
 ** return an array out of anything
 * @param thing 
 * @returns 
 */
export function asArray(thing)
{
  switch (typeof thing)
  {
    case 'object':
      if (Array.isArray(thing))
      {
        return thing;
      }
      else
      {
        return [thing];
      }
    case 'undefined':
      return [];
    default:
      return [thing];
  }
}


/**
 ** return data as timestamp
 * @param timestamp 
 */
export function asTimestamp(timestamp: Date | number | string)
{
  switch (typeof timestamp)
  {
    case 'object':
      return timestamp instanceof Date ? timestamp.getTime() : undefined;
    case 'number':
      return timestamp;
    case 'string':
      return new Date(timestamp).getTime();
  }
}


/**
 ** return if an element is found in an array
 * @param array 
 * @param obj 
 * @returns 
 */
export function inArray(array, obj)
{
  for (const el of array)
  {
    if (deepCompare(el, obj))
    {
      return true;
    }
  }
  return false;
}


/**
 ** deleta all properties of an object
 * @param obj 
 */
export function deleteProperties(obj)
{
  const keys = Object.keys(obj);

  for (const key of keys)
  {
    delete (obj[key])
  }
}


/**
 ** Return if date is valid
 * @param d 
 * @returns 
 */
export function isDateValid(d: Date)
{
  //@ts-ignore
  return (d instanceof Date && !isNaN(d));
}



/**
 ** count the nummber of element that match the given function
 * @param array 
 * @param fun 
 * @returns 
 */
export function arrayCount(array: any[], fun: (element: any) => boolean)
{
  let count = 0;

  for (const element of array)
  {
    if (fun(element))
    {
      count++;
    }
  }
  return count;
}

/**
 ** Return an array with only validated elements
 *
 * @param array 
 * @param validationFn 
 * @returns 
 */
export function trimArray<T = any>(array: T[], validationFn: (T) => boolean): T[]
{
  // output array
  const oa: T[] = [];

  for (const el of array)
  {
    if (validationFn(el))
    {
      oa.push(el);
    }
  }

  return oa;
}


/// https://stackoverflow.com/questions/14573223/set-cookie-and-get-cookie-with-javascript
/// https://www.quirksmode.org/js/cookies.html

export function setCookie(name, value, days) 
{
  var expires = "";
  if (days) 
  {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

export function getCookie(name) 
{
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');

  for (var i = 0; i < ca.length; i++) 
  {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }

  return null;
}

export function eraseCookie(name) 
{
  document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}


/**
 ** Return if all the value (of same index) inside the date array are equals 
 ** Used to test if date range have changed
 */
export function dateArrEq(d1: Date[], d2: Date[])
{
  return d1.every((d, i) => dateEq(d, d2[i]));
}

/**
 ** Return if two date object have the same time
 * 
 * @param d1 date 1
 * @param d2 date 2
 * @returns are the date object equals
 */
export function dateEq(d1: Date, d2: Date)
{
  return d1.getTime() === d2.getTime();
}


/**
 ** Promise that will return error after x ms
 ** useful when we need to be sure to clean up after request
 */
export async function timeOutPromise(p: Promise<any>, time: number)
{
  return await Promise.race([wait(time), p]);
}

function wait(ms: number) 
{
  return new Promise((_, reject) =>
  {
    setTimeout(() => reject(new Error('timeout')), ms);
  });
}

/**
 ** return a promise that resolve after the next animation frame
 */
export function requestAnimationFramePromise(): Promise<void>
{
  return new Promise(res =>
  {
    requestAnimationFrame(() =>
    {
      res();
    });
  });
}



// export function temporaryCookie