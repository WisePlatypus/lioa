import { ApplicationConfig, provideZoneChangeDetection } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getAuth, provideAuth } from '@angular/fire/auth';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { getFunctions, provideFunctions } from '@angular/fire/functions';
import { getStorage, provideStorage } from '@angular/fire/storage';

export const appConfig: ApplicationConfig = {
  providers: [
    provideZoneChangeDetection({ eventCoalescing: true }), 
    provideRouter(routes), 
    provideAnimationsAsync(), 
    provideFirebaseApp(() => initializeApp(
      {
        "projectId":"mini-erp-les-graines-en-joie",
        "appId":"1:338652880636:web:06cf86bcc11fb085594179",
        "storageBucket":"mini-erp-les-graines-en-joie.appspot.com",
        "apiKey":"AIzaSyBLjpHKPHThO9kMQButvDQQUlX1YYmenx8",
        "authDomain":"mini-erp-les-graines-en-joie.firebaseapp.com",
        "messagingSenderId":"338652880636",
        "measurementId":"G-GRWJEVXGZF"})), 
        provideAuth(() => getAuth()), 
        provideFirestore(() => getFirestore()), 
        provideFunctions(() => getFunctions()), 
        provideStorage(() => getStorage()), 
        provideAnimationsAsync(), 
        provideAnimationsAsync()
    ]
};
