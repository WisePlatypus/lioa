import { Component, Input, inject, signal } from '@angular/core';
import { Client, ClientService } from '../../forms/services/client.service';
import { MatCard, MatCardContent } from '@angular/material/card';
import { MatFormField, MatLabel } from '@angular/material/form-field';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { MatInput } from '@angular/material/input';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { merge } from 'rxjs';
import { CommonModule } from '@angular/common';
import { MatRadioModule } from '@angular/material/radio';
import { cloneObject, cloneObjectWithRef, setValueFormGroup } from '../../util/js-oddities';
import { MatButton } from '@angular/material/button';

@Component({
  selector: 'lioa-client-form',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    CommonModule,
    MatCard,
    MatCardContent,
    MatFormField,
    MatInput,
    MatLabel,
    MatSlideToggle,
    MatRadioModule,
    MatButton,
  ],
  templateUrl: './client-form.component.html',
  styleUrl: './client-form.component.scss'
})
export class ClientFormComponent
{
  @Input() set client(client: Client)
  {
    this._client = cloneObject(client);

    this._id = client._id;

    setValueFormGroup(this.client, this.form);
  }

  get client(): Client
  {
    return this._client;
  }

  _id: string;


  private _client: Client = {
    adresse: {
      code: 0,
      rue: '',
      ville: '',
    },
    email: '',
    nom: '',
    prenom: '',
    type: 'business',
    telephone: '',
    _id: undefined,
  };

  public form: FormGroup = new FormGroup(
    {
      nom: new FormControl(''),
      prenom: new FormControl(''),
      email: new FormControl(''),
      type: new FormControl('business'),
      telephone: new FormControl(''),
      adresse: new FormGroup(
        {
          code: new FormControl(0),
          rue: new FormControl(''),
          ville: new FormControl(''),
        }
      )
    });

  private clients = inject(ClientService);

  ngOnInit(): void
  {
    this.form.valueChanges.subscribe(value =>
    {
      console.log(value);
    }
    )

  }

  submit()
  {
    const client = this.form.value;

    if (this._id)
    {
      client._id = this._id;
      this.clients.editClient(client)
    }
    else
    {
      this.clients.createClient(client);
    }

  }

}
