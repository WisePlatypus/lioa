import { Component, inject } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { environment } from '../environment/environment';
import { Auth, User, authState } from '@angular/fire/auth'
import { Subscription } from 'rxjs';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent 
{
  title = 'lioa';

  private location = inject(Location);

  private router = inject(Router);

  private auth = inject(Auth);
  authState$ = authState(this.auth);
  authStateSubscription: Subscription | undefined;

  ngOnInit(): void 
  {
    this.authStateSubscription = this.authState$.subscribe((aUser: User | null) => 
    {
      console.log(aUser);
      if(aUser === null)
      {
        if(this.location.path() !== '/login')
        {
          this.router.navigateByUrl('/login');
        }
      }
      else
      {
        if(this.location.path() !== '/app')
        {
          this.router.navigateByUrl('/app');
        }
      }
    })
  }
}
