import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { asTimestamp } from '@nororljos/nrs-util';
import { TDateRangeValidator } from '../models/template';
import { testRange } from './number.directive';

@Directive({
  selector: '[validatorDateRange]',
  providers: [{provide: NG_VALIDATORS, useExisting: DateRangeDirective, multi: true}]
})
export class DateRangeDirective implements Validator
{
  
  @Input('validatorDateRange') validator: TDateRangeValidator;

  constructor() { }

  validate(control: AbstractControl): ValidationErrors | null 
  {
    return this.validator ? validatorDateRange(this.validator)(control) : null;
  }  

}


export function validatorDateRange(dateValidator: TDateRangeValidator): ValidatorFn 
{
  return (control: AbstractControl): {[key: string]: any} | null => 
  {
    // return validity
    return validateDateRange(dateValidator, control.value) ? null : {nbValidator: {value: control.value, validator: dateValidator}};
  };
}


export function validateDateRange(dateValidator: TDateRangeValidator, value: Date[])
{
  let valid = true;
  
  if(value !== undefined && Array.isArray(value) && value[0] !== undefined && value[1] !== undefined)
  {
    // if there are ranges
    if(dateValidator.dateSets)
    {
      // valid  is a logic AND between current validity and range validity
      valid = valid && testRange(dateValidator.dateSets, value.map(date => asTimestamp(date)));
    }
  }
  else
  {
    return false;
  }

  return valid;
}