/* eslint-disable */

import { Directive, Input } from '@angular/core';
import { AbstractControl, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { ipTabToIpNb, isIpv4, isIpv6, stringIpToTabs } from '@nororljos/nrs-util';
import { TIpValidator } from '../models/template';
import { testNumber } from './number.directive';

@Directive({
  selector: '[validatorIp]'
})
export class IpDirective implements Validator 
{

  @Input('validatorIp') validator: TIpValidator;

  constructor() { }
  validate(control: AbstractControl): ValidationErrors {
    return this.validator !== undefined ? validatorIp(this.validator)(control) : null;
  }

}



export function validatorIp(ipValidator: TIpValidator): ValidatorFn 
{
  return (control: AbstractControl): {[key: string]: any} | null => 
  {
    return validateIp(ipValidator, control.value) ? null : {nbValidator: {value: control.value, validator: ipValidator}};
  };
}


export function validateIp(ipValidator: TIpValidator, ip: string)
{
  let ipNumber: bigint | number;
  let _isIpv4 = isIpv4(ip);
  let nbRanges: {min: bigint | number, max: bigint | number, inside?: boolean}[] = [];

  try
  {
    if(!(_isIpv4 || isIpv6(ip)))
    {
      throw new Error(`String given wasn't on supported format IPV4 or IPV6`);
    }
  
    ipNumber = ipTabToIpNb(stringIpToTabs(ip, _isIpv4), _isIpv4);
  }
  catch(e)
  {
    return false;
  }

  let valid = true;
  // Handle Type validation (can actually add ranges)
  if(ipValidator?.type)
  {
    if(_isIpv4)
    {
      switch (ipValidator?.type) 
      {
        // case 'any':
          
          // break;
        case 'private':
          // 10.0.0.0 = 167772160
          // 10.255.255.255 = 184549375
          nbRanges.push({min: 167772160n, max: 184549375n});

          // 172.16.0.0 = 2886729728
          // 172.31.255.255 = 2887778303
          nbRanges.push({min: 2886729728n, max: 2887778303n});
          
          // 192.168.0.0 = 3232235520
          // 192.168.255.255 = 3232301055
          nbRanges.push({min: 3232235520n, max: 3232301055n});
          break;
        case 'multicast':
          // 224.0.0.0 = 3758096384
          // 239.255.255.255 = 4026531839
          nbRanges.push({min: 3758096384n, max: 4026531839n});
          break;
        case 'unicast':
          // include classes A, B, C 0.0.0.0
          // 127.0.0.1 = 2130706433
          // 1.0.0.0 = 16777216
          nbRanges.push({min: 0n, max: 3758096383n});
          
          break;
      }
    }
    else
    {
      switch (ipValidator?.type)
      {
        case 'private':
          // FC00::/7 
          // FD00:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF/7
          nbRanges.push({min: 0xFC000000000000000000000000000000n, max: 0xFD00FFFFFFFFFFFFFFFFFFFFFFFFFFFFn});
          break;

        case 'unicast':
          // 2000::
          // 3FFF::
          nbRanges.push({min: 0x20000000000000000000000000000000n, max: 0x3FFF0000000000000000000000000000n});  
          break;

        case 'multicast':
          // FF02:0:0:0:0:0:0:0 
          // FF02:0:0:0:0:1:FFFF:FFFF 
          nbRanges.push({min: 0xFF020000000000000000000000000000n, max: 0xFF0200000000000000000001FFFFFFFFn});
          break;
      }
    }
  }

  // if there are ranges
  if(ipValidator.ranges)
  {
    try
    {
      for(const range of ipValidator.ranges)
      {
        const nbRange = {
          max: range.max !== undefined ? ipTabToIpNb(stringIpToTabs(range.max, _isIpv4), _isIpv4) : undefined, 
          min: range.min !== undefined ? ipTabToIpNb(stringIpToTabs(range.min, _isIpv4), _isIpv4) : undefined,
          inside: range.inside !== undefined ? range.inside : undefined,
        }
        nbRanges.push(nbRange)
      }
    }
    catch(e)
    {
      console.error('template ip validation incorrect', ipValidator.ranges)
    }
  }

    // valid  is a logic AND between current validity and range validity
    // return validity
  return valid && testNumber(nbRanges, ipNumber);
}