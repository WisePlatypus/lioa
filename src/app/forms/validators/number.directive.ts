import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { TNumberValidator } from '../models/template';


@Directive({
  selector: '[validatorNumber]',
  providers: [{provide: NG_VALIDATORS, useExisting: NumberDirective, multi: true}]
})
export class NumberDirective implements Validator 
{

  @Input('validatorNumber') validator: TNumberValidator;

  constructor() { }

  validate(control: AbstractControl): ValidationErrors | null 
  {
    return this.validator ? validatorNumber(this.validator)(control) : null;
  }  

}


export function validatorNumber(nbValidator: TNumberValidator): ValidatorFn 
{
  return (control: AbstractControl): {[key: string]: any} | null => 
  {
    let valid = true;
    // check if value is int if validator is active
    if(nbValidator?.isInt)
    {
      valid = valid && (control.value - Math.floor(control.value)) === 0;
    }

    // if there are ranges
    if(nbValidator.ranges)
    {
      // valid  is a logic AND between current validity and range validity
      valid = valid && testNumber(nbValidator.ranges, control.value);
    }
    // return validity
    return valid ? null : {nbValidator: {value: control.value, validator: nbValidator}};
  };
}



export function testNumber(ranges: {min?: number | bigint, max?: number | bigint,inside?: boolean}[], value: number | bigint) 
{
  let rangesValid = undefined;
  // for each range apply a or or and deppending on incluvisity exlusivity
  for (const range of ranges) 
  {
    let insideRange = true;

    // Depending on range completion compute validation
    if (range?.min !== undefined) { insideRange = insideRange && (value >= range.min); }
    if (range?.max !== undefined) { insideRange = insideRange && (value <= range.max); }

    // apply AND or OR depending if the range is an "inside range" our "outside range"
    if (range?.inside !== undefined && range.inside === false) 
    {
      rangesValid = rangesValid === undefined ? insideRange : rangesValid && !insideRange; 
    }
    else 
    { 
      rangesValid = rangesValid === undefined ? insideRange : rangesValid || insideRange; 
    }
  }


  return rangesValid === undefined ? true : rangesValid;
}


export function testRange(dataSets: {value: any, include: boolean}[][], range: number[])
{
  if(range.length === 2 && range[0] !== undefined && range[1] !== undefined)
  {
    let valid = false;
  
    for(const dataSet of dataSets)
    {
      let setValid = true;
  
      for(const data of dataSet)
      {
        const include = range[0] <= data.value && range[1] >= data.value;
  
        setValid = setValid && data.include === false ? !include : include;
      }
  
      valid = valid || setValid;
    }
  
    return valid;
  }
  return false;
}