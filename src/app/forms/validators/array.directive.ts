/* eslint-disable */
import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { TArrayValidator } from '../models/template';

@Directive({
  selector: '[array]',
  providers: [{provide: NG_VALIDATORS, useExisting: ArrayDirective, multi: true}]
})
export class ArrayDirective implements Validator  
{
  @Input() array: TArrayValidator

  constructor() { }
  
  validate(control: AbstractControl): ValidationErrors 
  {
    return this.array ? validatorArray(this.array)(control) : null;
  }

}


export function validatorArray(validator: TArrayValidator): ValidatorFn 
{
  return (control: AbstractControl): {[key: string]: any} | null => 
  {
    return validateArray(validator, <any[]>control.value) ? null : {boolean: {value: control.value, expected: validatorArray}};
  };
}

export function validateArray(validator: TArrayValidator, array: any[])
{
  let valid = true;

  if(validator.max)
  {
    valid = valid && (array.length <= validator.max);
  }
  if(validator.min)
  {
    valid = valid && (array.length >= validator.min);
  }

  return valid;
}