import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[validatorBoolean]',
  providers: [{provide: NG_VALIDATORS, useExisting: BooleanDirective, multi: true}]
})
export class BooleanDirective implements Validator 
{

  @Input('validatorBoolean') bool: boolean;

  constructor() { }
  validate(control: AbstractControl): ValidationErrors | null 
  {
    return this.bool !== undefined ? validatorBoolean(this.bool)(control) : null;
  }  
}

export function validatorBoolean(bool: boolean): ValidatorFn 
{
  return (control: AbstractControl): {[key: string]: any} | null => 
  {
    const valid = bool === control.value;
    return valid ? {boolean: {value: control.value, expected: bool}} : null;
  };
}