import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { TDateValidator } from '../models/template';
import { testNumber } from './number.directive';

@Directive({
  selector: '[validatorDate]',
  providers: [{provide: NG_VALIDATORS, useExisting: DateDirective, multi: true}]
})
export class DateDirective implements Validator 
{

  @Input('validatorDate') validator: TDateValidator;

  constructor() { }

  validate(control: AbstractControl): ValidationErrors | null 
  {
    return this.validator ? validatorDate(this.validator)(control) : null;
  }  

}

export function validatorDate(dateValidator: TDateValidator): ValidatorFn 
{
  return (control: AbstractControl): {[key: string]: any} | null => 
  {
    // return validity
    return validateDate(dateValidator, control.value) ? null : {nbValidator: {value: control.value, validator: dateValidator}};
  };
}


export function validateDate(dateValidator: TDateValidator, value: Date)
{
  let valid = true;

  if(value)
  {
    // if there are ranges
    if(dateValidator.ranges)
    {
      // valid  is a logic AND between current validity and range validity
      valid = valid && testNumber(dateValidator.ranges, value.getTime());
    }
  }
  else
  {
    return false;
  }

  return valid;
}