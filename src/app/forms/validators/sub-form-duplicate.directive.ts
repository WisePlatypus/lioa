/* eslint-disable */
import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS } from '@angular/forms';
import { AbstractControl } from '@ngneat/reactive-forms';
import { ValidationErrors, ValidatorFn } from '@ngneat/reactive-forms/lib/types';
import { hashObjectValue } from '@nororljos/nrs-util';
import { TItem, TNamable } from '../models/template';


@Directive({
  selector: '[appSubFormDuplicate]',
  providers: [{provide: NG_VALIDATORS, useExisting: SubFormDuplicateDirective, multi: true}]
})
export class SubFormDuplicateDirective {

  @Input('validatorSubFormDuplicate') validator: SubFormDuplicateValidator;

  constructor() { }

  validate(control: AbstractControl): ValidationErrors | null 
  {
    return this.validator ? validatorSubFormDuplicate(this.validator)(control) : null;
  }  

}


export function validatorSubFormDuplicate(sfdValidator: SubFormDuplicateValidator): ValidatorFn 
{
  return (control: AbstractControl): {[key: string]: any} | null => 
  {
    const valid = testObjectKeyDuplicate(sfdValidator, control.value);

    return valid ? null : {nbValidator: {value: control.value, validator: sfdValidator.keys}};
  };
}



export function testObjectKeyDuplicate(sfdValidator: SubFormDuplicateValidator, value: any): boolean
{
  const valueH = hashObjectValue(value, sfdValidator.keys.map(sdf => sdf.path));

  if(sfdValidator.hashes[valueH] === true)
  {
    return false;
  }

  return true;
}



export interface SubFormDuplicateValidator
{
  keys: {item: TNamable, path: string[]}[], 
  hashes: 
  {
    [key: string]: boolean
  }
}




