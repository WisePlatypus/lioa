

/**
 * This file contain the interfaces and type checker far all tmeplate related data
 * It is definitly under commented since most of the things here are just definition and TS type guards no complexe logic here
 * See Template form documentation for mor documentation
 *
*/


// export const sample1 = templateSnmp.template;



export const INPUTS_LABELS: {[key: string]: boolean} =
{
    'text': false,
    'text-area': false,
    'number': false,
    'drop-down': false,
    'suggest-drop-down': false,
    'checkbox': true,
    'toggle': true,
    'check-group': true,
    'radio': false,
    'list': false,
    'date': false,
    'date-range': false,
    'range': false,
    'ip': false,
    'selection-list': false,
    'list-display': false,
}

export const INPUT_TYPES: {[key: string]: 'text' | 'number' | 'boolean' | 'boolean[]' | 'object' | 'date' | 'date[]' | 'ip' | 'any' | 'any[]'} =
{
    'text': 'text',
    'text-area': 'text',
    'drop-down': 'any',
    'suggest-drop-down': 'any',
    'radio': 'text',
    'number': 'number',
    'range': 'number',
    'checkbox': 'boolean',
    'toggle': 'boolean',
    'check-group': 'object',
    'list': 'object',
    'date': 'date',
    'date-range': 'date[]',
    'ip': 'ip',
    'selection-list': 'any[]',
    'list-display': 'any[]'
}

export const TEMPLATE_TYPES = [
    'group',
    'layout',
    'item',
    'display',
    'input',
    'array',
]


export const NAMABLE: {[key: string]: boolean} =
{
    'group': true,
    'layout': false,
    'item': true,
    'display': false,
    'input': false,
    'array': true,
}


export const ENCAPSULABLE: {[key: string]: boolean} =
{
    'group': true,
    'layout': true,
    'item': false,
    'display': false,
    'input': false,
    // data is not flat inside and not part of final control value as is
    'array': false,
}


export function isTemplateForm(tf): boolean
{
    if(Array.isArray(tf))
    {
        for(const el of tf)
        {
            if(!isTElement(el))
            {
                return false;
            }
        }
        return true;
    }
    else if(typeof tf === 'object' && 'children' in tf)
    {
        return isTemplateForm(tf.children)
    }
    return false;
}


export type TemplateBase = FormTemplate | TElement[];


export interface FormTemplate
{
    children: TElement[];
}


export interface TEncapsulable extends TElement
{
    children: TElement[];
}


export function isTEncapsulable(el): el is TEncapsulable
{
    if(typeof el === 'object' && 'children' in el && Array.isArray(el.children))
    {
        for(const sel of el.children)
        {
            if(!isTElement(sel))
            {
                return false;
            }
        }
        return true;
    }
    return false;
}


export interface TElement
{
    type: string;

    grow?: number;
    fz?: number;
    conditions?: TCondition[];
}



export function isTElement(el): el is TElement
{
   
    if(typeof el === 'object' && 'type' in el && typeof el.type === 'string')
    {
        if('grow' in el && typeof el.grow !== 'number') return false;
        if('fz' in el && typeof el.fz !== 'number') return false;
        if('conditions' in el)
        {
            if(Array.isArray(el.conditions))
            {
                for(const ar of el.conditions)
                {
                    if(!isTCondition(ar)) return false;
                }
            }
            else return false;
        }


        switch(el.type)
        {
            case 'group':
                if(!(isTGroup(el) && isTNamable(el) && isTEncapsulable(el))) return false;
                break;
            case 'array':
                if(!(
                    isTArray(el)
                    &&
                    isTNamable(el))) return false;
                break;
            case 'item':
                if(!(isTItem(el) && isTNamable(el))) return false;
                break;
            case 'layout':
                if(!(isTLayout(el) && isTEncapsulable(el))) return false;
                break;
            case 'text-display':
                if(!('text' in el)) return false
                break;
            default:
                return false;
        }

        return true;
    }
    return false;
}


export interface TNamable extends TElement
{
    label?: TTranslatable;
    name: string;
}

export function isTNamable(el): el is TNamable
{
    if(typeof el === 'object' && 'name' in el && typeof el.name === 'string')
    {
        if('label' in el)
        {
            if(typeof el.label === 'object')
            {
                if(!isTTranslatable(el.label)) return false;
            }
            else if (typeof el.label !== 'string') return false;
        }
        return true;
    }
    return false;
}


export type TTranslatable =  {[key: string]: string} | string;

export function isTTranslatable(el): el is TTranslatable
{
    if(typeof el === 'object')
    {
        for(const [key, value] of Object.entries(el))
        {
            if(typeof value !== 'string')
            {
                return false;
            }
        }
        return true;
    }
    return false;
}

export interface TGroup extends TNamable, TEncapsulable, TElement
{
    readonly type: 'group';
    useParent?: boolean;
    title?: number;

    hidden?: boolean;
    collapsed?: boolean;
    noMargin?: boolean;
}


export function isTGroup(el): el is TGroup
{
    if(typeof el === 'object' && 'type' in el && el.type === 'group')
    {
        if('title' in el && typeof el.title !== 'number') return false;
        if('collapsed' in el && typeof el.collapsed !== 'boolean') return false;
        if('hidden' in el && typeof el.hidden !== 'boolean') return false;
        if('useParent' in el && typeof el.useParent !== 'boolean') return false;
        if('noMargin' in el && typeof el.noMargin !== 'boolean') return false;

        return true;
    }
    return false;
}


export interface TArray extends TNamable, TElement
{
    readonly type: 'array';
    subForm: TElement[];
    arrayType?: 'list' | 'form';
    listColumn?: string[];
    validator?: TArrayValidator;
    array?: any[];
    title?: number;
    collapsed?: boolean;
    controls?: ('add' | 'delete' | 'update')[];
    uniqueItemComb?: string[] | string[][];
}

export function isTArray(el): el is TArray
{
    if(typeof el === 'object' && 'type' in el && el.type === 'array' && 'subForm' in el && Array.isArray(el.subForm))
    {
        if('arrayType' in el && !(el.arrayType === 'list' || el.arrayType === 'form')) return false;

        if('listColumn' in el && Array.isArray(el.listColumn))
        {
            for(const listEl of el.listColumn)
            {
                if(typeof listEl !== 'string') return false;
            }
        }

        if('validator' in el && !isTArrayValidator(el.validator)) return false;
        if('array' in el && !Array.isArray(el.array)) return false;
        if('title' in el && typeof el.title !== 'number') return false;
        if('collapsed' in el && typeof el.collapsed !== 'boolean') return false;


        if('controls' in el)
        {
            if(Array.isArray(el.controls))
            {
                for(const control of el.controls)
                {
                    if(typeof control !== 'string' || !(control === 'add' || control === 'delete' || control === 'update')) return false;
                }
            }
            else return false;
        }

        if('uniqueItemComb' in el)
        {
            if(Array.isArray(el.uniqueItemComb))
            {
                for(const item of el.uniqueItemComb)
                {
                    if(Array.isArray(item))
                    {
                        for(const sitem of item)
                        {
                            if(typeof sitem !== 'string') return false;
                        }
                    }
                    else if(typeof item !== 'string') return false;
                }
            }
            else return false;
        }

        for(const tel of el.subForm)
        {
            if(!isTElement(tel)) return false;
        }

        return true;
    }
    return false;
}

export interface TArrayValidator
{
    max?: number;
    min?: number;
}



export function isTArrayValidator(el): el is TArrayValidator
{
    if(typeof el === 'object')
    {
        if('min' in el && typeof el.min !== 'number') return false;
        if('max' in el && typeof el.max !== 'number') return false;

        return true;
    }
    return false;
}


export interface TLayout extends TEncapsulable, TElement
{
    readonly type: 'layout';
    layout: 'pages' | 'line' | 'stacked' |'column';
}

export function isTLayout(el): el is TLayout
{
    if(typeof el === 'object' && 'type' in el && el.type === 'layout' && 'layout' in el && (el.layout === 'pages' || el.layout === 'line' || el.layout === 'stacked' || el.layout === 'column'))
    {
        return true;
    }
    else return false;
}


export interface TItem<T extends TInterface = TInterface> extends TNamable, TElement
{
    readonly type: 'item';
    value: any;

    overwriteOnCondition?: boolean;

    interface?: T;
}

export function isTItem(el): el is TItem
{
    if(typeof el === 'object' && 'type' in el && el.type === 'item')
    {
        // if('value' in el === false)
        // {
        //     return false;
        // }

        if('interface' in el && !isTInterface(el.interface)) return false;

        return true;
    }
    return false;
}


export interface TInterface
{
    type: 'input' | 'display';
    help?: TTranslatable;
    width?: 'simple' | 'double'
}


export function isTInterface(el): el is TInterface
{
    if(typeof el === 'object' && 'type' in el && (el.type === 'input' || el.type === 'display'))
    {
        if('help' in el && !(typeof el.help === 'string' || isTTranslatable(el.help)))
        {
            return false;
        }

        if('width' in el)
        {
            if((typeof el.width !== 'string'))
            {
                return false;
            }
            switch(el.width)
            {
                case 'simple':
                case 'double':
                    break;
                default:
                    return false;
            }
        }

        switch(el.type)
        {
            case 'input':
                if(!isTInput(el)) return false;
                break;
            case 'display':
                break;
            default:
                return false;
        }

        return true;
    }
    return false;
}


export interface TCondition
{
    item: string;
    mode?: 'display' | 'enable' | 'itemField';
    validator: any;
    testedFieldPath?: string;
    affectedFieldPath?: string;
    trueVal?: any;
    falseVal?: any;
}

export function isTCondition(el): el is TCondition
{
    if(typeof el === 'object' && 'validator' in el && 'item' in el && typeof el.item === 'string')
    {
        if('mode' in el && !(el.mode === 'display' || el.mode === 'enable' || el.mode === 'itemField')) return false;

        return true;
    }
    return false;
}



export interface TConditionText extends TCondition
{
    validator: string;
}

export interface TConditionBoolean extends TCondition
{
    validator: boolean;
}

export interface TConditionText extends TCondition
{
    validator: string;
}

export interface TConditionNumber extends TCondition
{
    validator: TNumberValidator;
}

export interface TConditionDate extends TCondition
{
    validator: TDateValidator;
}

export interface TConditionDateRange extends TCondition
{
    validator: TDateRangeValidator;
}

export interface TConditionIp extends TCondition
{
    validator: TIpValidator;
}


export interface TConditionAny extends TCondition
{
    validator: TAnyValidator;
}

export interface TAnyValidator
{
    include?: any[];
    exclude?: any[];
}

export interface TNumberValidator
{
    ranges?:
    {
        min?: number,
        max?: number,
        inside?: boolean;
    }[];
    isInt?: boolean;
}

export function isTNumberValidator(el): el is TNumberValidator
{
    if(typeof el === 'object')
    {
        if('ranges' in el && Array.isArray(el.ranges))
        {
            for(const range of el.ranges)
            {
                if(typeof range === 'object')
                {
                    if('min' in range && typeof range.min !== 'number') return false;
                    if('max' in range && typeof range.max !== 'number') return false;
                    if('inside' in range && typeof range.inside !== 'boolean') return false;
                }
                else return false;
            }
        }
        if('isInt' in el && typeof el.isInt !== 'boolean') return false;

        return true;
    }

    return false;
}

export interface TDateValidator
{
    ranges?:
    {
        min?: number,
        max?: number,
        inside?: boolean;
    }[];
}

export function isTDateValidator(el): el is TDateValidator
{
    if(typeof el === 'object')
    {
        if('ranges' in el && Array.isArray(el.ranges))
        {
            for(const range of el.ranges)
            {
                if(typeof range === 'object')
                {
                    if('min' in range && typeof range.min !== 'number') return false;
                    if('max' in range && typeof range.max !== 'number') return false;
                    if('inside' in range && typeof range.inside !== 'boolean') return false;
                }
                else return false;
            }
        }
        return true;
    }
    return false;
}

export interface TIpValidator
{
    type?: 'multicast' | 'any' | 'private' | 'unicast';
    ranges?:
    {
        min?: string,
        max?: string,
        inside?: boolean;
    }[];
}

export function isTIpValidator(el): el is TIpValidator
{
    if(typeof el === 'object')
    {
        if('ranges' in el && Array.isArray(el.ranges))
        {
            for(const range of el.ranges)
            {
                if(typeof range === 'object')
                {
                    if('min' in range && typeof range.min !== 'string') return false;
                    if('max' in range && typeof range.max !== 'string') return false;
                    if('inside' in range && typeof range.inside !== 'boolean') return false;
                }
                else return false;
            }
        }

        if('type' in el) 
        {
            if(!(el.type !== 'multicast' || el.type !== 'any' || el.type !== 'private' || el.type !== 'unicast')) return false;
        }
        else
        {
            return false  
        }      

        return true;
    }
    return false;
}



export interface TDateRangeValidator
{
    dateSets: {value: any, include: boolean}[][];
}

export function isTDateRangeValidator(el): el is TDateRangeValidator
{
    if(typeof el === 'object' && 'dateSets' in el && Array.isArray(el.dateSets))
    {
        for(const dateSet of el.dateSets)
        {
            if(Array.isArray(dateSet))
            {
                for(const date of dateSet)
                {
                    if(!(typeof date === 'object' && 'value' in date && typeof date.value === 'number' && 'include' in date && typeof date.include === 'boolean'))
                        return false;
                }
            }
            else return false;
        }
        return true;
    }
    return false;
}

export interface TInput extends TInterface
{
    readonly type: 'input';
    input: 'text' | 'text-area' | 'number' | 'drop-down' | 'checkbox' | 'toggle' | 'check-group' | 'radio' | 'date' | 'date-range' | 'range' | 'ip' | 'suggest-drop-down' | 'selection-list';

    display?: boolean;

    validator?: any;
    unvalidMsg?: TTranslatable;
    required?: boolean;
    focus?: boolean;
}


export interface TDisplay extends TInterface
{
    readonly type: 'display';
    input: 'list-display';
}


export function isTInput(el): el is TInput
{
    if(typeof el === 'object' && 'input' in el && (
           el.input === 'text'
        || el.input === 'text-area'
        || el.input === 'number'
        || el.input === 'drop-down'
        || el.input === 'suggest-drop-down'
        || el.input === 'checkbox'
        || el.input === 'toggle'
        || el.input === 'check-group'
        || el.input === 'radio'
        || el.input === 'date'
        || el.input === 'date-range'
        || el.input === 'range'
        || el.input === 'ip'
        || el.input === 'selection-list'

        ) && 'type' in el && el.type === 'input')
    {
        if('display' in el && typeof el.display !== 'boolean') return false;
        if('unvalidMsg' in el && !(typeof el.unvalidMsg === 'string' || isTTranslatable(el.unvalidMsg))) return false;
        if('required' in el && typeof el.required !== 'boolean') return false;

        switch(el.input)
        {
            case 'text':
                if(!isTInputText(el)) return false;
                break;
            case 'text-area':
                if(!isTInputTextArea(el)) return false;
                break;
            case 'number':
                if(!isTInputNumber(el)) return false;
                break;
            case 'drop-down':
                if(!isTInputDropDown(el)) return false;
                break;
            case 'checkbox':
                if(!isTInputCheck(el)) return false;
                break;
            case 'toggle':
                if(!isTInputToggle(el)) return false;
                break;
            case 'date':
                if(!isTInputDate(el)) return false;
                break;
            case 'date-range':
                if(!isTInputDateRange(el)) return false;
                break;
            case 'ip':
                if(!isTInputIp(el)) return false;
                break;
            case 'check-group':
                return true;
            case 'suggest-drop-down':
            case 'selection-list':
                return true;
            default:
                return false;
        }

        return true;
    }
    return false;
}



export interface TInputText extends TInput
{
    readonly input: 'text';
    validator?: string;
    secret?: boolean;
    isRegExp?: boolean
}


export function isTInputText(el): el is TInputText
{
    if(typeof el === 'object' && 'input' in el && el.input === 'text')
    {
        if('validator' in el && typeof el.validator !== 'string')
        {
            return false;
        }
        if('secret' in el && typeof el.secret !== 'boolean')
        {
            return false;
        }
        return true;
    }
    return false;
}

export interface TInputTextArea extends TInput
{
    readonly input: 'text-area';
    validator?: string;
}

export function isTInputTextArea(el): el is TInputTextArea
{
    if(typeof el === 'object' && 'input' in el && el.input === 'text-area')
    {
        if('validator' in el && typeof el.validator !== 'string')
        {
            return false;
        }
        return true;
    }
    return false;
}

export interface TInputCheck extends TInput
{
    readonly input: 'checkbox';
    validator?: boolean;
    inline?: boolean;
}

export interface TInputToggle extends TInput
{
    readonly input: 'toggle';
    validator?: boolean;
    inline?: boolean;
}

export interface TInputCheckGroup<T = {alias?: TTranslatable, name: string, value?: boolean}[]> extends TInput
{
    readonly input: 'check-group';
    validator?: object;
    elements: T;
    // {alias?: TTranslatable, name: string, value?: boolean}[] | string[] | {[key: string]: boolean};
}

export function isTInputCheck(el): el is TInputCheck
{
    if(typeof el === 'object' && 'input' in el && el.input === 'checkbox')
    {
        if('validator' in el && typeof el.validator !== 'boolean')
        {
            return false;
        }
        return true;
    }
    return false;
}

export function isTInputToggle(el): el is TInputCheck
{
    if(typeof el === 'object' && 'input' in el && el.input === 'toggle')
    {
        if('validator' in el && typeof el.validator !== 'boolean')
        {
            return false;
        }
        return true;
    }
    return false;
}


export interface TInputNumber extends TInput
{
    readonly input: 'number';
    validator?: TNumberValidator;
    step?: number;
    hardStep?: boolean;
    min?: number;
    max?: number;
    suffix?: string;
}

export function isTInputNumber(el): el is TInputNumber
{
    if(typeof el === 'object' && 'input' in el && el.input === 'number')
    {
        if('validator' in el && !isTNumberValidator(el.validator)) return false;
        if('step' in el && typeof el.step !== 'number') return false;
        if('min' in el && typeof el.min !== 'number') return false;
        if('hardStep' in el && typeof el.hardStep !== 'boolean') return false;
        if('max' in el && typeof el.max !== 'number') return false;
        if('suffix' in el && typeof el.suffix !== 'string') return false;
        return true;
    }
    return false;
}


export interface TInputDropDown extends TInput
{
    readonly input: 'drop-down';
    elements: TemplateListElement[],
}

export interface TInputSuggestDropDown extends TInput
{
    readonly input: 'suggest-drop-down';
    elements:
    {
        alias?: TTranslatable;
        value: any;
    }[] | string[],
    editable: boolean;
}


export function isTInputDropDown(el): el is TInputDropDown
{
    if(typeof el === 'object' && 'input' in el && el.input === 'drop-down' && 'elements' in el && Array.isArray(el.elements))
    {
        for(const del of el.elements)
        {
            if(typeof del === 'object')
            {
                if(typeof del === 'object' && 'value' in del)
                {
                    if('alias' in del && !(typeof del.alias === 'string' || isTTranslatable(del.alias))) return false;
                }
                else return false;
            }
            else if(typeof del !== 'string') return false;
        }
        return true;
    }
    return false;
}


export interface TInputSuggestDropDown extends TInput
{
    readonly input: 'suggest-drop-down';
    elements:
    {
        alias?: TTranslatable;
        value: any;
    }[] | string[],
}


export interface TInputSelectionList extends TInput
{
    readonly input: 'selection-list';

    elementsLabel?: TTranslatable;
    selectionLabel?: TTranslatable;

    fieldLabels?: Label[];

    validator?: TArrayValidator;

    elements:  TemplateListElement[],
}



export interface TListDisplay extends TDisplay
{
    readonly input: 'list-display';

    elementsLabel?: TTranslatable;

    fieldLabels?: Label[];

    elements:  TemplateListElement[],
}





export interface TInputIp extends TInput
{
    readonly input: 'ip';
    validator?: TIpValidator;
    port?: boolean;
    mode?: 'ipv4' | 'ipv6' | 'both';
}


export function isTInputIp(el): el is TInputIp
{
    if(typeof el === 'object' && 'input' in el && el.input === 'ip')
    {
        if('validator' in el && !isTIpValidator(el.validator)) return false;
        if('port' in el && typeof el.port !== 'boolean') return false;
        return true;
    }
    return false;
}

export interface TInputDate extends TInput
{
    readonly input: 'date';
    validator?: TDateValidator;
    time?: boolean;
}


export function isTInputDate(el): el is TInputDate
{
    if(typeof el === 'object' && 'input' in el && el.input === 'date')
    {
        if('validator' in el && !isTDateValidator(el.validator)) return false;
        if('time' in el && typeof el.time !== 'boolean') return false;
        return true;
    }
    return false;
}


export interface TInputDateRange extends TInput
{
    readonly input: 'date-range';
    validator?: TDateRangeValidator;
    time?: boolean;
}


export function isTInputDateRange(el): el is TInputDate
{
    if(typeof el === 'object' && 'input' in el && el.input === 'date-range')
    {
        if('validator' in el && !isTDateRangeValidator(el.validator)) return false;
        if('time' in el && typeof el.time !== 'boolean') return false;
        return true;
    }
    return false;
}

export interface TDisplayText extends TElement
{
    type: string;
    text: TTranslatable;
}

export function isTDisplayText(el): el is TDisplayText
{
    if(typeof el === 'object' && 'text' in el)
    {
        return true;
    }
    return false;
}




export interface TMessage
{
    type: string;
    value?: any;
    // [key: string]: any;
}




export interface Label
{
  alias: TTranslatable;
  path: string;
}




export type TemplateListElement = string | {alias?: TTranslatable, value: any, name?: string, disabled?: boolean}
export type TemplateListElementSel = string | {alias?: TTranslatable, selectedAlias?: TTranslatable, value: any, name?: string, disabled?: boolean}
