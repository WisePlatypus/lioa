import { Pipe, PipeTransform } from '@angular/core';
import { TTranslatable } from '../models/template';
import { TemplateService } from '../services/template.service';

/**
 * Self explenatory
 */
@Pipe({
  name: 'translatable',
  pure: true,
})
export class TranslatablePipe implements PipeTransform
{
  constructor(private template: TemplateService) {/** empty */}


  transform(value: TTranslatable): string 
  {
    return this.template.getTranslation(value);
  }
}