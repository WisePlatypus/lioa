import { Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ResizeEvent } from '@nororljos/nrs-directives';
import flatpickr from 'flatpickr';
import { Instance } from 'flatpickr/dist/types/instance';
import { BaseOptions } from 'flatpickr/dist/types/options';

@Component({
  selector: 'forms-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR, 
      useExisting: forwardRef(() => DateInputComponent),
      multi: true,
    },
  ],
})
export class DateInputComponent implements OnInit, ControlValueAccessor  
{

  settings: Partial<BaseOptions> = {
    // mode: "time",
    // mode: 'single',
    inline: true,
    // enableTime: true,
    time_24hr: true,
  }

  disabled = false;

  @Input() asTimeStamp: boolean = false;

  @Input() autoSize: boolean = false;
  
  @Input() time: boolean = false;

  @ViewChild('range') 
  set range(range: ElementRef<HTMLDivElement>)
  {
    if(this.time)
    {
      this.settings.enableTime = true;
    }

    this._range = range;
    if(this._range?.nativeElement !== undefined)
    {
      this.flatpikrI =  flatpickr(this._range.nativeElement, this.settings);
      this.flatpikrI.config.onChange.push((sl ,ds, i)=>
      {
        if(sl)
        {
          this.onChange(this.asTimeStamp ? sl[0].getTime() : sl[0]);
          this.onTouched()
        }
      });
    }
  }
  _range: ElementRef<HTMLDivElement>;

  @ViewChild('cc') 
  cc: ElementRef<HTMLDivElement>;

  constructor(private elRef: ElementRef<HTMLDivElement>) { }

  flatpikrI: Instance;

  

  ngOnInit(): void {}

  onResize(event: ResizeEvent)
  {
    if(this.autoSize)
    {
      if(event.newWidth / event.newHeight < 0.9)
      {
        this.elRef.nativeElement.style.setProperty('font-size', `${event.newWidth/246.3}em`)
      }
      else
      {
        if(this.time)
        {
          this.elRef.nativeElement.style.setProperty('font-size', `${event.newHeight/273.58}em`)
        }
        else
        {
          this.elRef.nativeElement.style.setProperty('font-size', `${event.newHeight/241.58}em`)
        }
      }
    }
  }


  

  /**
   * ControlValueAccessor interface
   *  
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */ 


  // Function to call when text change 
  onChange = (date: number | Date) => {};
  // Function to call when the input is touched 
  onTouched = () => {};

  writeValue(date: number | Date): void
  {
    if(this.flatpikrI)
    {
      this.flatpikrI.setDate(typeof date === 'number' ? new Date(date) : date);
    }
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }

}
