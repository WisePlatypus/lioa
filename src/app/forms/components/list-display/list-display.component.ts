/* eslint-disable */
import { Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ResizeEvent } from '@nororljos/nrs-directives';
import { EventState, getValueWithPath, deepCompare, deepElementCompare } from '@nororljos/nrs-util';
import { Label, TemplateListElement, TTranslatable } from '../../models/template';
import { TemplateService } from '../../services/template.service';


@Component({
  selector: 'forms-list-display',
  templateUrl: './list-display.component.html',
  styleUrls: ['./list-display.component.scss'],providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting:
      forwardRef(() => ListDisplayComponent),
      multi: true,
    },
  ]
})
export class ListDisplayComponent implements OnInit
{
  @Input()
  set elementsLabel(label: TTranslatable)
  {
    this._elementsLabel = label;
  }

  get elementsLabel(): TTranslatable
  {
    return this.template.getTranslation(this._elementsLabel)
  }

  _elementsLabel: TTranslatable = undefined;

  /**
   ** elements setter
   */
  @Input()
  set elements(elements: TemplateListElement[])
  {
    this._elements = elements;

    this._selection = [];
  }

  /**
   ** elements getter
   */
  get elements(): TemplateListElement[]
  {
    return this._elements;
  }

  //- elements
  // these are unselected base elements with optionnal alias and value
  _elements: TemplateListElement[] = [];


  @Input()
  set fieldLabels(labels: Label[])
  {
    if(Array.isArray(labels))
    {
      this._fieldLabels = labels;
    }
  }

  get fieldLabels(): Label[]
  {
    return this._fieldLabels;
  }


  _fieldLabels: Label[] = [];




  @Input() namePath: string;


  //- selection
  // these are selected elements with opètionnal alias and value
  _selection: TemplateListElement[] = [];


  // column selected id
  elementsSelectedId: number[] = [];

  elementsSelectedMap: {[key: number]: boolean} = {};

  disabled: boolean = false;

  constructor(private template: TemplateService, private elRef: ElementRef<HTMLElement>) { }

  ngOnInit(): void {}

  /**
   ** return alias of a TemplateListElement
   * @param element
   * @returns
   */
  getAlias(element: TemplateListElement): string
  {
    return element !== undefined ? typeof element !== 'object' ? element : (element.alias !== undefined ? this.template.getTranslation(element.alias) : element.value) : '';
  }

  /**
   ** return value of a TemplateListElement
   * @param element
   * @returns
   */
  getValue(element: TemplateListElement)
  {
    return typeof element === 'object' && 'value' in element ? element.value : element;
  }

  /**
   ** return element alias given an ID
   * @param id
   * @returns
   */
  getElementAlias(id: number)
  {
    return this.getAlias(this.elements[id]);
  }

  /**
   ** return element value given an ID
   * @param id
   * @returns
   */
  getElementValue(id: number)
  {
    return this.getValue(this.elements[id]);
  }


  onListClick(i: number, selectedId: number[], selectedMap: {[key: number]: boolean}, opselectedId: number[], opselectedMap: {[key: number]: boolean})
  {
  }



  value(element)
  {
    return typeof element === 'object' ? ('value' in element ? element.value : element) : element;
  }

  getLabelValue(element: any, label: Label)
  {
    return getValueWithPath(this.getValue(element), label.path.split('.'));
  }

  onResize(resize: ResizeEvent)
  {
    this.elRef.nativeElement.style.setProperty('--table-body-height', `calc(${resize.newHeight}px - ${this.fieldLabels.length === 0 ? 2.467 : 2.467 + 2.5 }em)`)
  }

  getNameValue(element)
  {
    return this.namePath ? getValueWithPath(this.getValue(element), this.namePath.split('.')) : typeof element === 'object' ? ('name' in element ? element.name : this.value(element)) : element;
  }


  /**
   * ControlValueAccessor interface
   *
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */

  // Function to call when text change
  onChange = (elements: TemplateListElement[]) => {};
  // Function to call when the input is touched
  onTouched = () => {};

  writeValue(elements: any[]): void
  {
    this.elements = elements;
  }


  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }

}





// export interface Label
// {
//   alias: TTranslatable;
//   path: string;
// }


// --table-body-height
