/* eslint-disable */
import { Component, ComponentRef, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ModalService } from '@nororljos/nrs-modal';
import { partialDeepCompare } from '@nororljos/nrs-util';
import { TemplateListElement, TemplateListElementSel } from '../../models/template';
import { TemplateService } from '../../services/template.service';
import { ModalDropDownComponent } from '../modal-drop-down/modal-drop-down.component';


@Component({
  selector: 'forms-drop-down-list',
  templateUrl: './drop-down-list.component.html',
  styleUrls: ['./drop-down-list.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR, 
      useExisting: DropDownListComponent,
      multi: true,
    },
  ],
})
export class DropDownListComponent implements OnInit, ControlValueAccessor
{
  
  @Input() 
  set elements(els: TemplateListElementSel[])
  {
    if(els !== undefined && els.length > 0) 
    {
      this._elements = els;
      if(this.autoSelect && !this.alreadySelected)
      {
        if(this.selected < 0 || this.selected >= els.length)
        {
          this.select(0);
        }
        else
        {
          this.select(this.selected);
        } 
      }
    }
  }



  get elements(): TemplateListElementSel[]
  {
    return this._elements
  } 

  _elements: TemplateListElementSel[] = [];

  // selected suffix
  @Input() sSuffix: string = '';


  
  @Input() autoSelect: boolean = true;

  // horizontal and vertical
  @Input() vertical: 'top' | 'bottom' | 'middle' = 'middle';
  @Input() horizontal: 'right' | 'left' | 'from-right' | 'from-left' | 'middle' = 'middle';
  @Input() padding: string = '0.2em 0.5em 0.2em 0.2em';

  @Input() highlight: boolean = true;

  @Output() change: EventEmitter<string> = new EventEmitter<string>()
  @Output() changeId: EventEmitter<number> = new EventEmitter<number>()

  @Input()
  set selected(selected: number)
  {
    // this.alreadySelected = true;
    if(!isNaN(selected) && this.autoSelect && this._elements.length !== 0)
    {
      if(selected < 0 || selected >= this.elements.length)
      {
        this.select(0);
      }
      else
      {
        this.select(selected);
      }
    }
    else
    {
      this._selected = selected;
    }
  }

  get selected(): number
  {
    return this._selected;
  }

  _selected: number = -1;


  @ViewChild('cn') 
  set cn(cn: ElementRef<HTMLDivElement>)
  {
    if(cn)
    {
      this._cn = cn;
    }
  }
  
  _cn: ElementRef<HTMLDivElement> = undefined;

  disabled: boolean = false;
  dropRef: ComponentRef<ModalDropDownComponent>;

  alreadySelected: boolean = false;
  

  constructor(private template: TemplateService, private pop: ModalService) { }

  ngOnInit(): void {
  }


  /**
   ** toggle drop down list
   */
  public async toggleDrop()
  {
    if(!this.dropRef)
    {
      // set dropref
      this.dropRef = await this.pop.instanciateModal<ModalDropDownComponent>(ModalDropDownComponent);

      //-set dropref as undefined on destroy
      this.dropRef.onDestroy(()=>
      {
        this.dropRef = undefined;
      });

      //- configure drop down modal

      //pass elements and selection
      this.dropRef.instance.elements = this.elements;
      this.dropRef.instance.selected = this.selected;

      this.dropRef.instance.highlight = this.highlight;

      // pass the relative element and set font size
      this.dropRef.instance.relativeEl = this._cn.nativeElement;
      this.dropRef.instance.fontSize = window.getComputedStyle(this._cn.nativeElement).fontSize;
      
      // set vertical, horizontal pos and padding
      this.dropRef.instance.vertical = this.vertical;
      this.dropRef.instance.horizontal = this.horizontal;
      this.dropRef.instance.padding = this.padding;

      // set on select callback
      this.dropRef.instance.onSelect = (selected) => 
      {
        this.select(selected);
      }
    }
  }


  /**
   ** set selection
   * @param selected 
   */
  select(selected: number)
  {
    if(typeof selected === 'number')
    {
      this._selected = selected;
      
      setTimeout(() => {
        this.onChange(this.getValueByID(this.selected));
        this.onTouched();
    
        this.change.emit(this.getValueByID(this.selected));
        this.changeId.emit(selected);
      });
    }
    else
    {
      this.select(Math.max(0, this._elements.findIndex((value) => {this.getValue(value) === selected}))) 
    }
  }

  getValueDisplay(id: number): string
  {
    const element = (this.elements ?? [undefined])[id];
    return element !== undefined ? typeof element !== 'object' ? element : 
    (element.selectedAlias !== undefined ? this.template.getTranslation(element.selectedAlias) : 
    (element.alias !== undefined ? this.template.getTranslation(element.alias) : 
    element.value)) : '';
  }

  getValueByID(id: number): string
  {
    const element = this.elements[id];
    return typeof element === 'object' ? element.value : element;
  }

  getValue(element)
  {
    return typeof element === 'object' ? element.value : element;
  }


  // Function to call when the rating changes.
  onChange = (value: any) => {};
  // Function to call when the input is touched (when a star is clicked).
  onTouched = () => {};


  writeValue(value: any): void
  {
    const index = this.elements.findIndex((el, id) => 
    {
      if(typeof value !== typeof this.getValueByID(id))
      {
        return value == this.getValueByID(id);
      }
      switch(typeof value)
      {
        case 'object':
          return partialDeepCompare(value,  this.getValueByID(id));
        case 'string':
        case 'number':
          return value === this.getValueByID(id)
        default:
          return false
      }
    });

    if (index !== -1)
    {
      this.selected = index;
    }
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }

}
