/* eslint-disable */
import { AfterViewInit, Component, ComponentRef, ElementRef, ViewChild } from '@angular/core';
import { ModalComponent } from '@nororljos/nrs-modal';
import { extractNumber } from '@nororljos/nrs-util';

@Component({
  selector: 'app-tool-tip',
  templateUrl: './tool-tip.component.html',
  styleUrls: ['./tool-tip.component.scss']
})
export class ToolTipComponent extends ModalComponent implements AfterViewInit  
{
  @ViewChild('content') content: ElementRef<HTMLDivElement>;
  
  public baseCoo: {x: number, y: number};

  text: string;

  rect: {width: number, height: number} = {width: 0, height: 0};

  constructor() 
  {
    super();
  }

  ngAfterViewInit (): void 
  {
    requestAnimationFrame(()=>
    {
      this.rect.width = extractNumber(window.getComputedStyle(this.content.nativeElement).width);
      this.rect.height = extractNumber(window.getComputedStyle(this.content.nativeElement).height);
      
      this.forceInView();
      
      this.content.nativeElement.style.setProperty('left', `${this.baseCoo.x}px`);
      this.content.nativeElement.style.setProperty('top', `${this.baseCoo.y}px`);
      this.content.nativeElement.style.setProperty('opacity', '1');
    });
  }


  /**
   * force the menu context to be in the screen if one of it's border is not
   * @returns 
   */
  forceInView(): boolean
  {
    let changed = false;

    if(this.baseCoo.y + this.rect.height > window.innerHeight)
    {
      this.baseCoo.y -= this.rect.height;

      if(this.rect.height >= window.innerHeight || this.baseCoo.y < 0)
      {
        this.baseCoo.y = 0;
      }

      changed = true;
    }

    if(this.baseCoo.x + this.rect.width > window.innerWidth)
    {
      this.baseCoo.x = window.innerWidth - this.rect.width;
      changed = true;
    }
    return changed;
  }

  




  onMouseOut()
  {
    this.content.nativeElement.style.setProperty('opacity', '0');
    setTimeout(() => {
      this.componentRef.destroy();
    }, 300);
  }
}


