import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ModalComponent, ModalService } from '@nororljos/nrs-modal';

@Component({
  selector: 'reg-exp-modal',
  templateUrl: './reg-exp-modal.component.html',
  styleUrls: ['./reg-exp-modal.component.scss'],
})
export class RegExpModalComponent extends ModalComponent {
  constructor(private modal: ModalService, private translationService: TranslateService) {
    super();
  }

  regExpValue = '';
  isRegExpValid = true;
  testedValue = '';
  resultValue = '';
  match = false;

  action: (newRexExpValue: string) => void;

  onClickOut()
  {
    if(this.modalId === this.modal.topOfStackInstance.modalId)
    {
      this.componentRef.destroy();
    }
    else
    {
      console.log('cannot close');
    }
  }

  onValueOrRegExpChange()
  {
    //Some regexp value cause errors
    if(this.regExpValue !== '' && !this.regExpValue.startsWith("(?="))
    {
      try
      {
        const reg = new RegExp(this.regExpValue, 'mg');
        let res: RegExpExecArray;
        const matches = [];
        while(null !== (res = reg.exec(this.testedValue)) && res[0] != '') {
          if(res.length > 1)
          {
            res.shift();
            for(const i of res.values())
            {
              matches.push(i);
            }
          }
          else
          {
            matches.push(res[0]);
          }          
        }
        this.isRegExpValid = true;
        const tRes = matches.join(' ');
        this.match = tRes !== '';
        this.resultValue = this.match ? tRes : this.translationService.instant('No match');
      }
      catch(e)
      {
        this.match = false;
        this.isRegExpValid = false
        this.resultValue = this.translationService.instant('No match');
      }
    }
    else
    {
      this.match = false;
      this.resultValue = this.translationService.instant('No match');
    }
  }

  onCancel()
  {
    this.componentRef.destroy();
  }

  onSave()
  {
    if(this.action)
    {
      this.action(this.regExpValue);
    }
    this.componentRef.destroy();
  }
}
