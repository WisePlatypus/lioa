import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegExpModalComponent } from './reg-exp-modal.component';

describe('RexExpComponent', () => {
  let component: RegExpModalComponent;
  let fixture: ComponentFixture<RegExpModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegExpModalComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegExpModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
