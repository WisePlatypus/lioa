/* eslint-disable */
import { AfterViewInit, Component, ComponentRef, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator, Validators } from '@angular/forms';
import { ModalService } from '@nororljos/nrs-modal';
import { RegExpModalComponent } from '../reg-exp-modal/reg-exp-modal.component';


@Component(
{
  selector: 'forms-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting:
      forwardRef(() => TextInputComponent),
      multi: true,
    },
  ]
})
export class TextInputComponent implements AfterViewInit, ControlValueAccessor
{
  // input validation pattern
  @Input() pattern: RegExp = undefined;
  // if autosize
  @Input() autoSize: boolean = false;
  // minimum charactere space if autoSize
  @Input() minChar: number = 5;

  @Input() secret: boolean = false;
  @Input() isRegExp: boolean = false;

  @Input() onlyDisplay: boolean = false;

  @Input() autoFocus: boolean = false;

  // element Ref of our input and mesure hidden span for autosize
  @ViewChild('input') set inputEl(el: ElementRef<HTMLInputElement>)
  {
    this._inputEl = el;
    
    if(this.autoFocus === true && el !== undefined && el.nativeElement !== undefined)
    {
      el.nativeElement.focus();
    }
  }

  get inputEl(): ElementRef<HTMLInputElement>
  {
    return this._inputEl;
  }

  _inputEl: ElementRef<HTMLInputElement>;

  @ViewChild('measure') measureEl: ElementRef<HTMLSpanElement>;

  regExpModal: ComponentRef<RegExpModalComponent>;


  // text property
  _text: string = '';

  // diabled boolean
  disabled: boolean = false;

  showSecret = false;


  /**
   * text setter
   */
  set text(text:string)
  {
    // if text has changed and is not undefined
    if(text !== undefined && this._text !== text)
    {
      // assign text and call functions of controlValue Accessor
      this._text = text;
      this.onChange(text);


      // if set to autosize and our element refs are not undefined
      if(this.autoSize && this.inputEl?.nativeElement && this.measureEl.nativeElement)
      {
        // replace space with non break space (doesn't work isn't a priority)
        // autosize
        this.autoResize();
      }
    }
  }

  private autoResize()
  {
    // if text is not undefined and not null
    if(this._text !== undefined && this._text !== null)
    {
      // replace space with '-' (about same size char) since space don't take... well space... in our reference div
      // and assign our reference span text content the result
      this.measureEl.nativeElement.textContent = this._text.split(' ').join('-');
      // use this size reference to assign our input a width
      this.inputEl.nativeElement.style.setProperty('width',
        (this._text.length >= this.minChar - 1 ? `calc(${this.measureEl.nativeElement.offsetWidth}px + ${0.5}ch)` : `${this.minChar + 0.5}ch`));
    }
  }

  /**
   * text getter
   */
  get text(): string
  {
    return this._text;
  }


  constructor(private modalService: ModalService) {}

  /**
   * after init call autosize if needed
   */
  ngAfterViewInit(): void
  {
    if(this.autoSize)
    {
      this.autoResize();
    }
    if(this.secret)
    {
      this.inputEl.nativeElement.type = 'password';
    }
  }

  /**
   * value getter
   */
  get value(): string
  {
    return this.text;
  }

  /**
   * resize event
   */
  resize(event: KeyboardEvent)
  {
    // resize depending on key pressed if autosize is true
    if(this.autoSize && this.inputEl?.nativeElement && this.measureEl.nativeElement)
    {
      this.measureEl.nativeElement.innerText = this.text + (event.key.length == 1 ? event.key : '');
      this.inputEl.nativeElement.style.setProperty('width', this.text.length >= this.minChar - 1 ? `calc(${this.measureEl.nativeElement.offsetWidth}px + ${event.key === 'Backspace' ? 0 : 0.5}ch)` : `${this.minChar + 0.5}ch`)
    }
  }

  onBlur()
  {
    this.onTouched();
  }

  async openRegExModal()
  {
    this.regExpModal = <ComponentRef<RegExpModalComponent>>(await this.modalService.instanciateModal(RegExpModalComponent));
    this.regExpModal.instance.regExpValue = this.value;

    this.regExpModal.instance.action = (newValue) =>
    {
      this.text = newValue;
    }

      this.regExpModal.onDestroy(()=>
      {
        this.regExpModal = undefined;
      });
  }

  onSecretStateChange()
  {
    this.showSecret = !this.showSecret;
    this.inputEl.nativeElement.type = this.showSecret ? 'text' : 'password';
  }



  /**
   * ControlValueAccessor interface
   *
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */


  // Function to call when text change
  onChange = (text: string) => {};
  // Function to call when the input is touched
  onTouched = () => {};

  writeValue(text: string): void
  {
    this.text = text;
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }
}
