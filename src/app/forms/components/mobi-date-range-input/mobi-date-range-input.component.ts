/* eslint-disable */
import { Component, ElementRef, EventEmitter, forwardRef, INJECTOR, Injector, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { locale, MbscLocale, momentTimezone } from '@mobiscroll/angular';
import { DatepickerBase, MDate } from '@mobiscroll/angular/dist/js/core/components/datepicker/datepicker';
import { TranslateService } from '@ngx-translate/core';
import { ResizeEvent } from '@nororljos/nrs-directives';
import { Subscription } from 'rxjs';
import * as moment from 'moment-timezone';
import { consoleEnv } from '@nororljos/environments';
import { TimezoneService } from '@nororljos/nrs-angular-util';




momentTimezone.moment = moment;

@Component({
  selector: 'forms-mobi-date-range-input',
  templateUrl: './mobi-date-range-input.component.html',
  styleUrls: ['./mobi-date-range-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MobiDateRangeInputComponent),
      multi: true,
    },
  ],
})
export class MobiDateRangeInputComponent implements OnDestroy, ControlValueAccessor
{

  @Input() asTimeStamp: boolean = false;

  @Input() fromSec: number = 0;

  @Input() fromMilisec: number = 0;

  @Input() toSec: number = 0;

  @Input() toMilisec: number = 0;

  @Input() autoSize: boolean = true;

  @Input() maxRange: number = undefined;

  @Input() minRange: number = undefined;

  @Input()
  set time(time: boolean)
  {
    const index = this.controls.findIndex(v => {return v === 'time'});

    if(time)
    {
      if(index === -1)
      {
        this.controls.push('time')
      }
    }
    else
    {
      if(index !== -1)
      {
        this.controls.splice(index, 1)
      }
    }
  }

  @Input()
  set startFrom(start: number | Date)
  {
    if(typeof start === 'number')
    {
      this._startFrom = new Date(start);
    }
    else
    {
      this._startFrom = start;
    }

    this._startFromNoSec = new Date(this._startFrom);
    this._startFromNoSec.setSeconds(0, 0);
  }

  _startFrom: Date;

  _startFromNoSec: Date;

  @Input()
  set endAt(end: number | Date)
  {
    if(typeof end === 'number')
    {
      this._endAt = new Date(end);
    }
    else
    {
      this._endAt = end;
    }

    this._endAtNoSec = new Date(this._endAt);
    this._endAtNoSec.setSeconds(0, 0);
  }

  _endAt: Date;

  _endAtNoSec: Date;


  @Output() dateRange: EventEmitter<Date[] | number[]> = new EventEmitter<Date[] | number[]>();

  @ViewChild('cc') cc: ElementRef<HTMLDivElement>;

  public momentPlugin = momentTimezone;

  disabled: boolean = false;

  instance: DatepickerBase;

  dates: Date[];

  controls: string[] = ['calendar'];

  locales: {[key: string]: MbscLocale} = locale;

  locale: MbscLocale;

  $language: Subscription;


  displayTimezone: string = 'utc';
  dateFormat: string;
  timeFormat: string;

  constructor(
    private translate: TranslateService,
    private timezoneService: TimezoneService)
  {
    this.updateLanguage(translate.currentLang);

    this.$language = this.translate.onLangChange.subscribe(lang =>
    {
      this.updateLanguage(lang.lang);
    });


    this.timezoneService.timeZoneChangedEvent.subscribe(tz =>
    {
       this.displayTimezone = tz;
    });

    this.dateFormat = this.timezoneService.dateFormat;
    this.timeFormat = this.timezoneService.hourMinutesTimeFormat;
    //this.timeWheels = this.timezoneService.selectedDateFormat === 'iso' || !moment.localeData().isPM ? "HH:mm" : "hh:mm";
  }

  updateLanguage(lang: string)
  {
    switch(lang)
    {
      case 'en':
        this.locale = this.locales['en-GB'];
        break;
      case 'fr':
        this.locale = this.locales[lang];
        this.locale.rangeStartLabel = 'Début';
        break;
      default:
        this.locale = this.locales[lang];
        break;
    }
  }


  ngOnDestroy(): void
  {
    this.$language.unsubscribe();
  }


  //! MDate === string in our case, replacing mdate to string will result in compilation error for some reason
  onMobiChange(data: MobiDatePickerChangeEvent<MDate[]>)
  {
    // window['testD'] = data.value

    this.dates = data.value.map(m => m == undefined ? undefined : new Date(m));


    if(this.dates[0])
    {
      this.dates[0].setSeconds(this.fromSec, this.fromMilisec);

      if(this._startFrom)
      {
        const noSec = new Date(this.dates[0]);
        noSec.setSeconds(0, 0);

        if(noSec.getTime() === this._startFromNoSec.getTime())
        {
          this.dates[0].setTime(this._startFrom.getTime());
        }
      }
    }

    if(this.dates[1])
    {
      this.dates[1].setSeconds(this.toSec, this.toMilisec);

      if(this._endAt)
      {
        const noSec = new Date(this.dates[1]);
        noSec.setSeconds(0, 0);

        if(noSec.getTime() === this._endAtNoSec.getTime())
        {
          this.dates[1].setTime(this._endAt.getTime());
        }
      }
    }

    this.onChange(this.asTimeStamp ? this.dates.map(d => (d != undefined ? d.getTime() : undefined)) : this.dates);

    this.dateRange.emit(this.dates);
  }


  onMobiInit(event: {inst: DatepickerBase, type: string})
  {
    this.instance = event.inst;
    const tz = localStorage.getItem('tz');
    this.displayTimezone  = tz ? tz : 'utc';

    if(this.dates !== undefined && this.dates.length === 2)
    {
      this.instance.setVal(this.dates);
    }
  }



  onResize(event: ResizeEvent)
  {
    if(this.autoSize)
    {
      if(event.newWidth / event.newHeight < (480 / 382))
      {
        this.cc.nativeElement.style.setProperty('transform', `scale(${event.newWidth/480})`);
      }
      else
      {
        this.cc.nativeElement.style.setProperty('transform', `scale(${event.newHeight/382})`);
      }
    }
  }

  /**
   * ControlValueAccessor interface
   *
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */


  // Function to call when text change
  onChange = (dates: Date[] | number[]) => {};
  // Function to call when the input is touched
  onTouched = () => {};

  writeValue(dates: Date[] | number[] | undefined): void
  {
    const asDate: Function = (date: Date | number | string) =>
    {
      switch(typeof date)
      {
        case 'object':
          return date instanceof Date ? date : (moment.isMoment(date) ? (<moment.Moment>date).toDate() : undefined);
        case 'number':
        case 'string':
          return new Date(date);
      }
    }
    if(Array.isArray(dates) && dates.length >= 1)
    {
      this.dates = [asDate(dates[0]), Date.length >= 2 ? asDate(dates[1]) : undefined];

      if(this.instance)
      {
        setTimeout(() =>
        {
          this.instance.setVal(this.dates);
        });
      }
      else
      {
        console.info('%c - No instance yet', consoleEnv.error);
      }
    }
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }
}


export interface MobiDatePickerChangeEvent<DateType = Date>
{
  inst: DatepickerBase
  type: string;
  value: DateType;
  valueText: string;
}
