import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobiDateRangeInputComponent } from './mobi-date-range-input.component';

describe('MobiDateRangeInputComponent', () => {
  let component: MobiDateRangeInputComponent;
  let fixture: ComponentFixture<MobiDateRangeInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobiDateRangeInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobiDateRangeInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
