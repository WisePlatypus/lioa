import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'forms-range-input',
  templateUrl: './range-input.component.html',
  styleUrls: ['./range-input.component.scss']
})
export class RangeInputComponent implements OnInit 
{
  @Output()
  using: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  valueChange: EventEmitter<number> = new EventEmitter<number>();
  
  @Input()
  value: number = 50;

  @Input()
  min: number = 0;

  @Input()
  max: number = 100;



  @ViewChild('track') trackEl: ElementRef<HTMLDivElement>;


  dragging: boolean = false;


  constructor() { }

  ngOnInit(): void {}

  
  @HostListener('mousedown', ['$event'])
  dragEnter(mouseEvent: MouseEvent)
  {
    this.dragging = true;
    this.using.emit(this.dragging);
    this.updateCursor(mouseEvent)
  }

  
  @HostListener('document:mouseup', ['$event'])
  dragLeave(mouseEvent: MouseEvent)
  {
    if(this.dragging)
    {
      this.updateCursor(mouseEvent)
      
      this.dragging = false;
      this.using.emit(this.dragging);
    }
  }


  @HostListener('document:mousemove', ['$event'])
  onDrag(mouseEvent)
  {
    if(this.dragging)
    {
      this.updateCursor(mouseEvent);
    }
  }
  
  updateCursor(mouseEvent: MouseEvent = undefined)
  {
    this.value = this.stayInRange((mouseEvent.clientX - this.trackEl.nativeElement.getBoundingClientRect().left) / this.trackEl.nativeElement.offsetWidth, 0.0, 1.0)  * this.deltaRange;

    this.valueChange.emit(this.value);
  }

  stayInRange(number, min, max)
  {
    return (number > max ? max : (number < min ? min : number)); 
  }

  get deltaRange()
  {
    return this.max - this.min;
  }

  get percentageStr()
  {
    return `${this.value * 100 / this.deltaRange}%`;
  }

}
