/* eslint-disable */
import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { FormGroup } from '@ngneat/reactive-forms';
import { TMessage, TElement, TGroup, TItem, TLayout, TArray, TDisplayText } from '../../models/template';
import { TemplateService } from '../../services/template.service';

@Component({
  selector: 'forms-template-element',
  templateUrl: './template-element.component.html',
  styleUrls: ['./template-element.component.scss']
})
export class TemplateElementComponent implements OnInit 
{

  @Input()
  set displayed(displayed: boolean)
  {
    if(displayed !== undefined)
    {
      // actually send a disable signal to avoid invalidity conflicts
      setTimeout(() => 
      {
        this.message = {type: 'enable', value: displayed};
      }, 0);
    }
  }

  @Input() 
  set enabled(enabled: boolean)
  {
    if(enabled !== undefined)
    {
      setTimeout(() => 
      {
        this.message = {type: 'enable', value: enabled};
      }, 0);
    }
  }

  @Input() 
  set itemFieldCs(itemFieldCs: {val: boolean, fieldPath: string,  affectedFieldPath: string, trueVal: any, falseVal: any}[])
  {
    if(itemFieldCs !== undefined)
      {   
        itemFieldCs.forEach(itemFieldC =>
        {
          // we search through template tree for the given value
          if(itemFieldC.affectedFieldPath !== "value")
          {
            const s = itemFieldC.fieldPath.split('.')          
            let field = this.templateElement;
            s.forEach((fieldStr, i) => 
              { 
                if(i === s.length - 1)
                {
                  field[fieldStr]= itemFieldC.val ? itemFieldC.trueVal : itemFieldC.falseVal;
                }
                else
                {
                  field = (field[fieldStr] !== undefined ? field[fieldStr] : field);
                }
              });  
              this.templateElement['value'] = this.templateElement['value'];   
          }
          //If we want value we have to acces it through control form
          else
          {
            const val= itemFieldC.val ? itemFieldC.trueVal : itemFieldC.falseVal;
            const c = this.parentFormGroup.getControl((<TItem>this.templateElement).name);
            if(c !== null && val !== c?.value)
            {           
              c.setValue(val);
            }
          }
        });
    }
  }

  @Input()
  set message(msg: TMessage)
  {
    this._message = msg;
  }

  get message():TMessage
  {
    return this._message;
  }

  _message: TMessage;

  @Input()
  set templateElement(te: TElement)
  {
    // if(this.template.isFinal(te))
    // {
    if(typeof te?.fz === 'number' && te.fz > 0)
    {
      this.elRef.nativeElement.style.setProperty('font-size', `${te.fz}em`)
    }
    this._templateElement = te;
    // }
  }

  get templateElement(): TElement
  {
    return this._templateElement;
  }
  
  
  _templateElement: TElement = undefined;

  @Input()
  set parentFormGroup(formGroup: FormGroup)
  {
    this._parentFormGroup = formGroup;
  }

  get parentFormGroup(): FormGroup
  {
    return this._parentFormGroup;
  }

  _parentFormGroup: FormGroup;

  constructor(private elRef: ElementRef<HTMLElement>, private template: TemplateService) { }

  ngOnInit(): void {
  }

  get asGroup(): TGroup
  {
    return <TGroup>this.templateElement;
  }

  get asItem(): TItem
  {
    return <TItem>this.templateElement;
  }

  get asLayout(): TLayout
  {
    return <TLayout>this.templateElement;
  }

  get asArray(): TArray
  {
    return <TArray>this.templateElement;
  }

  get asTextDisplay(): TDisplayText
  {
    return <TDisplayText>this.templateElement;
  }
}
