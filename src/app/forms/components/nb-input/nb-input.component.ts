/* eslint-disable */
import { Component, ElementRef, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { getCarretPosition, setCarretPosition } from '@nororljos/nrs-util';

@Component(
{
  selector: 'forms-nb-input',
  templateUrl: './nb-input.component.html',
  styleUrls: ['./nb-input.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR, 
      useExisting: 
      forwardRef(() => NbInputComponent),
      multi: true,
    },
  ]
})
export class NbInputComponent implements OnInit, ControlValueAccessor 
{

  readonly pregNumber: string = '(\\+|-)?[0-9]+((\\.|,)[0-9]+)?';

  @ViewChild('editable') editableEl: ElementRef <HTMLDivElement>;

  @Input()
  suffixRegExp: string = '';

  @Input()
  displayResetOnDisable: boolean = false;

  @Input()
  integer: boolean = false;

  @Input()
  hardStep: boolean = false;

  @Input()
  set enabled(enabled: boolean)
  {
    if (!enabled)
    {
      if (this.displayResetOnDisable)
      {
        this.displayValue(Math.min(Math.max(this.defaultValue, this._min), this._max));
      }
    }
    else
    {
      this.displayValue(this.getValMinMax(this._value || this.defaultValue || 0));
      this.onChange(this.getValMinMax(this._value || this.defaultValue || 0));
    }
    this._enabled = enabled;
  }

  get enabled(): boolean
  {
    return this._enabled;
  }

  _enabled: boolean = true;

  @Input()
  onlyDisplay: boolean = false;

  @Input()
  set defaultSuffix(ds: string)
  {
    if(ds)
    {
      this._defaultSuffix = ds;
    }
  }
  
  _defaultSuffix: string = '';

  @Input()
  forcePositive: boolean = false;


  @Input()
  set step(step: number)
  {
    if(step !== undefined || typeof step === 'number' && step > 0)
    {
      this._step = step;
      this._precision = this.findPrecision(this._step)
    }
  }

  _step: number = 1;
  _precision = 0;

  @Input()
  set min(min: number)
  {
    if(min !== undefined && !isNaN(min) && typeof min === 'number')
    {
      this._min = min;
      this._value = Math.max(this._value, this._min);
      this.displayValue(this._value);
      this.onChange(this._value);
    }
  }

  _min: number = -Infinity;

  @Input()
  set max(max: number)
  {
    if(max !== undefined && !isNaN(max) && typeof max === 'number')
    {
      this._max = max;
      this._value = Math.min(this._value, this._max);
      this.displayValue(this._value);
      this.onChange(this._value);
    }
  }

  _max: number = Infinity;


  @Input()
  defaultValue: number = 0;

  // @Output()
  // change: EventEmitter < number > = new EventEmitter < number > ();

  model: string = '0';
  valid: boolean = true;

  // lastCarret: number = 0;

  defaultRegExp: RegExp;
  numberRegExp: RegExp = /^(\+|-)?[0-9]+((\.|,)[0-9]+)?$/;

  _value: number = 0;

  selection: Selection;

  constructor()
  {
    this.selection = window.getSelection();
  }


  ngOnInit(): void
  {
    this.defaultRegExp = new RegExp(`^${this.pregNumber}${this.suffixRegExp?this.suffixRegExp:this._defaultSuffix}$`);
    this._value = this.getValMinMax(this.defaultValue || 0);
    this.displayValue(this._value);
  }


  onKeyPress(event: KeyboardEvent)
  {
    if (!this.enabled || event.key === 'Enter')
    {
      event.preventDefault();
    }
  }


  onKeyUp(event: KeyboardEvent)
  {
    if (this.enabled)
    {
      const next = this.model;

      // if there's only a number
      if(this.numberRegExp.test(next))
      {
        //emit change and add sufix
        this.verifyNumber(next.replace(',', '.'), true);
        this.valid = true;
      }
      // if there's a fullmatch on number + suffix regexp
      else if (this.defaultRegExp.test(next))
      {
        // emit only number
        this.valid = true;
        this.verifyNumber(next.match(this.pregNumber)[0].replace(',', '.'), true);
      }
      else
      {
        this.valid = false;
      }
    }
  }

  
  onInput(event: InputEvent)
  {
    const carret = getCarretPosition(this.editableEl.nativeElement);

    // @ts-ignore
    this.model = event.target.textContent;
     
    //- apply fefore next frame
    requestAnimationFrame(()=>
    {
      try
      {
        setCarretPosition(this.editableEl.nativeElement, carret);
      }
      catch{}
    })
  }

  verifyNumber(str: string, rewrite: boolean = false)
  {
    let number: number = Number(str);

    if (!isNaN(number))
    {
      if (this._max !== undefined && this._max < number)
      {
        // this.displayValue(this._max);
        number = this._max;
      }
      else if (this._min !== undefined && this._min > number)
      {
        // this.displayValue(this._min);
        number = this._min;
      }

      if(this.integer && number !== Math.round(number))
      {
        number = Math.round(number);
        // this.displayValue(number);
      }

      if(rewrite)
      {
        this._value = number;
      }
      this.onChange(number);
    }
  }

  stepUp()
  {
    if (this.enabled)
    {
      this._value = Math.min(this._value + this._step, this._max);
      this.displayValue(this._value);
      this.onChange(this._value);
    }
  }

  stepDown()
  {
    if (this.enabled)
    {
      this._value = Math.max(this._value - this._step, this._min);
      this.displayValue(this._value);
      this.onChange(this._value);
      this.onTouched();
    }
  }


  onFocusOut()
  {
    if(this.hardStep === true)
    {
      this._value = Math.round(this._value / this._step) * this._step;
    }

    this.displayValue(this._value);
    this.onChange(this._value);
    this.onTouched();
  }

  displayValue(value)
  {
    this.model = `${value == 0 ? '0' : value.toFixed(this._precision)}`
  }


  findPrecision(number: number, level: number = 0)
  {
    if(number >= 1)
    {
      return level;
    }
    else
    {
      return this.findPrecision(number * 10, level+1)
    }
  }

  getValMinMax(val: number): number
  {
    return Math.min(Math.max(val, this._min), this._max)
  }


  
  /**
   * ControlValueAccessor interface
   *  
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */ 


  // Function to call when text change 
  onChange = (nb: number) => {
  };
  // Function to call when the input is touched 
  onTouched = () => {};

  writeValue(nb: number): void
  {
    if(typeof nb === 'number')
    {
      this._value = nb;
    }
    else if (typeof nb === 'undefined')
    {
      this._value = 0;
    }
    else
    {
      const tnb = Number(nb)
      if(!Number.isNaN(tnb))
      {
        this._value = tnb;
      }
      else
      {
        this._value = 0;
      }
    }
    this._value = this.getValMinMax(this._value)
    
    this.displayValue(this._value);
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this._enabled = !isDisabled;
  }
}