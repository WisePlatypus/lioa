import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NbInputComponent } from './nb-input.component';

describe('NbInputComponent', () => {
  let component: NbInputComponent;
  let fixture: ComponentFixture<NbInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NbInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NbInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
