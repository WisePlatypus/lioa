/* eslint-disable */
import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { extractNumber } from '@nororljos/nrs-util';
import { ModalComponent } from '@nororljos/nrs-modal';
import { TemplateService } from '../../services/template.service';
import { TemplateListElement } from '../../models/template';

@Component({
  selector: 'app-modal-drop-down',
  templateUrl: './modal-drop-down.component.html',
  styleUrls: ['./modal-drop-down.component.scss']
})
export class ModalDropDownComponent extends ModalComponent implements AfterViewInit  
{
  readonly displayedClip: string = 'polygon(-10% -10%, 110% -10%, 110% 110%, -10% 110%)';
  
  
  @ViewChild('content') content: ElementRef<HTMLDivElement>;
  
  
  public elements: TemplateListElement[];
  public selected: number = -1;

  public highlight: boolean = true;

  public relativeEl: HTMLElement;
  
  public set vertical(v: 'top' | 'bottom' | 'middle')
  {
    switch(v)
    {
      case 'top':
        this.hiddenClip = 'polygon(0 100%, 100% 100%, 100% 100%, 0 100%)';
        break;
      case 'middle':
      default:
        this.hiddenClip = 'polygon(0 50%, 100% 50%, 100% 50%, 0 50%)';
        break;
      case 'bottom':
        this.hiddenClip = 'polygon(0 0, 100% 0, 100% 0, 0 0)';
        break;
    }
    
    this.dropClipPath = this.hiddenClip;

    this._vertical = v;
  }

  
  public get vertical(): 'top' | 'bottom' | 'middle'
  {
    return this._vertical;
  }
  
  private _vertical: 'top' | 'bottom' | 'middle' = 'middle';

  public horizontal: 'right' | 'left' | 'from-right' | 'from-left' | 'middle' = 'from-left';
  public fontSize: string = '10px';
  
  public padding: string = '0.2em 0.5em 0.2em 0.2em';

  public ihneritWidth: boolean = true;

  public hiddenClip: string = 'polygon(0px 50%, 100% 50%, 100% 50%, 0px 50%)';
  public onSelect: (number) => void;

  public notOverRect: boolean = false;
  
  dropRect: {width: number, height: number} = {width: 0, height: 0};
  dropCoo: {x: number, y:number} = {x: 0, y: 0}
  

  set dropClipPath(clipPath: string)
  {
    if(this.content)
    {
      this.content.nativeElement.style.setProperty('clip-path', clipPath);
    }
  }

  set dropMinWidth(width: number)
  {
    if(this.content)
    {
      this.content.nativeElement.style.setProperty('min-width', `${width}px`);
    }
  }


  constructor(private template: TemplateService, private elRef: ElementRef<HTMLElement>) 
  {
    super();
  }

  select(id: number)
  {
    if(!(this.elements[id] as any).disabled)
    {

      if(this.onSelect)
      {
        this.onSelect(id);
      }

      this.diseapear();
    }
  }

  ngAfterViewInit (): void 
  {
    //- after animation frame we should have received all parameters (one event loop does the job as well)
    requestAnimationFrame(()=>
    {
      // set min wisth as relative element's width
      if(this.ihneritWidth)
      {
        this.dropMinWidth = this.relativeEl.offsetWidth;
      }
      
      // set font size
      this.elRef.nativeElement.style.setProperty('font-size', this.fontSize)
      const relativeBounds = this.relativeEl.getBoundingClientRect();

      // compute drop list rect (width and height)
      this.dropRect.width = extractNumber(window.getComputedStyle(this.content.nativeElement).width);
      this.dropRect.height = extractNumber(window.getComputedStyle(this.content.nativeElement).height);

      // set horizontal pos
      switch(this.horizontal)
      {
        case 'left':
          this.dropCoo.x = relativeBounds.left - this.dropRect.width;
          break;
        case 'from-left':
          this.dropCoo.x = relativeBounds.left;
          break;
        case 'middle':
        default:
          this.dropCoo.x = relativeBounds.left + (this.relativeEl.offsetWidth / 2) - (this.dropRect.width / 2);
          break;
        case 'from-right':
          this.dropCoo.x = relativeBounds.right - this.dropRect.width;
          break;
        case 'right':
          this.dropCoo.x = relativeBounds.left + this.relativeEl.offsetWidth;
          break;
      }

      if(!this.notOverRect)
      {
        // set vertical pos
        switch(this.vertical)
        {
          case 'top':
            this.dropCoo.y = relativeBounds.top - this.dropRect.height;
            break;
          case 'middle':
          default:
            this.dropCoo.y = relativeBounds.top + (this.relativeEl.offsetHeight / 2) - (this.dropRect.height / 2);
            break;
          case 'bottom':
            this.dropCoo.y = relativeBounds.top + this.relativeEl.offsetHeight;
            break;
        }
  
        // correct pos if it goes out of screen
        this.forceInView();

        this.content.nativeElement.style.setProperty('top', `${this.dropCoo.y}px`);
      }
      else
      {
        if(relativeBounds.top < relativeBounds.bottom - window.innerHeight)
        {
          this.dropCoo.y = relativeBounds.top;
          this.relativeEl.style.setProperty('max-height', `${relativeBounds.top}px`);
          
          this.content.nativeElement.style.setProperty('bottom', `${this.dropCoo.y}px`);
        }
        else
        {
          this.dropCoo.y = relativeBounds.bottom;
          this.relativeEl.style.setProperty('max-height', `${relativeBounds.bottom - window.innerHeight}px`);  
          
          this.content.nativeElement.style.setProperty('top', `${this.dropCoo.y}px`);
        }
      }

      
      // assign position to element
      this.content.nativeElement.style.setProperty('left', `${this.dropCoo.x}px`);

      //- display element
      this.dropClipPath = this.displayedClip;
    });
  }


  /**
   ** force the drop down element to be in the view
   * @returns 
   */
  forceInView(): boolean
  {
    let changed = false;

    if(this.dropCoo.y + this.dropRect.height > window.innerHeight)
    {
      this.dropCoo.y = window.innerHeight - this.dropRect.height;
      changed = true;
    }
    else if(this.dropCoo.y < 0)
    {
      this.dropCoo.y = 0;
      changed = true;
    }

    if(this.dropCoo.x + this.dropRect.width > window.innerWidth)
    {
      this.dropCoo.x = window.innerWidth - this.dropRect.width;
      changed = true;
    }
    else if(this.dropCoo.x < 0)
    {
      this.dropCoo.x = 0;
      changed = true;
    }

    return changed;
  }

  /**
   ** destroy after clip animation
   */
  diseapear()
  {
    this.dropClipPath = this.hiddenClip;
    setTimeout(() => {
      this.componentRef.destroy();
    }, 300);
  }




  getValueDisplay(id: number): string
  {
    const element = this.elements[id];
    return element !== undefined ? typeof element !== 'object' ? element : (element.alias !== undefined ? this.template.getTranslation(element.alias) : element.value) : '';
  }

}