import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDropDownComponent } from './modal-drop-down.component';

describe('ModalDropDownComponent', () => {
  let component: ModalDropDownComponent;
  let fixture: ComponentFixture<ModalDropDownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalDropDownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDropDownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
