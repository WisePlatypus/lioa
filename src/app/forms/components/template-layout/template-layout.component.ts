/* eslint-disable */
import { Component, Input, OnInit, Output } from "@angular/core";
import { FormGroup } from '@ngneat/reactive-forms';
import { getValueWithPath } from "@nororljos/nrs-util";
import { Subscription } from "rxjs";
import { TMessage, TLayout, TCondition, TNamable, TElement, TInput, TItem } from "../../models/template";
import { TemplateService } from "../../services/template.service";


@Component({
  selector: 'forms-template-layout',
  templateUrl: './template-layout.component.html',
  styleUrls: ['./template-layout.component.scss']
})
export class TemplateLayoutComponent implements OnInit 
{

  @Input()
  set message(msg: TMessage)
  {
    if(msg !== undefined)
    {
      for(let i = 0; i < this.messages.length; i++)
      {
        this.messages[i] = msg
      }
    }
  }

  get messages():TMessage[]
  {
    return this._messages;
  }

  _messages: TMessage[];

  

  @Input()
  set parentFormGroup(formGroup: FormGroup)
  {
    this._parentFormGroup = formGroup;
  }

  get parentFormGroup(): FormGroup
  {
    return this._parentFormGroup;
  }

  _parentFormGroup: FormGroup;
  
  
  @Input()
  set templateLayout(tl: TLayout)
  {
    if(tl !== undefined && tl?.children !== undefined)
    {
      this._templateLayout = tl;
      this._messages = Array(tl.children.length);
    }
  }

  get templateLayout(): TLayout
  {
    return this._templateLayout;
  }
  
  _templateLayout: TLayout;

  
  
  $groupValue: Subscription;

  conditionsEnable: {[key: string]: boolean} = {};
  conditionsDisplay: {[key: string]: boolean} = {};
  conditionsItemField: {[key: string]: {val: boolean, testedFieldPath: string, affectedFieldPath: string, trueVal: any, falseVal: any}[]} = {};
  conditions: {[key: string]: {condition: TCondition, nammable: TNamable, valuePath: string[], valueTest: boolean}[]} = {};

  constructor(private template: TemplateService) { }

  /**
   * on init check if an element has a condition and register to trackthem
   */
  ngOnInit(): void 
  {
    if(this.templateLayout !== undefined && this.templateLayout.children !== undefined)
    {
      // if there are conditions
      for(const [id, element] of this.templateLayout.children.entries())
      {
        if(element.conditions)
        {
          // if conditions aren't tracked / handled
          if(this.conditions[id] === undefined)
          {
            // register a new condition array and validation value
            this.conditions[id] = [];
  
           
    
            // for each condition find item by name and register namable, condition and value path
            for(const condition of element.conditions)
            {
              if((condition?.mode === 'display' || condition?.mode === undefined) && this.conditionsDisplay[id] === undefined)
              {
                this.conditionsDisplay[id] = false;
              }
              if(condition?.mode === 'enable' && this.conditionsEnable[id] === undefined)
              {
                this.conditionsEnable[id] = false;
              }
              if(condition?.mode === 'itemField' && this.conditionsItemField[id] === undefined)
              {
                this.conditionsItemField[id] = [];
              }
  
              const nammableData = this.template.findNammable(<TElement>{type: 'layout', children: this.templateLayout.children}, condition.item);
              
              if(nammableData !== undefined)
              {
                this.conditions[id].push({condition: condition, nammable: nammableData.namable, valuePath: condition?.testedFieldPath !== undefined && condition?.testedFieldPath !== 'value' ? condition.testedFieldPath.split('.') : nammableData.path, valueTest : condition?.testedFieldPath === undefined || condition?.testedFieldPath === 'value'});
              }
            }
  
            // if no value subscription then sub
            if(this.$groupValue === undefined)
            {
              this.$groupValue = this._parentFormGroup.valueChanges.subscribe(v => this.valueWatcher(v))
            }
          }
        }
      }
    }
  }


  /**
   * check condition display validity
   * @param id 
   * @returns 
   */
  conditionDisplay(id: number)
  {
    return this.conditionsDisplay[id];
  }

  /**
   * check condition Enable validity
   * @param id 
   * @returns 
   */
  conditionEnable(id: number)
  {
    return this.conditionsEnable[id];
  }


    /**
 * check condition Enable validity
 * @param id 
 * @returns 
 */
  conditionItemField(id: number): {val: boolean, affectedFieldPath: string, testedFieldPath: string,  trueVal: any, falseVal: any}[]
  {
    return this.conditionsItemField[id];    
  }


  valueWatcher(values: any): void
  {
    for(const [key, conditions] of Object.entries(this.conditions))
    {
      let conditionDisplay = undefined;
      let conditionEnable = undefined;
      let conditionsItemField = [];
      
      conditions.forEach((condition, i) =>
      {
        const conditionFullfilled = this.template
        .testCondition(
          condition.condition, 
          (<TInput>(<TItem>condition.nammable).interface).input, 
          getValueWithPath(condition.valueTest ? values : (<TItem>condition.nammable), condition.valuePath)
        );
          
        if(condition.condition.mode === 'enable')
        {
          conditionEnable = conditionEnable === undefined ? conditionFullfilled : conditionEnable && conditionFullfilled;
        }
        else if(condition.condition.mode === 'display')
        {
          conditionDisplay = conditionDisplay === undefined ? conditionFullfilled : conditionDisplay && conditionFullfilled;
        }
        else if(condition.condition.mode === 'itemField')
        {
          conditionsItemField.push({val: conditionsItemField[i]?.val === undefined ? conditionFullfilled : conditionDisplay && conditionFullfilled, 
            testedFieldPath: condition.condition.testedFieldPath, affectedFieldPath: condition.condition.affectedFieldPath, trueVal: condition.condition.trueVal, falseVal: condition.condition.falseVal});
        }
      });

      if(conditionDisplay !== undefined)
      {
        this.conditionsDisplay[key] = conditionDisplay;
      }

      if(conditionEnable !== undefined)
      {
        this.conditionsEnable[key] = conditionEnable;
      }

      if(conditionsItemField.length > 0)
      {
        this.conditionsItemField[key] = conditionsItemField;
      }
    }
  }



  get layout(): string
  {
    return this._templateLayout !== undefined && this._templateLayout.layout !== undefined ? this._templateLayout.layout : '';
  }



  get children(): TElement[]
  {
    return this._templateLayout !== undefined && this._templateLayout.children !== undefined ? this._templateLayout.children : [];
  }


  islastel(index: number): boolean
  {
    let i = this.children.length - 1;
    
    while('interface' in this.children[i] && this.conditionDisplay[i])
    {
      i--;
    }

    return i === index;
  }
}
