import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestDropDownComponent } from './suggest-drop-down.component';

describe('SuggestDropDownComponent', () => {
  let component: SuggestDropDownComponent;
  let fixture: ComponentFixture<SuggestDropDownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuggestDropDownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestDropDownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
