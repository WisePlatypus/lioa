/* eslint-disable */
import { Component, ComponentRef, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@ngneat/reactive-forms';
import { ModalService } from '@nororljos/nrs-modal';
import { getCarretPosition, partialDeepCompare, setCarretPosition } from '@nororljos/nrs-util';
import { TemplateListElement, TTranslatable } from '../../models/template';
import { TemplateService } from '../../services/template.service';
import { ModalDropDownComponent } from '../modal-drop-down/modal-drop-down.component';

@Component({
  selector: 'forms-suggest-drop-down',
  templateUrl: './suggest-drop-down.component.html',
  styleUrls: ['./suggest-drop-down.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR, 
      useExisting: SuggestDropDownComponent,
      multi: true,
    },
  ],
})
export class SuggestDropDownComponent implements OnInit, ControlValueAccessor
{
  
  @Input() 
  set elements(els: TemplateListElement[])
  {
    this._elements = els;
    if(this.autoSelect)
    {
      if(this.selected < 0 || this.selected >= els.length)
      {
        this.select(0);
      }
      else
      {
        this.select(this.selected);
      }
    }
  }

  model: string;
  
  
  get elements(): TemplateListElement[]
  {
    return this._elements
  } 
  
  _elements: TemplateListElement[] = [];
  
  // selected suffix
  @Input() sSuffix: string = '';

  @Input() editable: boolean = false;
  
  @Input() autoSelect: boolean = true;

  // horizontal and vertical
  @Input() vertical: 'top' | 'bottom' | 'middle' = 'middle';
  @Input() horizontal: 'right' | 'left' | 'from-right' | 'from-left' | 'middle' = 'middle';
  @Input() padding: string = '0.2em 0.5em 0.2em 0.2em';


  @Output() change: EventEmitter<string> = new EventEmitter<string>()
  @Output() changeId: EventEmitter<number> = new EventEmitter<number>()

  @Input()
  set selected(nb: number)
  {
    this.select(nb, false);
  } 

  get selected(): number
  {
    return this._selected;
  }
  
  _selected: number = -1;


  @ViewChild('editableEl') 
  set editableEl(editable: ElementRef<HTMLInputElement>)
  {
    if(editable)
    {
      this._editableEl = editable;
    }
  }

  get editableEl(): ElementRef
  {
    return this._editableEl;
  }

  @ViewChild('cn') 
  set cn(cn: ElementRef<HTMLInputElement>)
  {
    if(cn)
    {
      this._cn = cn;
    }
  }
  
  _cn: ElementRef<HTMLDivElement> = undefined;
  _editableEl: ElementRef<HTMLDivElement> = undefined;

  disabled: boolean = false;
  dropRef: ComponentRef<ModalDropDownComponent>;
  

  constructor(private template: TemplateService, private pop: ModalService) { }

  ngOnInit(): void {}


  /**
   ** toggle drop down list
   */
  public async toggleDrop()
  {
    if(!this.dropRef)
    {
      // set dropref
      this.dropRef = await this.pop.instanciateModal<ModalDropDownComponent>(ModalDropDownComponent);

      //-set dropref as undefined on destroy
      this.dropRef.onDestroy(()=>
      {
        this.dropRef = undefined;
      });

      //- configure drop down modal

      //pass elements and selection
      this.dropRef.instance.elements = this.elements;
      this.dropRef.instance.selected = this.selected;

      // pass the relative element and set font size
      this.dropRef.instance.relativeEl = this._cn.nativeElement;
      this.dropRef.instance.fontSize = window.getComputedStyle(this._cn.nativeElement).fontSize;
      // console.log(window.getComputedStyle(this.elRef.nativeElement).fontSize)
      
      // set vertical, horizontal pos and padding
      this.dropRef.instance.vertical = this.vertical;
      this.dropRef.instance.horizontal = this.horizontal;
      this.dropRef.instance.padding = this.padding;

      // set on select callback
      this.dropRef.instance.onSelect = (selected) => 
      {
        this.select(selected);
      }
    }
  }


  /**
   ** set selection
   * @param selected 
   */
  select(selected: number, emit = true)
  {
    if(typeof selected === 'number')
    {
      this._selected = selected;

      this.model = this.getAliasByID(selected) !== '' ? this.getAliasByID(selected) + this.sSuffix : '...'
  
      if(emit)
      {
        this.onChange(this.getValueByID(this.selected));
        this.onTouched();
    
        this.change.emit(this.getValueByID(this.selected));
        this.changeId.emit(selected);
      }
    }
    else
    {
      this.select(Math.max(0, this._elements.findIndex((value) => {this.getValue(value) === selected}))) 
    }
  }

  getAliasByID(id: number): string
  {
    const element = this.elements[id];
    return element !== undefined ? typeof element !== 'object' ? element : (element.alias !== undefined ? this.template.getTranslation(element.alias) : element.value) : '';
  }


  getAlias(element): string
  {
    return element !== undefined ? typeof element !== 'object' ? element : (element.alias !== undefined ? this.template.getTranslation(element.alias) : element.value) : '';
  }

  getValueByID(id: number): string
  {
    const element = this.elements[id];
    return typeof element === 'object' ? element.value : element;
  }

  getValue(element)
  {
    return typeof element === 'object' ? element.value : element;
  }



  onFocusOut()
  {
    setTimeout(() => {
      if(!this.editable)
      {
        this.model = this.getAliasByID(this.selected) !== '' ? this.getAliasByID(this.selected) + this.sSuffix : '...';
      }
    });
  }

  onKeyPress(kev: KeyboardEvent) {}

  onInput(event: InputEvent)
  {
    const carret = getCarretPosition(this.editableEl.nativeElement);

    // @ts-ignore
    this.model = event.target.textContent;
     
    //- apply fefore next frame
    requestAnimationFrame(()=>
    {
      try
      {
        setCarretPosition(this.editableEl.nativeElement, carret);
      }
      catch{}
    })
  }

  async onKeyUp(kev: KeyboardEvent)
  {
    // @ts-ignore
    const matchingEls = this._elements.map((val, id) => 
    {
      return typeof val === 'object' ? {...val, id: id} : {alias: val, value: val, id: id}; 
    }).filter(element =>
    {
      return (new RegExp(`.*${this.model.toLowerCase()}.*`)).test(this.getAlias(element).toLowerCase());
    });


    if(matchingEls.length !== 0)
    {

      if(!this.dropRef)
      {
        // set dropref
        this.dropRef = await this.pop.instanciateModal<ModalDropDownComponent>(ModalDropDownComponent);

        //-set dropref as undefined on destroy
        this.dropRef.onDestroy(()=>
        {
          this.dropRef = undefined;
        });

        //- configure drop down modal

        //pass elements and selection
        
        this.dropRef.instance.notOverRect = true;
        this.dropRef.instance.elements = matchingEls;
        this.dropRef.instance.selected = this.selected;

        // pass the relative element and set font size
        this.dropRef.instance.relativeEl = this._cn.nativeElement;
        this.dropRef.instance.fontSize = window.getComputedStyle(this._cn.nativeElement).fontSize;
        // console.log(window.getComputedStyle(this._editable.nativeElement).fontSize)
        
        // set vertical, horizontal pos and padding
        this.dropRef.instance.vertical = this.vertical;
        this.dropRef.instance.horizontal = this.horizontal;
        this.dropRef.instance.padding = this.padding;

        // set on select callback
        this.dropRef.instance.onSelect = (selected) => 
        {
          this.select(matchingEls[selected].id);
        }
      }
      else
      {
        this.dropRef.instance.elements = matchingEls;

        // set on select callback
        this.dropRef.instance.onSelect = (selected) => 
        {
          this.select(matchingEls[selected].id);
        }
      }
    }
    else
    {
      if(this.dropRef)
      {
        this.dropRef.destroy();
      }
    }

    if(this.editable)
    {
      this.onChange(this.model);
    }
  }


  // Function to call when the rating changes.
  onChange = (text: string) => {};
  // Function to call when the input is touched (when a star is clicked).
  onTouched = () => {};


  writeValue(value: any): void
  {
    const index = this.elements.findIndex((el, id) => 
    {
      if(typeof value !== typeof this.getValueByID(id))
      {
        return value == this.getValueByID(id);
      }
      switch(typeof value)
      {
        case 'object':
          return partialDeepCompare(value,  this.getValueByID(id));
        case 'string':
        case 'number':
          return value === this.getValueByID(id)
        default:
          return false
      }
    });

    if (index !== -1)
    {
      this.selected = index;
    }
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }


  onFocus()
  {
    // document.execCommand('selectAll',false,null)
    var sel, range;
    if (window.getSelection && document.createRange) 
    {
      range = document.createRange();
      range.selectNodeContents(this._editableEl.nativeElement);
      sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    }
  }
}
