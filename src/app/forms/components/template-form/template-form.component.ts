/* eslint-disable @angular-eslint/component-selector */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@ngneat/reactive-forms';
import { Obj } from '@ngneat/reactive-forms/lib/types';
import { transformKeys, cloneObject, cloneObjectWithRef } from '@nororljos/nrs-util';
import { Subscription } from 'rxjs';
import { TMessage, FormTemplate, TElement } from '../../models/template';


@Component({
  selector: 'forms-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.scss']
})
export class TemplateFormComponent 
{

  @Output()
  form: EventEmitter<FormGroupTemplate> = new EventEmitter<FormGroupTemplate>();


  @Input()
  set message(msg: TMessage)
  {
    this._message = msg;
  }

  get message():TMessage
  {
    return this._message;
  }


  _message: TMessage;

  @Input()
  set template(template: FormTemplate | TElement[])
  {
    this._template = template;

    const fg = new FormGroupTemplate({});
    this.formGroup = fg;

    // fg.value$.subscribe(v => console.log(v));
  }

  get template(): FormTemplate | TElement[]
  {
    return this._template;
  }

  set formGroup(formGroup: FormGroupTemplate)
  {
    this._formGroup = formGroup;
    this.form.emit(this._formGroup);
  }

  get formGroup(): FormGroupTemplate
  {
    return this._formGroup;
  }

  _formGroup: FormGroupTemplate;
  _formExp: FormGroup;

  
  _template: FormTemplate | TElement[] = undefined;

  get asLayout(): FormTemplate
  {
    return Array.isArray(this._template) ? <FormTemplate>{children: this._template} : <FormTemplate>this._template;
  }
}





export class FormGroupTemplate<T extends Obj = any, E extends object = any> extends FormGroup 
{
  get templateValue()
  {
    return transformKeys(cloneObject(this.value), key => /[^#]+#[^#]+/.test(key), key => key.split('#')[0]);
  }


  override setValue(value, opt = undefined): Subscription
  {
    const ref = this.getRawValue();

    // // @ts-ignore
    return super.setValue(cloneObjectWithRef(value, ref, {ignoreArrays: true, keyTrans: key => key.split('#')[0]}), opt)
  }

}

