import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckGroupInputComponent } from './check-group-input.component';

describe('CheckGroupInputComponent', () => {
  let component: CheckGroupInputComponent;
  let fixture: ComponentFixture<CheckGroupInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckGroupInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckGroupInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
