/* eslint-disable */
import { Component, ContentChildren, ElementRef, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@ngneat/reactive-forms/lib/controlValueAccessor';
import { extractNumber } from '@nororljos/nrs-util';
import { Subscription } from 'rxjs';
import { TTranslatable } from '../../models/template';
import { TemplateService } from '../../services/template.service';

@Component({
  selector: 'forms-check-group-input',
  templateUrl: './check-group-input.component.html',
  styleUrls: ['./check-group-input.component.scss'], 
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR, 
      useExisting: CheckGroupInputComponent,
      multi: true,
    },
  ]
})
export class CheckGroupInputComponent implements OnInit, ControlValueAccessor  
{
  @ViewChild('checkGroup') checkGroupEl: ElementRef<HTMLDivElement>;

  @ViewChildren('checkCn') 
  set checkCns(ql: QueryList<ElementRef<HTMLDivElement>>)  
  {
    if(this.$qlChngSub)
    {
      this.$qlChngSub.unsubscribe();
    }

    this.adaptGrid(ql);

    this.$qlChngSub = ql.changes.subscribe(()=>
    {
      this.adaptGrid(ql)
    });  
  }

  @Input() label: string;

  @Input()
  set elements(els: {alias?: TTranslatable, name: string, value?: boolean}[] | string[] | {[key: string]: boolean})
  {
    this.checkGroup = {};
    this._elements = [];

    if(Array.isArray(els))
    {
      for(const el of els)
      {
        if(typeof el === 'string')
        {
          this.checkGroup[el] = false;
          this._elements.push({alias: el, name: el});
        }
        else
        {
          if(el.value)
          {
            this.checkGroup[el.name] = el.value;
          }
          else
          {
            this.checkGroup[el.name] = false;
          }
          this._elements.push(el)
        }
      }
    }
    this.onChange(this.checkGroup)
  }

  $qlChngSub: Subscription;

  allChecked: boolean = false;

  _elements: {alias?: TTranslatable, name: string}[] = [];

  checkGroup: {[key: string]: boolean} = {};

  disabled: boolean;

  constructor(private template: TemplateService) {}


  /**
   ** react to check changes
   * @param element 
   * @param value 
   */
  onCheckChange(element: {alias?: TTranslatable, name: string}, value: boolean)
  {
    this.checkGroup[element.name] = value;
    this.onChange(this.checkGroup);

    
    this.allChecked = this.isAll() ?? false;
  }


  /**
   ** return alias of an element
   * @param el 
   * @returns 
   */
  getAlias(el: {alias?: TTranslatable, name: string}): string
  {
    return el.alias ? this.template.getTranslation(el.alias) : el.name;
  }


  ngOnInit(): void {}


  checkAll(allChecked)
  {
    this.allChecked = allChecked;

    if(allChecked !== undefined)
    {
      for(const el of this._elements)
      {
        this.checkGroup[el.name] = allChecked;
      }
      this.onChange(this.checkGroup);
      this.onTouched();
    }
  }

  /**
   ** Return if all check control are set at true
   * @returns 
   */
  isAll(): boolean
  {
    let all;
    for(const el of this._elements)
    {
      if(all === undefined)
      {
        all = this.checkGroup[el.name];
      }
      if(all !== this.checkGroup[el.name])
      {
        return undefined;
      }
    }

    return all;
  }


  /**
   ** Adapt the grid in function of a querylis of HTML elements
   * @param ql 
   */
  adaptGrid(ql: QueryList<ElementRef<HTMLDivElement>>)
  {
    let max = 0;

    //- look for may elements width
    for(const el of ql.toArray())
    {
      const nb = extractNumber(window.getComputedStyle(el.nativeElement).width);
      
      if(nb > max)
      {
        max = nb;
      }
    }

    //- set column width automatic in funtion of widest check
    this.checkGroupEl.nativeElement.style.setProperty('grid-template-columns', `repeat(auto-fill, minmax(${max}px, 1fr))`);
  }


  /**
   ** return value of one check
   * @param name 
   * @returns 
   */
  valueOf(name: string)
  {
    return this.checkGroup[name];
  }
  
  // grid-template-columns: repeat(auto-fill, minmax(5em, 1fr));

  /**
   * ControlValueAccessor interface
   *  
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */ 

  // Function to call when text change 
  onChange = (check: {[key: string]: boolean}) => {};
  // Function to call when the input is touched
  onTouched = () => {};


  writeValue(data: {[key: string]: boolean}): void
  {
    for(const [key, val] of Object.entries(this.checkGroup))
    {
      if(key in data)
      {
        this.checkGroup[key] = data[key] ?? val;
      }
    }
    
    this.allChecked = this.isAll() ?? false;   
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }
}
