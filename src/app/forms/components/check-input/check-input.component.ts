/* eslint-disable */
import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'forms-check-input',
  templateUrl: './check-input.component.html',
  styleUrls: ['./check-input.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR, 
      useExisting: 
      forwardRef(() => CheckInputComponent),
      multi: true,
    },
  ],
})
export class CheckInputComponent implements OnInit, ControlValueAccessor 
{
  @Input()
  label: string = '';

  
  set checked(checked: boolean)
  {
    this._checked = checked || false;
  }

  disabled = false;

  constructor() { }
  

  _checked: boolean = false;
  

  ngOnInit(): void {}

  toggle()
  {
    this._checked = !this._checked;

    this.onChange(this._checked);
    this.onTouched();
  }


  /**
   * ControlValueAccessor interface
   *  
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */ 

  // Function to call when text change 
  onChange = (check: boolean) => {};
  // Function to call when the input is touched
  onTouched = () => {};


  writeValue(checked: boolean): void
  {
    if(typeof checked === 'string')
    {
      if(/(t|T)(r|R)(u|U)(e|E)/.test(checked))
      {
        this.checked = true;
      }
      else if(/(f|F)(a|A)(l|L)(s|S)(e|E)/.test(checked))
      {
        this.checked = false;
      }
      else
      {
        this.checked = checked === true;
      }

      this.onChange(this.checked);
    }
    else
    {
      this.checked = checked === true;
    }
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }
}
