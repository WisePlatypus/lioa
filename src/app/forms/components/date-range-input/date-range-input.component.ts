import { Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ResizeEvent } from '@nororljos/nrs-directives';
import flatpickr from "flatpickr";
import { Instance } from 'flatpickr/dist/types/instance';
import { BaseOptions } from 'flatpickr/dist/types/options';

@Component({
  selector: 'forms-date-range-input',
  templateUrl: './date-range-input.component.html',
  styleUrls: ['./date-range-input.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR, 
      useExisting: 
      // TextInputComponent, 
      forwardRef(() => DateRangeInputComponent),
      multi: true,
    },
  ],
})
export class DateRangeInputComponent implements OnInit, ControlValueAccessor
{
  settings: Partial<BaseOptions> = {
    mode: "range",
    // mode: 'single',
    inline: true,
    // enableTime: true,
    time_24hr: true,
  }

  disabled = false;

  dateRangesStr: string;

  @Input() autoSize: boolean = false;

  @Input() time: boolean = false;

  @Input() asTimeStamp: boolean = false;

  @ViewChild('range') 
  set range(range: ElementRef<HTMLDivElement>)
  {
    if(this.time)
    {
      this.settings.enableTime = true;
    }

    this._range = range;
    if(this._range?.nativeElement !== undefined)
    {
      this.flatpikrI =  flatpickr(this._range.nativeElement, this.settings);
      this.flatpikrI.config.onChange.push((sl ,ds, i)=>
      {
        if(sl.length>=2)
        {
          // this.dateRanges = sl;
          this.dateRangesStr = ds;
          this.dateRange.emit(this.asTimeStamp ? sl.map(s => s.getTime()) : sl);
          this.onChange(this.asTimeStamp ? sl.map(s => s.getTime()) : sl);
          this.onTouched()
        }
      });
    }
  }
  _range: ElementRef<HTMLDivElement>;

  @ViewChild('cc') 
  cc: ElementRef<HTMLDivElement>;


  @Output() dateRange: EventEmitter<Date[] | number[]> = new EventEmitter<Date[] | number[]>();

  constructor(private elRef: ElementRef<HTMLDivElement>) { }

  flatpikrI: Instance;

  

  ngOnInit(): void {}

  onResize(event: ResizeEvent)
  {
    if(this.autoSize === true)
    {
      if(this.time === true)
      {
        if( event.newWidth / event.newHeight < 0.9)
        {
          this.elRef.nativeElement.style.setProperty('font-size', `${event.newWidth/246.3}em`)
        }
        else
        {
          this.elRef.nativeElement.style.setProperty('font-size', `${event.newHeight/273.58}em`)
        }
      }
      else
      {
        if(event.newWidth / event.newHeight < 1.019)
        {
          this.elRef.nativeElement.style.setProperty('font-size', `${event.newWidth/246.3}em`)
        }
        else
        {
          // if(this.time)
          // {
            // this.elRef.nativeElement.style.setProperty('font-size', `${event.newHeight/273.58}em`)
          // }
          // else
          // {
            this.elRef.nativeElement.style.setProperty('font-size', `${event.newHeight/241.58}em`)
          // }
        }
      }
    }
  }


  

  /**
   * ControlValueAccessor interface
   *  
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */ 


  // Function to call when text change 
  onChange = (dates: Date[] | number[]) => {};
  // Function to call when the input is touched 
  onTouched = () => {};

  writeValue(dates: Date[] | number[] | undefined): void
  {
    if(this.flatpikrI)
    {
      if(dates !== undefined)
      {
        this.flatpikrI.setDate(typeof dates[0] === 'number' ? (<number[]>dates).map(d=>{return new Date(d)}) : dates);
      }
      else
      {
        this.flatpikrI.setDate(undefined);
        this.dateRangesStr = '';
      }
    }
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }

}
