/* eslint-disable */
import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'forms-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit 
{
  @Input() text: boolean = true;
  @Input() transparent: boolean = false;
  @Input() disabled: boolean = false;
  @Input() working: boolean = false;
  @Input() fs: number = 1;
  @Input() hard: boolean = false;
  @Input() hardAnimationTime: number = 1000;

  @Output() onClick: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

  down: boolean = false;
  hardDown: boolean = false;

  hardTimout;


  constructor(private elRef: ElementRef<HTMLElement>) {}

  ngOnInit(): void {
    this.elRef.nativeElement.style.setProperty('font-size', `${this.fs}em`);
    this.elRef.nativeElement.style.setProperty('--hard-animation-time', `${this.hardAnimationTime}ms`);
  }


  /**
   ** on mouse down event
   * register the mouse down and react acordingly
   * @param event 
   */
  onMouseDown(event)
  {
    // if button is in hard mode
    if(this.hard)
    {
      // set harddown as true
      this.hardDown = true;

      // set timout after hard animation time, register the timout ref
      this.hardTimout =  setTimeout(() => 
      {
        // register down for good and unregister timout
        this.down = true;
        this.hardTimout = undefined;
      }, this.hardAnimationTime);
    }
    else
    {
      // register down immediatly
      this.down = true;
    }
  }

  /**
   ** 
   * @param event 
   */
  onMouseUp(event)
  {
    if(this.down)
    {
      this.onClick.emit(event)
    }
  }

  @HostListener('document:mouseup', ['$event'])
  onDocumentMouseUp(event)
  {
    if(this.hardTimout)
    {
      clearTimeout(this.hardTimout);
    }
    this.hardDown = false;
    this.down = false;
  }

}
