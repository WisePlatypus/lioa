/* eslint-disable */
import { Component, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DropDownListComponent } from '../drop-down-list/drop-down-list.component';

@Component({
  selector: 'forms-editable-drop-down',
  templateUrl: './editable-drop-down.component.html',
  styleUrls: ['./editable-drop-down.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR, 
      useExisting: 
      // TextInputComponent, 
      forwardRef(() => EditableDropDownComponent),
      multi: true,
    },
  ],
})
export class EditableDropDownComponent implements OnInit, ControlValueAccessor 
{

  @ViewChild('dropDownList') dropDownList: DropDownListComponent = undefined;

  @Input() elements: string[] = ['Empty'];
  @Input() autoSize: boolean = false;


  set text(text: string)
  {
    if(text !== undefined && this._text !== text)
    {
      this._text = text;
      this.onChange(text);
      this.onTouched();
    }
  }

  get text(): string
  {
    return this._text;
  }

  _text: string = '';

  disabled: boolean;

  constructor() { }

  ngOnInit(): void {
  }

  dropList()
  {
    if(this.dropDownList)
    {
      this.dropDownList.toggleDrop();
    }
  }

  onDropChange(element: string)
  {
    this.text = element;
  }


  // Function to call when text change 
  onChange = (text: string) => {};
  // Function to call when the input is touched 
  onTouched = () => {};



  get value(): string
  {
    return this.text;
  }

  writeValue(text: string): void
  {
    this.text = text;
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }

}
