import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditableDropDownComponent } from './editable-drop-down.component';

describe('EditableDropDownComponent', () => {
  let component: EditableDropDownComponent;
  let fixture: ComponentFixture<EditableDropDownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditableDropDownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableDropDownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
