/* eslint-disable */
import { Component, ElementRef, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { locale, MbscLocale, momentTimezone } from '@mobiscroll/angular';
// import { locale, MbscLocale } from '@mobiscroll/angular';
import { DatepickerBase, MDate } from '@mobiscroll/angular/dist/js/core/components/datepicker/datepicker';
import { TranslateService } from '@ngx-translate/core';
import { ResizeEvent } from '@nororljos/nrs-directives';
import { Subscription } from 'rxjs';
import { MobiDatePickerChangeEvent } from '../mobi-date-range-input/mobi-date-range-input.component';
// import moment from 'moment-timezone';
import * as moment from 'moment-timezone';
import { TimezoneService } from '@nororljos/nrs-angular-util';


momentTimezone.moment = moment;


@Component({
  selector: 'forms-mobi-date-input',
  templateUrl: './mobi-date-input.component.html',
  styleUrls: ['./mobi-date-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MobiDateInputComponent),
      multi: true,
    },
  ],
})
export class MobiDateInputComponent implements OnDestroy, ControlValueAccessor
{
  @Input() milisec: number = 0;

  @Input() sec: number = 0;

  @Input() asTimeStamp: boolean = false;

  @Input()
  set time(time: boolean)
  {
    const index = this.controls.findIndex(v => {return v === 'time'});

    if(time)
    {
      if(index === -1)
      {
        this.controls.push('time')
      }
    }
    else
    {
      if(index !== -1)
      {
        this.controls.splice(index, 1)
      }
    }
  }

  @Output() dateRange: EventEmitter<Date | number> = new EventEmitter<Date | number>();

  @Input() autoSize: boolean = false;

  @ViewChild('cc')
  cc: ElementRef<HTMLDivElement>;

  public momentPlugin = momentTimezone;


  displayTimezone: string = 'utc';


  disabled: boolean = false;


  instance: DatepickerBase;

  date: Date;

  controls: string[] = ['calendar'];

  locales: {[key: string]: MbscLocale} = locale;

  locale: MbscLocale;

  $language: Subscription;

  dateFormat: string;
  timeFormat: string;

  constructor(
    private translate: TranslateService,
    private timezoneService: TimezoneService)
  {
    this.updateLanguage(translate.currentLang);

    this.$language = this.translate.onLangChange.subscribe(lang =>
    {
      this.updateLanguage(lang.lang);
    });

    this.timezoneService.timeZoneChangedEvent.subscribe(tz =>
    {
       this.displayTimezone = tz;
    });

    this.dateFormat = this.timezoneService.dateFormat;
    this.timeFormat = this.timezoneService.hourMinutesTimeFormat;
    //this.timeWheels = this.timezoneService.selectedDateFormat === 'iso' || !moment.localeData().isPM ? "HH:mm" : "hh:mm";
  }

  updateLanguage(lang: string)
  {
    switch(lang)
    {
      case 'en':
        this.locale = this.locales['en-GB'];
        break;
      case 'fr':
        this.locale = this.locales[lang];
        this.locale.rangeStartLabel = 'Début';
        break;
      default:
        this.locale = this.locales[lang];
        break;
    }
  }


  ngOnDestroy(): void
  {
    this.$language.unsubscribe();
  }


  //! MDate === string in our case, replacing mdate to string will result in compilation error for some reason
  onMobiChange(data: MobiDatePickerChangeEvent<MDate>)
  {
    this.date = data.value == undefined ? undefined : new Date(data.value);

    this.date.setSeconds(this.sec, this.milisec);

    this.onChange(this.asTimeStamp ? (this.date !== null ? this.date.getTime() : undefined) : this.date);

    this.dateRange.emit(this.date);
  }


  onMobiInit(event: {inst: DatepickerBase, type: string})
  {
    this.instance = event.inst;
    const tz = localStorage.getItem('tz');
    this.displayTimezone  = tz ? tz : 'utc';

    if(this.date !== undefined)
    {
      this.instance.setVal(this.date === undefined ? undefined : moment(this.date));
    }
  }

  onResize(event: ResizeEvent)
  {
    if(this.autoSize)
    {
      if(event.newWidth / event.newHeight < (480 / 382))
      {
        this.cc.nativeElement.style.setProperty('transform', `scale(${event.newWidth/480})`);
      }
      else
      {
        this.cc.nativeElement.style.setProperty('transform', `scale(${event.newHeight/382})`);
      }
    }
  }

  /**
   * ControlValueAccessor interface
   *
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */


  // Function to call when text change
  onChange = (dates: Date | number) => {};
  // Function to call when the input is touched
  onTouched = () => {};

  writeValue(date: Date | number | undefined): void
  {
    const asDate: Function = (date: Date | number | string) =>
    {
      switch(typeof date)
      {
        case 'object':
          return date instanceof Date ? date : undefined;
        case 'number':
        case 'string':
          return new Date(date);
      }
    }
    if(date !== undefined)
    {
      this.date = asDate(date);

      if(this.instance)
      {
        this.instance.setVal(this.date === undefined ? undefined : moment(this.date));
      }
    }
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }

}
