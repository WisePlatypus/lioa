import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobiDateInputComponent } from './mobi-date-input.component';

describe('MobiDateInputComponent', () => {
  let component: MobiDateInputComponent;
  let fixture: ComponentFixture<MobiDateInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobiDateInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobiDateInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
