/* eslint-disable */
import { AfterViewInit, Component, ElementRef, forwardRef, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { getCarretPosition, setCarretPosition } from '@nororljos/nrs-util';

@Component({
  selector: 'forms-text-area',
  templateUrl: './text-area.component.html',
  styleUrls: ['./text-area.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR, 
      useExisting: 
      forwardRef(() => TextAreaComponent),
      multi: true,
    },
  ]
})

export class TextAreaComponent implements AfterViewInit, ControlValueAccessor
{

  // element Ref of our input and mesure hidden span for autosize
  @ViewChild('input') inputEl: ElementRef<HTMLInputElement>;

  // text property
  _text: string = '';

  // diabled boolean
  disabled: boolean = false;

  pressedEnter: boolean = false;

  /**
   * text setter
   */
  set text(text: string)
  {
    // if text has changed and is not undefined
    if(text !== undefined && this._text !== text)
    {
      let carret;
      
      if(this.inputEl?.nativeElement !== undefined)
      {
        carret = getCarretPosition(this.inputEl.nativeElement);

        if(Array.isArray(carret))
        {
          carret = carret[0];
        }
      }

      this._text = text;

      requestAnimationFrame(()=>
      {
        if(carret && !this.disabled && document.activeElement === this.inputEl.nativeElement)
        {
          setCarretPosition(this.inputEl.nativeElement, carret + (this.pressedEnter ? 1 : 0));
        }

        this.pressedEnter = false;
      });

      this.onChange(text);
    }
  }

  /**
   * text getter
   */

  get text(): string
  {
    return this._text;
  }

  constructor() {}

  /**
   * after init call autosize if needed
   */

  ngAfterViewInit(): void 
  { }


  /**
   * value getter
   */
  get value(): string
  {
    return this.text;
  }

  onBlur()
  {
    this.onTouched();
  }

  onInput(event: InputEvent)
  {
    const carret = getCarretPosition(this.inputEl.nativeElement);

    // @ts-ignore
    this.model = event.target.textContent;
     
    //- apply fefore next frame
    requestAnimationFrame(()=>
    {
      try
      {
        
        if(carret && !this.disabled && document.activeElement === this.inputEl.nativeElement)
        {
          setCarretPosition(this.inputEl.nativeElement, carret);
        }
      }
      catch{}
    })
  }

  onKeypress(event: KeyboardEvent)
  {
    if (event.key === 'Enter')
    {
      this.pressedEnter = true;
    }
  }


  /**
   * ControlValueAccessor interface
   *  
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */ 

  // Function to call when text change 
  onChange = (text: string) => {};
  // Function to call when the input is touched 
  onTouched = () => {};

  writeValue(text: string): void
  {
    this.text = text;
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }
}
