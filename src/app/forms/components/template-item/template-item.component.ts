/* eslint-disable */
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@ngneat/reactive-forms';
import { getDefaultTypeValueOf, Syncronizer } from '@nororljos/nrs-util';
import { Subscription } from 'rxjs';
import { TMessage, INPUTS_LABELS, TItem, TInterface, TInput, TInputText, TInputDropDown, TInputSuggestDropDown, TInputIp, TInputCheck, TInputNumber, TInputDate, TInputDateRange, TInputCheckGroup, TInputSelectionList, TListDisplay, TDisplay } from '../../models/template';
import { TemplateService } from '../../services/template.service';
import { TranslateService } from '@ngx-translate/core';



@Component({
  selector: 'forms-template-item',
  templateUrl: './template-item.component.html',
  styleUrls: ['./template-item.component.scss']
})
export class TemplateItemComponent implements OnInit 
{
  @Input()
  set message(msg: TMessage)
  {
    if(msg !== undefined)
    {
      switch(msg.type)
      {
        case 'reset value':
          this.backup = this.formControl.value;

          if(this.templateItem.value)
          {
            this.formControl.setValue(this.templateItem.value);
          }
          break;
        case 'restore value':
          if(this.backup !== undefined)
          {
            this.formControl.setValue(this.backup);
          }
          break;
        case 'enable':
          if(!msg.value)
          {
            this.backup = this.formControl.value;
            this.formControl.disable();

            if(this.templateItem.value !== undefined)
            {
              this.formControl.setValue(this.templateItem.value);
            }
            else
            {
              this.formControl.setValue(getDefaultTypeValueOf(this.backup));
            }
          }
          else
          {
            this.formControl.enable();

            if(this.backup === undefined || this.backup === '')
            {
              this.formControl.setValue(this.templateItem.value);
            }
            else
            {
              this.formControl.setValue(this.backup);
            }
          }
          break; 
      }
    }
  }

  backup: any;

  showHelp: boolean = false;
  timerHelp: number = 0;
  timerHelpTimeout: number;

  inputSyncronizer: Syncronizer = new Syncronizer(2);

  inputHasLabel = INPUTS_LABELS;

  $enabled: Subscription;
  $value: Subscription;

  @Input()
  set templateItem(ti: TItem)
  {
    if(ti !== undefined)
    {
      this._templateItem = ti;
      this.inputSyncronizer.sync('tg');
    }
  }

  get templateItem(): TItem
  {
    return this._templateItem;
  }
  
  private _templateItem: TItem = undefined;


  @Input()
  set parentFormGroup(formGroup: FormGroup)
  {
    if(formGroup !== undefined)
    {
      this._parentFormGroup = formGroup;
      this.inputSyncronizer.sync('pfg');
    }
  }

  get parentFormGroup(): FormGroup
  {
    return this._parentFormGroup;
  }

  private _parentFormGroup: FormGroup = undefined;

  private _formControl: FormControl = undefined;

  get formControl(): FormControl
  {
    return this._formControl;
  }

  constructor(private fb: FormBuilder, private template: TemplateService, private translate: TranslateService) 
  {
    this.inputSyncronizer.onSyncEvent.subscribe(()=>
    {
      this._formControl = this.fb.control(this.templateItem.value);
      
      if(this.$value)
      {
        this.$value.unsubscribe();
      }
      this.$value = this._formControl.value$.subscribe(v=>
      {
        if(this._formControl.disabled && !((this.templateItem.value  && this.formControl.value === this.templateItem.value) || (!this.templateItem.value && this.formControl.value === undefined)))
        {
          this.backup = v;
          if(this.templateItem.value && this.templateItem.overwriteOnCondition === undefined || this.templateItem.overwriteOnCondition === true)
          {
            this.formControl.setValue(this.templateItem.value);
          }
        }
      })

      if(this.$enabled)
      {
        this.$enabled.unsubscribe();
      }
      this.$enabled = this._formControl.enabled$.subscribe(e =>
      {
        if(!e)
        {
          this.backup = this.formControl.value;
          if(this.templateItem.value)
          {
            this.formControl.setValue(this.templateItem.value);
          }
          else
          {
            this.formControl.setValue(undefined);
          }
        }
        else
        {
          this.formControl.enable();
          
          if(this.backup !== undefined)
          // && ((this.templateItem.value  && this.formControl.value === this.templateItem.value) || (!this.templateItem.value && this.formControl.value === undefined))
          {
            this.formControl.setValue(this.backup);
          }
        }
      });


      this.formControl.setValidators(this.template.getValidator(this.templateItem));
      setTimeout(() => {
        this._parentFormGroup.addControl(this.templateItem.name, this._formControl);
      }, 0);
      this.inputSyncronizer.reset();
    });
  }

  ngOnInit(): void {}


  helpMouseEnter()
  {
    this.timerHelpTimeout = this.timerHelp;

    setTimeout(() => {
      if(this.timerHelp === this.timerHelpTimeout)
      {
        this.showHelp = true;
      }
    }, 400);
  }

  helpMouseLeave()
  {
    this.timerHelp++;
    this.showHelp = false;
  }


  get templateInterface(): TInterface
  {
    return <TInput>this.templateItem.interface;
  }

  get templateInput(): TInput
  {
    return <TInput>this.templateItem.interface;
  }

  get templateDisplay(): TDisplay
  {
    return <TDisplay>this.templateItem.interface;
  }

  get inputLabel(): string
  {
    return `${this.template.getLabel(this.templateItem)}${this.formControl.enabled && this.templateInput.required && this.inDisplay === false ? '*' : ' '}`;
  }

  get intefaceHelp(): string
  {
    const t = this.template.getTranslation(this.templateInterface.help);
    return t !== 'invalid' ? t : '';
  }

  get validatorHelpText()
  {
    if(this.templateInterface) 
    {      
      let textHelp = '';
      let isfirst = true;
      if('value' in this.templateItem && this.templateItem['value'] !== '')
      {
        isfirst = false;
        textHelp += `${this.translate.instant('default')}: ${this.templateItem['value']}`;
      }
      if((<TInput>this.templateInterface)?.validator)
      {
        const validator = (<TInput>this.templateInterface)?.validator;
      
        if(typeof validator === 'object')
        {
          for(const [key, val] of Object.entries(validator))
          {
            if(!isfirst) 
            {
              isfirst = false;
              textHelp += ',';
            }
            textHelp += ` ${this.translate.instant(key)}: ${typeof val === 'string' || typeof val === 'boolean' 
            ? this.translate.instant(val.toString()) : val}`;
          }
        } 
      }     
      if('min' in this.templateInterface)
      {
        if(!isfirst) 
        {
          isfirst = false;
          textHelp += ',';
        }
        textHelp += ` ${this.translate.instant('min')}: ${this.templateInterface['min']}`;
      }
      if('max' in this.templateInterface)
      {
        if(!isfirst) 
        {
          isfirst = false;
          textHelp += ',';
        }
        textHelp += ` ${this.translate.instant('max')}: ${this.templateInterface['max']}`;
      }
      if('suffix' in this.templateInterface)
      {
        if(!isfirst) 
        {
          isfirst = false;
          textHelp += ',';
        }
        textHelp += ` ${this.translate.instant('in')} ${this.translate.instant((this.templateInterface['suffix'] as string).trim())}`;
      }
      return textHelp !== '' ? `${this.templateInterface?.help !== undefined ? '\n' : ''} ${textHelp}` : '';
    }
    
    return '';
  }
  
  hasValidator(validator)
  {
    if(validator === undefined)
    {
      return false
    }
    return typeof validator === 'object'
  }

  get asInputText(): TInputText
  {
    return <TInputText>this.templateInput
  }

  get asInputDropDown(): TInputDropDown
  {
    return <TInputDropDown>this.templateInput
  }

  get asInputSuggestDropDown(): TInputSuggestDropDown
  {
    return <TInputSuggestDropDown>this.templateInput
  }

  get asInputIp(): TInputIp
  {
    return <TInputIp>this.templateInput
  }

  get asInputCheck(): TInputCheck
  {
    return <TInputCheck>this.templateInput
  }
  
  get asInputNumber(): TInputNumber
  {
    return <TInputNumber>this.templateInput
  }

  get asInputDate(): TInputDate
  {
    return <TInputDate>this.templateInput
  }
  
  get asInputDateRange(): TInputDateRange
  {
    return <TInputDateRange>this.templateInput
  }

  get asInputCheckGroup(): TInputCheckGroup
  {
    return <TInputCheckGroup>this.templateInput
  }

  get asSelectionList(): TInputSelectionList
  {
    return <TInputSelectionList>this.templateInput
  }

  get asListDisplay(): TListDisplay
  {
    return <TListDisplay>this.templateDisplay
  }

  get invalidMsg(): string
  {
    return this.template.getTranslation(this.templateInput?.unvalidMsg);
  }


  get inDisplay(): boolean
  {
    return this.templateInput?.display !== undefined ? this.templateInput.display : false; 
  }

  get doubleWidth(): boolean
  {
    return this.templateItem.interface ? this.templateItem.interface.width === 'double' : false;
  }

}