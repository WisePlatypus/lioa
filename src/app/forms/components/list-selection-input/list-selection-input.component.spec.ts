import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSelectionInputComponent } from './list-selection-input.component';

describe('ListSelectionInputComponent', () => {
  let component: ListSelectionInputComponent;
  let fixture: ComponentFixture<ListSelectionInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSelectionInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSelectionInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
