/* eslint-disable */
import { Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ResizeEvent } from '@nororljos/nrs-directives';
import { EventState, getValueWithPath, deepCompare, deepElementCompare } from '@nororljos/nrs-util';
import { Label, TemplateListElement, TTranslatable } from '../../models/template';
import { TemplateService } from '../../services/template.service';


@Component({
  selector: 'forms-list-selection-input',
  templateUrl: './list-selection-input.component.html',
  styleUrls: ['./list-selection-input.component.scss'],providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting:
      forwardRef(() => ListSelectionInputComponent),
      multi: true,
    },
  ]
})
export class ListSelectionInputComponent implements OnInit
{
  @Output() change: EventEmitter<any[]> = new EventEmitter<any[]>();

  @Output() remove: EventEmitter<any[]> = new EventEmitter<any[]>();

  @Output() add: EventEmitter<any[]> = new EventEmitter<any[]>();

  @Input()
  set elementsLabel(label: TTranslatable)
  {
    this._elementsLabel = label;
  }

  get elementsLabel(): TTranslatable
  {
    return this.template.getTranslation(this._elementsLabel)
  }

  _elementsLabel: TTranslatable = 'From';

  /**
   ** elements setter
   */
  @Input()
  set elements(elements: TemplateListElement[])
  {
    this._elements = elements;

    this._selection = [];
  }

  /**
   ** elements getter
   */
  get elements(): TemplateListElement[]
  {
    return this._elements;
  }

  //- elements
  // these are unselected base elements with optionnal alias and value
  _elements: TemplateListElement[] = [];


  @Input()
  set selectionLabel(label: TTranslatable)
  {
    this._selectionLabel = label;
  }


  get selectionLabel(): TTranslatable
  {
    return this.template.getTranslation(this._selectionLabel)
  }

  _selectionLabel: TTranslatable = 'Selected';

  /**
   ** selected setter
   */
  @Input()
  set selection(selected: TemplateListElement[])
  {
    this._selection = selected;
  }

  /**
   ** selected getter
   */
  get selection(): TemplateListElement[]
  {
    return this._selection;
  }


  @Input()
  set fieldLabels(labels: Label[])
  {
    if(Array.isArray(labels))
    {
      this._fieldLabels = labels;
    }
  }

  get fieldLabels(): Label[]
  {
    return this._fieldLabels;
  }


  _fieldLabels: Label[] = [];




  @Input() namePath: string;


  //- selection
  // these are selected elements with opètionnal alias and value
  _selection: TemplateListElement[] = [];


  // column selected id
  selectionSelectedId: number[] = [];

  selectionSelectedMap: {[key: number]: boolean} = {};

  // column selected id
  elementsSelectedId: number[] = [];

  elementsSelectedMap: {[key: number]: boolean} = {};

  disabled: boolean = false;

  constructor(private template: TemplateService, private elRef: ElementRef<HTMLElement>) { }

  ngOnInit(): void {}

  /**
   ** return alias of a TemplateListElement
   * @param element
   * @returns
   */
  getAlias(element: TemplateListElement): string
  {
    return element !== undefined ? typeof element !== 'object' ? element : (element.alias !== undefined ? this.template.getTranslation(element.alias) : element.value) : '';
  }

  /**
   ** return value of a TemplateListElement
   * @param element
   * @returns
   */
  getValue(element: TemplateListElement)
  {
    return typeof element === 'object' && 'value' in element ? element.value : element;
  }

  /**
   ** return element alias given an ID
   * @param id
   * @returns
   */
  getElementAlias(id: number)
  {
    return this.getAlias(this.elements[id]);
  }

  /**
   ** return element value given an ID
   * @param id
   * @returns
   */
  getElementValue(id: number)
  {
    return this.getValue(this.elements[id]);
  }

  /**
   ** return selected alias given an ID
   * @param id
   * @returns
   */
  getSelectedAlias(id: number)
  {
    return this.getAlias(this.selection[id]);
  }

  /**
   ** return selected valuer given an ID
   * @param id
   * @returns
   */
  getSelectedValue(id: number)
  {
    return this.getValue(this.selection[id]);
  }

  /**
   ** select a row
   * @param rowID
   */
  onSelect(rowID, selectedId: number[], selectedMap: {[key: number]: boolean}): void
  {
    if(EventState.keyPressed['control'])
    {
      // if selection is already selected
      if(selectedMap[rowID])
      {
        delete(selectedMap[rowID]);
        if(typeof selectedId === 'number')
        {
          selectedId = [];
        }
        else
        {
          selectedId.splice(selectedId.findIndex(v=>v===rowID), 1)
        }
      }
      else
      {
        this.addToSelection([rowID], selectedId, selectedMap);
      }
    }
    else if(EventState.keyPressed['shift'] )
    {
      let range = [];

      // range start at last selection
      let start = (typeof selectedId === 'number' ? selectedId : selectedId[(<number[]>selectedId).length - 1]);
      let end = rowID;

      // permutation
      if(start > end)
      {
        const perm = start;
        start = end;
        end = perm;
      }


      for(let i = start; i <= end; i++)
      {
        if(!selectedMap[i])
        {
          range.push(i);
        }
      }

      this.addToSelection(range, selectedId, selectedMap);
    }
    else
    {
      while(selectedId.length !== 0)
      {
        selectedId.pop();
      }

      selectedId.push(rowID)

      this.resetSelectedMap(selectedMap);
      selectedMap[rowID] = true;
    }
  }



  private resetSelectedMap(selectedMap: {[key: number]: boolean})
  {
    if(selectedMap)
    {
      for (const key of Object.keys(selectedMap))
      {
        delete(selectedMap[key]);
      }
    }
  }

  addToSelection(nSelection: number[], selectedId: number[], selectedMap: {[key: number]: boolean})
  {
    for(const selected of nSelection)
    {
      selectedId.push(selected);
      selectedMap[selected] = true;
    }
  }


  resetSelection(selectedId: number[], selectedMap: {[key: number]: boolean})
  {
    while(selectedId.length !== 0)
    {
      selectedId.pop();
    }
    this.resetSelectedMap(selectedMap);
  }


  onListClick(i: number, selectedId: number[], selectedMap: {[key: number]: boolean}, opselectedId: number[], opselectedMap: {[key: number]: boolean})
  {
    this.resetSelection(opselectedId, opselectedMap);
    this.onSelect(i, selectedId, selectedMap);
  }

  onElementToSelection()
  {
    this.elementsSelectedId.sort((a, b) => a - b);
    const adds = [];

    while(this.elementsSelectedId.length !== 0)
    {
      const id = this.elementsSelectedId.pop();
      const add = this.elements.splice(id, 1)[0];
      this.selection.push(add);
      adds.push(add);
      delete(this.elementsSelectedMap[id])
    }

    const selectionVal = this.selectionValues;

    this.onChange(selectionVal);
    this.change.emit(selectionVal);
    this.add.emit(adds.map(sel => this.value(sel)));
  }

  onSelectionToElement()
  {
    this.selectionSelectedId.sort((a, b) => a - b);
    const removes = [];

    while(this.selectionSelectedId.length !== 0)
    {
      const id = this.selectionSelectedId.pop();
      const remove = this.selection.splice(id, 1)[0];
      this.elements.push(remove);
      removes.push(remove);
      delete(this.selectionSelectedMap[id])
    }

    const selectionVal = this.selectionValues;

    this.onChange(selectionVal);
    this.change.emit(selectionVal);
    this.remove.emit(removes.map(sel => this.value(sel)));
  }


  get selectionValues()
  {
    return this.selection.map(sel => this.value(sel));
  }


  value(element)
  {
    return typeof element === 'object' ? ('value' in element ? element.value : element) : element;
  }

  getLabelValue(element: any, label: Label)
  {
    return getValueWithPath(this.getValue(element), label.path.split('.'));
  }

  onResize(resize: ResizeEvent)
  {
    this.elRef.nativeElement.style.setProperty('--table-body-height', `calc(${resize.newHeight}px - ${this.fieldLabels.length === 0 ? 2.467 : 2.467 + 2.5 }em)`)
  }

  getNameValue(element)
  {
    return this.namePath ? getValueWithPath(this.getValue(element), this.namePath.split('.')) : typeof element === 'object' ? ('name' in element ? element.name : this.value(element)) : element;
  }


  /**
   * ControlValueAccessor interface
   *
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */

  // Function to call when text change
  onChange = (selection: TemplateListElement[]) => {};
  // Function to call when the input is touched
  onTouched = () => {};

  writeValue(selection: any[]): void
  {
    if(Array.isArray(selection))
    {
      // all template list elements
      const ref: TemplateListElement[] = [...this._elements, ...this._selection];

      // the new selection
      const newSelection: TemplateListElement[] = [];

      // for the given selection
      for(const element of selection)
      {
        // find an ID where current selection item match ref item
        const id = ref.findIndex((v) => deepCompare(this.getNameValue(v), this.getNameValue(element)));

        let el = element;

        if(id !== -1)
        {
          el = ref.splice(id, 1)[0];
        }
        newSelection.push(el);
      }

      // elements are the remaining items
      this._elements = ref;



      if(!deepElementCompare(this._selection, newSelection))
      {
        this._selection = newSelection;

        setTimeout(() =>
        {
          this.onChange(this.selectionValues);
        }, 0);
      }

    }
  }


  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }

}





// export interface Label
// {
//   alias: TTranslatable;
//   path: string;
// }


// --table-body-height
