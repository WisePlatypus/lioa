import { Component, Input } from '@angular/core';
import { TTranslatable } from '../../models/template';
import { TemplateService } from '../../services/template.service';

@Component({
  selector: 'forms-text-display',
  templateUrl: './text-display.component.html',
  styleUrls: ['./text-display.component.scss'],
})
export class TextDisplayComponent {
  constructor(private template: TemplateService) {
  }

  @Input() text: TTranslatable;

  textV(): string
  {
    return this.template.getTranslation(this.text);
  }
}
