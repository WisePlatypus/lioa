/* eslint-disable */
import { ChangeDetectorRef, Component, ComponentRef, ElementRef, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@ngneat/reactive-forms';
import { TranslateService } from '@ngx-translate/core';
import { ModalService } from '@nororljos/nrs-modal';
import { Syncronizer, isTypeArray, hashObjectValue, undefinedIsString, cloneObject, getValueWithPath } from '@nororljos/nrs-util';
import { TMessage, TItem, TArray, TLayout, TGroup } from '../../models/template';
import { TemplateService } from '../../services/template.service';
import { SubFormDuplicateValidator, testObjectKeyDuplicate } from '../../validators/sub-form-duplicate.directive';
import { FormGroupTemplate } from '../template-form/template-form.component';
import { ToolTipComponent } from '../tool-tip/tool-tip.component';


@Component({
  selector: 'forms-template-array',
  templateUrl: './template-array.component.html',
  styleUrls: ['./template-array.component.scss']
})
export class TemplateArrayComponent 
{
  readonly title = [1.3, 1, 0.9, 0.8, 0.7];


  inputSyncronizer: Syncronizer = new Syncronizer(3);

  controlAdd = true;
  controlUpdate = true;
  controlDelete = true;

  addValidity = true;
  updateValidity = true;

  addMaxValidity = true;

  selectedHash: string = undefined;

  
  duplicationAddReasons: string[][] = [];
  duplicationUpdateReasons: string[][] = [];


  @Input()
  set message(msg: TMessage)
  {
    if(msg !== undefined)
    { 
      switch(msg.type)
      {
        case 'reset value':
          this.backup = this._formControl.value;
          if(this.templateArray.array)
          {
            this.formControl.setValue(this.templateArray.array);
          }
          else
          {
            this.formControl.setValue([]);
          }
          break;
        case 'restore value':
          if(this.backup !== undefined)
          {
            this.formControl.setValue(this.backup);
          }
          break;
        case 'enable':
          if(!msg.value)
          {
            this.formControl.disable();
            this.backup = this._formControl.value;
            if(this.templateArray.array)
            {
              this.formControl.setValue(this.templateArray.array);
            }
            else
            {
              this.formControl.setValue([]);
            }
          }
          else
          {
            this.formControl.enable();
            
            if(this.backup !== undefined)
            {
              this.formControl.setValue(this.backup);
            }
          }
          break;
      }
    }
  }

  backup: any;

  flatItems: {item: TItem, path: string[]}[] = [];
  
  @Input() 
  set templateArray(ta: TArray)
  {
    if(ta !== undefined)
    {
      this._templateArray = ta;

      const title = this.title[ta?.title];

      if(title !== undefined)
      {
        this.elRef.nativeElement.style.setProperty('--label-fz', `${title}em`)
      }
      else
      {
        this.elRef.nativeElement.style.setProperty('--label-fz', `${1}em`)
      }

      if(ta?.collapsed !== undefined && typeof ta.collapsed === 'boolean')
      {
        this.collapse = ta.collapsed;
      }

      if(ta?.controls !== undefined && Array.isArray(ta.controls))
      {
        this.controlAdd = false;
        this.controlUpdate = false;
        this.controlDelete = false;

        for(const control of ta.controls)
        {
          switch(control)
          {
            case 'add':
              this.controlAdd = true;
              break;
            case 'delete': 
              this.controlDelete = true;
              break;
            case 'update':
              this.controlUpdate = true;
              break;
          }
        }
      }
      this.inputSyncronizer.sync('ta');
    }
  }

  get templateArray(): TArray
  {
    return this._templateArray;
  }
  
  _templateArray: TArray;


  @Input()
  set parentFormGroup(formGroup: FormGroup)
  {
    if(formGroup !== undefined)
    {
      this._parentFormGroup = formGroup;
      this.inputSyncronizer.sync('pfg');
    }
  }

  get parentFormGroup(): FormGroup
  {
    return this._parentFormGroup;
  }


  get formGroup(): FormControl
  {
    return this._formControl;
  }

  _parentFormGroup: FormGroup = undefined;

  _formControl: FormControl = undefined;

  _subForm: FormGroupTemplate;

  _sync = false;

  selected = undefined;

  collapse = false;

  subFormDuplicates: SubFormDuplicateValidator[] = undefined;


  controlledChange = false;

  defaultTopStackInstanceId: number;


  toolTipRef: ComponentRef<ToolTipComponent>;

  constructor(
    private fb: FormBuilder, 
    private template: TemplateService, 
    private elRef: ElementRef<HTMLElement>, 
    private changeDetectorRef: ChangeDetectorRef, 
    private modal: ModalService,
    private translate: TranslateService) 
  {

    this.defaultTopStackInstanceId = this.modal.topOfStackInstance ? this.modal.topOfStackInstance.modalId : 0;

    // when eveything is synced
    this.inputSyncronizer.onSyncEvent.subscribe(()=>
    {
      // add array validatr
      this._formControl = this.fb.control([], this.template.getValidator(this._templateArray));

      // if there is an unique item combination
      if(this._templateArray.uniqueItemComb && this._templateArray.uniqueItemComb.length > 0)
      {
        // set it up
        if(Array.isArray(this._templateArray.uniqueItemComb[0]))
        {
          this.subFormDuplicates = [];

          for(const unique of this._templateArray.uniqueItemComb)
          {
            const sfd: SubFormDuplicateValidator = {keys: [], hashes: {}}

            for(const item of unique)
            {
              const itemData = this.template.findNammable(<TLayout>{type: 'layout', children: this._templateArray.subForm}, item);
  
              sfd.keys.push({path: itemData.path, item: itemData.namable});
            }

            this.subFormDuplicates.push(sfd);
          }
        }
        else if (isTypeArray<string>(this._templateArray.uniqueItemComb, 'string'))
        {
          this.subFormDuplicates = [{keys: [], hashes: {}}];
  
          for(const item of this._templateArray.uniqueItemComb)
          {
            const itemData = this.template.findNammable(<TLayout>{type: 'layout', children: this._templateArray.subForm}, item);
  
            this.subFormDuplicates[0].keys.push({path: itemData.path, item: itemData.namable});
          }
        }

      }


      if(this._templateArray.listColumn !== undefined && this._templateArray.listColumn.length >= 1)
      {
        // use childrens as group since find nammabe find with direct encapsulable path (arrays are special case)
        const asGroup = <TGroup>{type: 'group', children: this._templateArray.subForm};
        
        for(const name of this._templateArray.listColumn)
        {
          // find item
          const item = this.template.findNammable(asGroup, name);
          
          if(item !== undefined && item.namable.type === 'item')
          {
            item.path.shift();
            this.flatItems.push({item: <TItem>item.namable, path: item.path.map(path => path.split('#')[0])});     
          }
        }
      }
      else
      {
        this.flatItems = this.template.flatItems(<TLayout>{type: 'layout', children: this.templateArray.subForm}).map(item => {return {item: item, path: [item.name.split('#')[0]]}});
      }
      
      setTimeout(() => 
      {
        if(this._templateArray?.array !== undefined && Array.isArray(this._templateArray.array))
        {
          for(const el of this._templateArray.array)
          {
            this.addElement(el);
          }
        }

        this._sync = true;
        this._parentFormGroup.addControl(this.templateArray.name, this._formControl);

        this._formControl.valueChanges.subscribe(array => 
        {
          if(Array.isArray(array))
          {
            for(const subFormDuplicate of this.subFormDuplicates)
            {
              if(this.controlledChange === false && this.subFormDuplicates)
              {
                subFormDuplicate.hashes = {};
    
                for(const el of array)
                {
                  
                //{"alias":"Realtek PCIe GBE Family Controller [192.168.100.54]","value":{"ip":["192.168.100.54"],"name":"Realtek PCIe GBE Family Controller"}}
                  subFormDuplicate.hashes[hashObjectValue(el, subFormDuplicate.keys.map(sfd => sfd.path))] = true;
                }      
              }
    
              this.controlledChange = false;
            }
          }
        })
      }, 0);

      this.inputSyncronizer.reset();
    });
  }


  /**
   * get item label
   * @param item 
   * @returns 
   */
  getItemLabel(item: {item: TItem, path: string[]}): string
  {
    return this.template.getLabel(item.item);
  }

  /**
   ** properties form group getter
   */
  get formControl(): FormControl
  {
    return this._formControl
  }

  /**
   ** Add a property or an empty one if none is given
   * @param property 
   */
  addElement(property = undefined)
  {
    this.controlledChange = true;
    // Clone property to avoid change by reference
    const propcloned = undefinedIsString(cloneObject(property));

    // if validator is active add object hash
    if(this.subFormDuplicates)
    {
      for(const subFormDuplicate of this.subFormDuplicates)
      {
        subFormDuplicate.hashes[hashObjectValue(property, subFormDuplicate.keys.map(sfd => sfd.path))] = true;
        this.updateDuplicateValidity();
      }
    }


    this.formControl.setValue([...this.formControl.value, propcloned]);

    
    this.addMaxValidity = (this._templateArray?.validator?.max) === undefined || (<any[]>this.formControl.value).length < this._templateArray.validator.max;
  }



  /**
   * update add validitiy
   */
  updateDuplicateValidity()
  {
    this.duplicationAddReasons = [];
    this.duplicationUpdateReasons = [];

    this.addValidity = this.subFormDuplicates.map(subFormDuplicate => 
    {
      const validity = testObjectKeyDuplicate(subFormDuplicate, this._subForm.templateValue);

      if(validity === false)
      {
        this.duplicationAddReasons.push(subFormDuplicate.keys.map(sfd => this.template.getLabel(sfd.item)))
      }


      return validity;
    }).reduce((a, b) => 
    {
      return a && b;
    }); 

    this.updateValidity = this.addValidity || this.subFormDuplicates.map(subFormDuplicate => 
    {
      const validity = hashObjectValue(this._subForm.templateValue, subFormDuplicate.keys.map(sfd => sfd.path)) === this.selectedHash 

      if(validity === false)
      {
        this.duplicationUpdateReasons.push(subFormDuplicate.keys.map(sfd => this.template.getLabel(sfd.item)))
      }

      return validity; 
    }).reduce((a, b) =>
    {
      return a && b;
    });


  }


  /**
   * remove a property at the i index
   * @param i 
   */
  removeElement(i)
  {
    this.controlledChange = true;

    const removed = (<any[]>this.formControl.value).splice(i, 1)[0];

    
    // if validator is active update hashes
    if(this.subFormDuplicates)
    {
      for(const subFormDuplicate of this.subFormDuplicates)
      {
        subFormDuplicate.hashes[hashObjectValue(removed, subFormDuplicate.keys.map(sfd => sfd.path))] = false;
      }

      this.updateDuplicateValidity();
    }
    
    
    this.formControl.updateValueAndValidity();
    
    if(this.selected < this.formControl.value.length)
    {
      this._subForm.setValue(this.formControl.value[this.selected]);
    }
    else
    {
      this.selected = undefined;
    }
    this.addMaxValidity = (this._templateArray?.validator?.max) === undefined || (<any[]>this.formControl.value).length < this._templateArray.validator.max;
  }


  /**
   * array label getter
   */
  get label(): string
  {
    return this.template.getLabel(this._templateArray);
  }


  onSubForm(form: FormGroupTemplate)
  {
    this._subForm = form;
    this._subForm.valueChanges.subscribe(v=>
    {
      if(this.subFormDuplicates)
      {
        setTimeout(() => {
          this.updateDuplicateValidity();
        });
      }
    })
    this.inputSyncronizer.sync('subForm');

  }


  addCurrentElement()
  {
    this.selected = undefined;
    this.addElement(this._subForm.templateValue);
  }


  updateSelected()
  {
    this.controlledChange = true;

    const newV = undefinedIsString(cloneObject(this._subForm.templateValue));

    if(this.subFormDuplicates)
    {
      const oldV = this.formControl.value[this.selected];

      for(const subFormDuplicate of this.subFormDuplicates)
      {
        subFormDuplicate.hashes[hashObjectValue(oldV, subFormDuplicate.keys.map(sfd => sfd.path))] = false;
        subFormDuplicate.hashes[hashObjectValue(newV, subFormDuplicate.keys.map(sfd => sfd.path))] = true;
      }

      this.updateDuplicateValidity();
    }

    this.formControl.value[this.selected] = newV;
    this.formControl.updateValueAndValidity();
  }

  getValue(element: any, path: string[])
  {
    return getValueWithPath(element, path);
  }


  get subValid(): boolean
  {
    return this._subForm?.valid ?? false;
  }


  select(i, element)
  {
    this.selected = i;

    if(this.subFormDuplicates)
    {
      for(const subFormDuplicate of this.subFormDuplicates)
      {
        this.selectedHash = hashObjectValue(element, subFormDuplicate.keys.map(sfd => sfd.path));
      }
      this.updateDuplicateValidity();
    }

    this._subForm.setValue(element);
  }


  get formArrayValid(): boolean
  {
    return this._formControl?.valid ?? false;
  }


  toggleCollapse()
  {
    this.collapse = !this.collapse;
  }

  async mouseOverStillAdd(event: MouseEvent)
  {
    if(!this.subValid || !this.addValidity || !this.addMaxValidity)
    {
      let msg = 'Cannot add element:';

      if(!this.subValid)
      {
        msg += `\n - ${this.translate.instant('Formular is not valid')}`;
      }

      if(!this.addMaxValidity)
      {
        msg += `\n - ${this.translate.instant('Max number of element reached')}`;
      }

      if(!this.addValidity)
      {
        msg += `\n - ${this.translate.instant('Elements or combination of some elements have to be unique')}`

        // console.log(this.duplicationAddReason);

        for(const duplication of this.duplicationAddReasons)
        {
          if(duplication.length === 1)
          {
            msg += `\n    - ${duplication[0]} ${this.translate.instant('has to be unique')}`
          }
          else
          {
            msg += `\n    - ${duplication.reduce((p, c, i) => 
            {
              return i === 0 ? c : `${p}, ${c}`;
            })} ${this.translate.instant('combination has to be unique')}`
          }
        }
      }

      if(this.toolTipRef === undefined)
      {
        this.toolTipRef = await this.modal.instanciateModal(ToolTipComponent);

        this.toolTipRef.instance.baseCoo = {x: event.clientX, y: event.clientY};
        this.toolTipRef.instance.text = msg;

        this.toolTipRef.onDestroy(()=>
        {
          this.toolTipRef = undefined;
        })
      }
    }
  }


  async mouseOverStillUpdate(event: MouseEvent)
  {
    if(!this.subValid || !this.updateValidity)
    {
      let msg = 'Cannot add element:';

      if(!this.subValid)
      {
        msg += `\n - ${this.translate.instant('Formular is not valid')}`;
      }

      if(!this.updateValidity)
      {
        msg += `\n - ${this.translate.instant('Elements or combination of some elements have to be unique')}`

        for(const duplication of this.duplicationUpdateReasons)
        {
          if(duplication.length === 1)
          {
            msg += `\n    - ${duplication[0]} ${this.translate.instant('has to be unique')}`
          }
          else
          {
            msg += `\n    - ${duplication.reduce((p, c, i) => 
            {
              return i === 0 ? c : `${p}, ${c}`;
            })} ${this.translate.instant('combination has to be unique')}`
          }
        }
      }

      if(this.toolTipRef === undefined)
      {
        this.toolTipRef = await this.modal.instanciateModal(ToolTipComponent);

        this.toolTipRef.instance.baseCoo = {x: event.clientX, y: event.clientY};
        this.toolTipRef.instance.text = msg;

        this.toolTipRef.onDestroy(()=>
        {
          this.toolTipRef = undefined;
        })
      }
    }
  }

  getStringFromValue(value) {
    if(typeof value === 'object' && value !== null && !Array.isArray(value))
    {
      if('ip' in value && 'name' in value) //Is Interfaces Object
      {
        return `${value.name}
       ${value.ip}`;
      }
      else
      {
        return Object.values(value);
      }
    }
    else
    {
      return value;
    }
  }


  onClickOut()
  {
    if(this.modal.topOfStackInstance.modalId <= this.defaultTopStackInstanceId)
    {
      this.selected = undefined;
    }
  }
}
