/* eslint-disable */
import { AfterViewInit, Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ControlValueAccessor } from '@ngneat/reactive-forms';

@Component({
  selector: 'forms-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR, 
      useExisting: 
      forwardRef(() => SearchInputComponent),
      multi: true,
    },
  ]
})
export class SearchInputComponent implements AfterViewInit, ControlValueAccessor
{
  @Input() working: boolean = false;


  @Output() submit: EventEmitter<string> = new EventEmitter<string>();

  @Input() debounceTimout: number = 100;

  @Output() affine: EventEmitter<string> = new EventEmitter<string>();

  // text property
  text: string = '';

  // disabled boolean
  disabled: boolean = false;

  debounceTimoutref


  constructor() { }




  /**
   ** emit a submit event when user press enter
   * @param event 
   */
  onKeyDown(event: KeyboardEvent)
  {
    if(event.key === 'Enter' && !this.working)
    {
      this.submit.emit(this.text);
    }
  }


  /**
   ** emit a affine event when user hasn't pressed a key in more than x ms
   * @param event 
   */
  onKeyUp()
  {
    if(this.debounceTimoutref !== undefined)
    {
      clearTimeout(this.debounceTimoutref);
    }

    this.debounceTimoutref = setTimeout(() => 
    {
      this.debounceTimoutref = undefined; 
      this.affine.emit(this.text);
    }, this.debounceTimout);
  }
 

  /**
   * Emit when user click
   */
  onSearchClick()
  {
    if(!this.working)
    {
      this.submit.emit(this.text);
    }
  }



  ngAfterViewInit(): void { }


  /**
   * ControlValueAccessor interface
   *  
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */ 


  // Function to call when text change 
  onChange = (text: string) => {};
  // Function to call when the input is touched 
  onTouched = () => {};

  writeValue(text: string): void
  {
    this.text = text;
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }
}
