/* eslint-disable */
import { ChangeDetectorRef, Component, ElementRef, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@ngneat/reactive-forms';
import { Syncronizer } from '@nororljos/nrs-util';
import { TMessage, TGroup } from '../../models/template';
import { TemplateService } from '../../services/template.service';


@Component({
  selector: 'forms-template-group',
  templateUrl: './template-group.component.html',
  styleUrls: ['./template-group.component.scss'],
  // viewProviders: [{ provide: TemplateFormComponent, useExisting: FormGroupDirective }]
})
export class TemplateGroupComponent implements OnInit 
{
  readonly title = [1.3, 1, 0.9, 0.8, 0.7];


  inputSyncronizer: Syncronizer = new Syncronizer(2);

  @Input()
  set message(msg: TMessage)
  {
    if(msg !== undefined && msg.type !== undefined)
    {
      switch(msg.type)
      {
        case 'enable':
          if(this._templateGroup.useParent)
          {
            this._message = msg;
          }
          else
          {
            if(!msg.value)
            {
              setTimeout(() => {
                this._formGroup.disable();
              });
            }
            else
            {
              this._formGroup.enable();
            }
          }
          break;
        default:
          this._message = msg;
          break;
      }
    }
  }

  get message():TMessage
  {
    return this._message;
  }

  _message: TMessage;
  
  @Input() 
  set templateGroup(tg: TGroup)
  {
    if(tg !== undefined)
    {
      this._templateGroup = tg;
      
      const title = this.title[tg?.title];

      if(title !== undefined)
      {
        this.elRef.nativeElement.style.setProperty('--label-fz', `${title}em`)
      }
      else
      {
        this.elRef.nativeElement.style.setProperty('--label-fz', `${1}em`)
      }

      if(tg?.collapsed !== undefined && typeof tg.collapsed === 'boolean')
      {
        this.collapse = tg.collapsed;
      }

      if(tg?.noMargin !== undefined && typeof tg.noMargin === 'boolean')
      {
        this.noMargin = tg.noMargin;
      }

      this.inputSyncronizer.sync('tg');
    }
  }

  get templateGroup(): TGroup
  {
    return this._templateGroup;
  }
  
  _templateGroup: TGroup;


  @Input()
  set parentFormGroup(formGroup: FormGroup)
  {
    if(formGroup !== undefined)
    {
      this._parentFormGroup = formGroup;
      this.inputSyncronizer.sync('pfg');

      this.parentFormGroup.valueChanges.subscribe(() =>
      {
        this.changeDetectorRef.detectChanges();
      });
    }
  }

  get parentFormGroup(): FormGroup
  {
    return this._parentFormGroup;
  }


  get formGroup(): FormGroup
  {
    return this._formGroup;
  }

  _parentFormGroup: FormGroup = undefined;

  _formGroup: FormGroup = undefined;


  collapse: boolean = false;

  noMargin: boolean = false;


  constructor(private fb: FormBuilder, private template: TemplateService, private elRef: ElementRef<HTMLElement>, private changeDetectorRef: ChangeDetectorRef) 
  {
    this.inputSyncronizer.onSyncEvent.subscribe(()=>
    {
      // don't nest id useParent is active
      if(this.templateGroup?.useParent)
      {
        this._formGroup = this._parentFormGroup;
      }
      else
      {
        this._formGroup = this.fb.group({});
        setTimeout(() => {
          this._parentFormGroup.addControl(this.templateGroup.name, this._formGroup);
          this.inputSyncronizer.reset();
        }, 0);
      }
    });
  }

  ngOnInit(): void {}


  get groupLabel(): string
  {
    return this.template.getLabel(this.templateGroup)
  }

  toggleCollapse()
  {
    this.collapse = !this.collapse;
  }


  get hidden(): boolean
  {
    return this._templateGroup?.hidden !== undefined ? this._templateGroup.hidden : false; 
  }
}
