/* eslint-disable */
import { Component, ElementRef, forwardRef, Input, OnInit, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { extractNumber } from '@nororljos/nrs-util';

@Component({
  selector: 'forms-ip-input',
  templateUrl: './ip-input.component.html',
  styleUrls: ['./ip-input.component.scss'],
  providers: [
    { 
      provide: NG_VALUE_ACCESSOR, 
      useExisting: 
      forwardRef(() => IpInputComponent),
      multi: true,
    },
  ]
})
export class IpInputComponent implements OnInit, ControlValueAccessor
{

  @ViewChild('byte7') 
  set byte7(element: ElementRef<HTMLSpanElement>)
  {
    if(element?.nativeElement !== undefined)
    {
      this.bytesElements[7] = element.nativeElement;
      if(this.bytes[7] !== undefined)
      {
        element.nativeElement.textContent = `${this.bytes[3]}`;
      }
    }
  }
  
  @ViewChild('byte6') 
  set byte6(element: ElementRef<HTMLSpanElement>)
  {
    if(element?.nativeElement !== undefined)
    {
      this.bytesElements[6] = element.nativeElement;
      if(this.bytes[6] !== undefined)
      {
        element.nativeElement.textContent = `${this.bytes[2]}`;
      }
    }
  }
  
  @ViewChild('byte5') 
  set byte5(element: ElementRef<HTMLSpanElement>)
  {
    if(element?.nativeElement !== undefined)
    {
      this.bytesElements[5] = element.nativeElement;
      if(this.bytes[5] !== undefined)
      {
        element.nativeElement.textContent = `${this.bytes[1]}`;
      }
    }
  }
  
  @ViewChild('byte4') 
  set byte4(element: ElementRef<HTMLSpanElement>)
  {
    if(element?.nativeElement !== undefined)
    {
      this.bytesElements[4] = element.nativeElement;
      if(this.bytes[4] !== undefined)
      {
        element.nativeElement.textContent = `${this.bytes[0]}`;
      }
    }
  }

  @ViewChild('byte3') 
  set byte3(element: ElementRef<HTMLSpanElement>)
  {
    if(element.nativeElement !== undefined)
    {
      this.bytesElements[3] = element.nativeElement;
      if(this.bytes[3] !== undefined)
      {
        element.nativeElement.textContent = `${this.bytes[3]}`;
      }
    }
  }
  
  @ViewChild('byte2') 
  set byte2(element: ElementRef<HTMLSpanElement>)
  {
    if(element.nativeElement !== undefined)
    {
      this.bytesElements[2] = element.nativeElement;
      if(this.bytes[2] !== undefined)
      {
        element.nativeElement.textContent = `${this.bytes[2]}`;
      }
    }
  }
  
  @ViewChild('byte1') 
  set byte1(element: ElementRef<HTMLSpanElement>)
  {
    if(element.nativeElement !== undefined)
    {
      this.bytesElements[1] = element.nativeElement;
      if(this.bytes[1] !== undefined)
      {
        element.nativeElement.textContent = `${this.bytes[1]}`;
      }
    }
  }
  
  @ViewChild('byte0') 
  set byte0(element: ElementRef<HTMLSpanElement>)
  {
    if(element.nativeElement !== undefined)
    {
      this.bytesElements[0] = element.nativeElement;
      if(this.bytes[0] !== undefined)
      {
        element.nativeElement.textContent = `${this.bytes[0]}`;
      }
    }
  }

  @ViewChild('portEl') 
  set portElements(element: ElementRef<HTMLSpanElement>)
  {
    if(element.nativeElement !== undefined)
    {
      this._portElement = element.nativeElement;
      if(this.portValue !== undefined)
      {
        element.nativeElement.textContent = `${this.portValue}`;
      }
    }
  }

  _portElement: HTMLSpanElement;

  get portElement(): HTMLSpanElement
  {
    return this._portElement;
  }

  @Input()
  ipType: 'any' | 'multicast' | 'no-multicast' = 'any';

  @Input()
  mode: 'ipv4' | 'ipv6' | 'both';

  @Input()
  port: boolean = false;

  @Input()
  onlyDisplay: boolean = false;

  bytesElements: HTMLSpanElement[] = [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined];


  bytes: number[] = [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined];

  portValue: number;

  isIpv4: boolean;

  // selected: boolean = false;

  constructor() 
  {    
  }

  ngOnInit(): void {
    this.isIpv4 = this.mode !== undefined ? this.mode !== 'ipv6' : true;
  }


  /**
   * OnkeyDown event
   * @param event 
   * @param byte 
   */
  onkeyDown(event: KeyboardEvent, byte: number)
  {
    if(!this.disabled)
    {
      if(event.key === 'Enter')
      {
        this.focusNextByte(byte);
        event.preventDefault();
      }
      else if(/(Backspace)|(ArrowL)/.test(event.key))
      {
        if((byte !== undefined 
          ? this.getCaretPosition(this.bytesElements[byte], this.isIpv4)[1] === 0
          : this.getCaretPosition(this.portElement, this.isIpv4)[1] === 0) || !this.isIpv4 && this.bytes[byte] === 0)
        {
          this.focusPreviousByte(byte);
          event.preventDefault();
        }
      }
      // arrow right
      else if(/ArrowR/.test(event.key))
      {
        // if caret not selection and lat char focus next
        if(byte !== undefined 
          ? this.getCaretPosition(this.bytesElements[byte], this.isIpv4)[0] === this.bytesElements[byte].textContent.length 
          : this.getCaretPosition(this.portElement, this.isIpv4)[0] === this.portElement.textContent.length)
        {
          this.focusNextByte(byte);
          event.preventDefault();
        }
      }
      // Special case point has to focus next exept if nothing has been typed
      else if(/\./.test(event.key))
      {
        event.preventDefault();
        const caretPos = this.getCaretPosition(this.bytesElements[byte], this.isIpv4);
        if(caretPos[0] === this.bytesElements[byte].textContent.length && caretPos[0] !== 0)
        {
          this.focusNextByte(byte);
        }
      }
      // do not allow anythin that is not in regExp (mostly any other char than number and not controls)
      else if(this.isIpv4 && !/([0-9])|(Backspace)|(Delete)|(Arrow)|(Tab)/.test(event.key))
      {
        event.preventDefault();
      }
      else if(!this.isIpv4)
      {
        if(byte !== undefined && !/([0-9]|[A-F]|[a-f])|(Backspace)|(Delete)|(Arrow)|(Tab)/.test(event.key))
          event.preventDefault();
        if(byte === undefined && !/([0-9])|(Backspace)|(Delete)|(Arrow)|(Tab)/.test(event.key))
          event.preventDefault();
      }
    }
    else
    {
      event.preventDefault();
    }
  }


  
  private focusNextByte(byte: number) 
  {
    if(byte !== undefined)
    {
      if (byte !== 0) 
      {
        this.bytesElements[byte - 1].focus();
      }
      else
      {
        if(this.port !== undefined)
        {
          this.portElement.focus();
        }
      }
    }
  }


  private focusPreviousByte(byte: number) 
  {
    if(byte === undefined)
    {
      this.bytesElements[0].focus();
    }
    else if (this.isIpv4 ? byte !== 3 :  byte !== 7) 
    {
      this.bytesElements[byte + 1].focus();
    }
  }

  private FourDigitsPad(n: number): string
  {
    if(n !== undefined) 
    {
      let zeros = '';
    
      if(n < 1000)
      {
        if(n < 100)
        {
          if(n < 10)
          {
            zeros += "0";
          }
          zeros += "0";
        }
        zeros += "0";
      }
      return zeros + n.toString(16);
    }
  }


  onInput(byte)
  {
    if(byte !== undefined)
    {
      const element = this.bytesElements[byte];
      
      let value = this.isIpv4 ? Number(element.textContent) : Number("0x" + element.textContent);

      // force 0 in input
      if(element.textContent === '')
      {
        value = -1;
      }
      
      let corrected = this.isIpv4 ? Math.max(0, Math.min(255, value)) : Math.max(0, Math.min(0xffff, value));
      
      if(value !== corrected)
      {
        value = corrected;
        this.bytesElements[byte].textContent = this.isIpv4 ? `${value}` :  `${this.FourDigitsPad(value)}`;
      }
      this.bytes[byte] = value;
      
      
      if(this.isIpv4 ? element.textContent.length === 3 : element.textContent.length === 4)
      {
        this.focusNextByte(byte);
      }
    }
    else
    {
      const element = this.portElement;
     
      let value = Number(element.textContent);

      let corrected = Math.max(0, Math.min(0xFFFF, value));
          
      if(value !== corrected)
      {
        value = corrected;
        this.portElement.textContent = `${value}`;
      }
      this.portValue = value;
    }
    this.declareChange();
  }

  onBlur()
  {
    this.onTouched();
  }

  onFocus(byte: number)
  {
    this.selectElementContents(byte === undefined ? this.portElement : this.bytesElements[byte]);
  }

  ipMode4()
  {
    if(!this.isIpv4 && this.mode === 'both') 
    {
      this.bytes = [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined]
      this.isIpv4 = true;
    }
  }

  ipMode6()
  {
    if(this.isIpv4 && this.mode === 'both')
    {
      this.bytes = [undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined]
      this.isIpv4 = false;
    }
  }


  /**
   * select content of a content editable
   * https://stackoverflow.com/questions/6139107/programmatically-select-text-in-a-contenteditable-html-element
   * @param el 
   */
  selectElementContents(el) 
  {
    const range = document.createRange();
    range.selectNodeContents(el);
    const sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  }

  declareChange()
  {
    const ipadress = this.isIpv4 ? `${this.bytes[3]}.${this.bytes[2]}.${this.bytes[1]}.${this.bytes[0]}${this.port ? `:${this.portValue}` : ``}`
    : `[${this.FourDigitsPad(this.bytes[7])}:${this.FourDigitsPad(this.bytes[6])}:${this.FourDigitsPad(this.bytes[5])}:${this.FourDigitsPad(this.bytes[4])}:${this.FourDigitsPad(this.bytes[3])}:${this.FourDigitsPad(this.bytes[2])}:${this.FourDigitsPad(this.bytes[1])}:${this.FourDigitsPad(this.bytes[0])}]${this.port ? `:${this.portValue}` : ``}` ;
    this.onChange(ipadress);
  }

  /**
   * Caret management
   *  
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */ 
  getCaretPosition(elem, isIpv4: boolean)
  {
    var sel: any = window.getSelection();
    var cum_length = [0, 0];

    if (sel.anchorNode == elem)
      cum_length = [sel.anchorOffset, sel.extentOffset];
    else
    {
      var nodes_to_find = [sel.anchorNode, sel.extentNode];
      if (!elem.contains(sel.anchorNode) || !elem.contains(sel.extentNode))
        return undefined;
      else
      {
        var found: any[] = [0, 0];
        var i;
        this.node_walk(elem, function (node)
        {
          for (i = 0; i < (isIpv4 ? 2 : 6); i++)
          {
            if (node == nodes_to_find[i])
            {
              found[i] = true;
              if (found[i == 0 ? 1 : 0])
                return false; // all done
            }
          }

          if (node.textContent && !node.firstChild)
          {
            for (i = 0; i < (isIpv4 ? 2 : 6); i++)
            {
              if (!found[i])
                cum_length[i] += node.textContent.length;
            }
          }
          return undefined;
        });
        cum_length[0] += sel.anchorOffset;
        cum_length[1] += sel.extentOffset;
      }
    }
    if (cum_length[0] <= cum_length[1])
      return cum_length;
    return [cum_length[1], cum_length[0]];
  }

  node_walk(node, func)
  {
    var result = func(node);
    for (node = node.firstChild; result !== false && node; node = node.nextSibling)
      result = this.node_walk(node, func);
    return result;
  };

  setCarretPosition(node, caret)
  {
    node.focus();
    let textNode = node.firstChild;
    let range = document.createRange();
    range.setStart(textNode, caret);
    range.setEnd(textNode, caret);
    let sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  }


  
  /**
   * ControlValueAccessor interface
   *  
   *       ||||
   *     __||||__
   *     \\\\////
   *      \\\///
   *       \\//
   *        \/
   */ 
  disabled: boolean = false;

  // Function to call when text change 
  onChange = (ip: string) => {};
  // Function to call when the input is touched 
  onTouched = () => {};

  writeValue(ip: string): void
  {
    if(this.isIpv4 && /^([0-2]?[0-9]?[0-9]\.){3}[0-2]?[0-9]?[0-9](:[0-6]?[0-9]{1,4})?$/g.test(ip))
    {
      let valid = true;
      const ipPort: any[] = ip.split(':');
      ipPort[0] = ipPort[0].split('.');

      

      for(const byte of ipPort[0])
      {
        const nbByte = Number(byte);
        const corrected = Math.max(0, Math.min(255, nbByte));

        if(nbByte !== corrected)
        {
          valid = false;
          break;
        }
      }

      if(ipPort.length === 2)
      {
        valid = valid && this.correctPortValue(ipPort[1]);
      }

      if(valid === true)
      {
        for(const [id, byte] of ipPort[0].entries())
        {
          if(id < 4) 
          {
            const byteId = 3 - id;
            this.bytes[byteId] = Number(byte);
            if(this.bytesElements[byteId] !== undefined)
            {
              this.bytesElements[byteId].textContent = byte;
            }
          }
        }
      }
    }
    else if(!this.isIpv4 && /\[(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))\](:[0-6]?[0-9]{1,4})?$/gi.test(ip))
    {
      let valid = true;
      const ipPort: string[] = ip.split(']');
      const ipS = ipPort[0].replace('[', '').split(':');
      const port = ipPort[1].replace(':', '');
      

      for(const byte of ipS)
      {
        const nbByte = Number(byte);
        const corrected = Math.max(0, Math.min(0xffff, nbByte));

        if(nbByte !== corrected)
        {
          valid = false;
          break;
        }
      }

      if(ipPort.length === 2)
      {
        valid = valid && this.correctPortValue(port);
      }

      if(valid === true)
      {
        for(const [id, byte] of ipS.entries())
        {
          const byteId = 7 - id;
          this.bytes[byteId] = Number(byte);
          if(this.bytesElements[byteId] !== undefined)
          {
            this.bytesElements[byteId].textContent = byte;
          }
        }
      }

    }
    else if(/^:[0-6]?[0-9]{1,4}$/g.test(ip) && this.port)
    {
      const nb = Math.max(Math.min(extractNumber(ip), 65353), 0);

      this.portValue = nb;
      if(this.portElement)
      {
        this.portElement.textContent = `${nb}`;
      }
    }
  }

  correctPortValue(ipPort)
  {
    const nb = Math.max(Math.min(Number(ipPort[1]), 65353), 0);
    const corrected = Math.max(0, Math.min(65353, nb));
    let valid = true;

    if(nb !== corrected)
    {
      valid = false;
    }
    else if(valid === true)
    {
      this.portValue = nb;
      if(this.portElement)
      {
        this.portElement.textContent = `${nb}`;
      }
    }
    return valid;
  }

  registerOnChange(fn: any): void
  {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void
  {
    this.onTouched = fn;
  }

  setDisabledState ? (isDisabled: boolean) : void
  {
    this.disabled = isDisabled;
  }
}
