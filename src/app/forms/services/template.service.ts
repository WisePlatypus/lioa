// /* eslint-disable no-case-declarations */
// /* eslint-disable @typescript-eslint/no-explicit-any */
// import { Injectable } from '@angular/core';
// import { ValidatorFn, Validators } from '@angular/forms';
// import { TranslateService } from '@ngx-translate/core';;
// import { partialDeepCompare } from '@nororljos/nrs-util';
// import { INPUT_TYPES, NAMABLE, ENCAPSULABLE, TElement, TItem, TInput, TInputText, TInputNumber, TNumberValidator, TInputIp, TInputDate, TInputDateRange, TArray, TTranslatable, TNamable, TCondition, TConditionText, TConditionNumber, TConditionBoolean, TDateValidator, TConditionDate, TIpValidator, TConditionIp, TDateRangeValidator, TConditionDateRange, TAnyValidator, TConditionAny, TGroup, TEncapsulable, isTemplateForm, isTNumberValidator, isTIpValidator, isTDateValidator, isTDateRangeValidator, TInputSelectionList } from '../models/template';
// import { validatorArray } from '../validators/array.directive';
// import { validatorBoolean } from '../validators/boolean.directive';
// import { validatorDateRange, validateDateRange } from '../validators/date-range.directive';
// import { validatorDate } from '../validators/date.directive';
// import { validatorIp, validateIp } from '../validators/ip.directive';
// import { validatorNumber, testNumber } from '../validators/number.directive';


// @Injectable({
//   providedIn: 'root'
// })
// export class TemplateService 
// {
  
//   inputType: {[key: string]: 'ip' | 'text' | 'number' | 'boolean' | 'boolean[]' | 'object' | 'date' | 'date[]' | 'any' | 'any[]'} = INPUT_TYPES;
//   nammable = NAMABLE;
//   encapsulable = ENCAPSULABLE;
  
//   constructor(private translate: TranslateService) 
//   {
//     window['nTemplateService'] = this;
//   }
  
//   getValidator(templateElement: TElement)
//   {
//     const validators: ValidatorFn[] = [];

//     if(templateElement.type === 'item' && (<TItem>templateElement).interface != undefined && (<TItem>templateElement).interface.type === 'input')
//     {

//       if((<TInput>(<TItem>templateElement).interface).required)
//       {
//         validators.push(Validators.required)
//       }

//       switch(this.inputType[(<TInput>(<TItem>templateElement).interface).input])
//       {
//         case 'text':
//           if((<TInputText>(<TItem>templateElement).interface).validator !== undefined)
//           {
//             validators.push(Validators.pattern((<TInputText>(<TItem>templateElement).interface).validator));
//           }
//           break;
//         case 'boolean':
//           if(<boolean>(<TInput>(<TItem>templateElement).interface).validator !== undefined)
//           {
//             validators.push(validatorBoolean(<boolean>(<TInput>(<TItem>templateElement).interface).validator));
//           }
//           break;
//         case 'number':
//           if((<TInputNumber>(<TItem>templateElement).interface).validator !== undefined)
//           {
//             validators.push(validatorNumber(<TNumberValidator>(<TInput>(<TItem>templateElement).interface).validator));
//           }
//           break;
//         case 'ip':
//           if((<TInputIp>(<TItem>templateElement).interface).validator !== undefined)
//           {
//             validators.push(validatorIp((<TInputIp>(<TItem>templateElement).interface).validator))
//           }
//           break;
//         case 'date':
//           if((<TInputDate>(<TItem>templateElement).interface).validator !== undefined)
//           {
//             validators.push(validatorDate((<TInputDate>(<TItem>templateElement).interface).validator))
//           }
//           break;
//         case 'date[]':
//           if((<TInputDateRange>(<TItem>templateElement).interface).validator !== undefined)
//           {
//             validators.push(validatorDateRange((<TInputDateRange>(<TItem>templateElement).interface).validator))
//           }
//           break;
//         case 'any[]':
//           if((<TInputDateRange>(<TItem>templateElement).interface).validator !== undefined)
//           {
//             validators.push(validatorArray((<TInputSelectionList>(<TItem>templateElement).interface).validator));
//           }
//           break;
//       }
//     }
//     else if(templateElement.type === 'array' && (<TArray>templateElement).validator !== undefined)
//     {
//       validators.push(validatorArray((<TArray>templateElement).validator));
//     }
    
//     return validators;
//   }

//   /**
//    * get traduction of a traductible element
//    * return a string if the object given was a string
//    * @param translatable 
//    * @returns 
//    */
//   getTranslation(translatable: string | TTranslatable): string
//   {
//     return translatable !== undefined ? 
//       typeof translatable === 'string' ? 
//         translatable 
//         : translatable[this.translate.currentLang] !== undefined ?
//           translatable[this.translate.currentLang] 
//           : translatable['en'] !== undefined ?
//             translatable['en'] :
//             translatable[Object.keys(translatable)[0]] 
//       : 'invalid';
//   }

//   /**
//    * return label of a nammable
//    * @param namable 
//    * @returns 
//    */
//   getLabel(namable: TNamable): string
//   {
//     return namable.label !== undefined ? this.getTranslation(namable.label) : namable.name
//   }


//   /**
//    * Test a condition on an ITEM
//    * return true is condition is fullfilled
//    * False otherwise or if  the condition cannot be fullfiled
//    * @param condition 
//    * @param inputType
//    * @param value 
//    * @returns boolean
//    */
//   testCondition(condition: TCondition, inputType: string, value: any): boolean
//   {
//     if(value !== undefined)
//     {   
//       // validity variable
//       let valid = true;

//       if(isTNumberValidator(condition.validator) && typeof value === 'number')
//       {
//         const nbValidator: TNumberValidator = (<TConditionNumber>condition).validator;  
//         // console.log(condition, nbValidator.ranges, value)        
//         // check if value is int if validator is active
//         if(nbValidator?.isInt)
//         {
//           valid = valid && (value - Math.floor(value)) === 0;
//         }
//         // if there are ranges
//         if(nbValidator.ranges)
//         {
//           // valid  is a logic AND between current validity and range validity
//           valid = valid && this.testRange(nbValidator.ranges, value);
//         }
//         // return validity
//         return valid;
//       }
//       else if(isTIpValidator(condition.validator) && typeof value === 'string')
//       {
//         const ipValidator: TIpValidator = (<TConditionIp>condition).validator;
//         // if there are ranges
//         if(ipValidator)
//         {
//           // valid  is a logic AND between current validity and range validity
//           valid = valid && validateIp(ipValidator, value);
//         }
//         return valid;
//       }
//       else if(isTDateValidator(condition.validator) && value instanceof Date)
//       {
//         const dateValidator: TDateValidator = (<TConditionDate>condition).validator;
//         // if there are ranges
//         if(dateValidator.ranges)
//         {
//           // valid  is a logic AND between current validity and range validity
//           valid = valid && this.testRange(dateValidator.ranges, (<Date>value).getTime());
//         }
//         return valid;
//       }
//       else if(isTDateRangeValidator(condition.validator) && Array.isArray(value) && value[0] instanceof Date)
//       {
//         if((<Date[]>value).length === 2 && value[0] !== undefined && value[1] !== undefined)
//         {
//           const dateRangeValidator: TDateRangeValidator = (<TConditionDateRange>condition).validator;

//           if(dateRangeValidator)
//           {
//             valid = valid && validateDateRange(dateRangeValidator, value)
//           }

//           return valid;
//         }
//         else
//         {
//           return false;
//         }
//       }
//       else if((typeof value === 'string' || value instanceof String) && (typeof condition.validator === 'string'))
//       {
//         const match = (<string>value).match((<TConditionText>condition).validator);
//         return match === null ? false : match.length > 0;
//       }
//       else if(typeof value === 'boolean' || value instanceof Boolean)
//       {
//         return (<boolean>value) === (<TConditionBoolean>condition).validator;
//       }
//       else
//       {
//         const anyValidator: TAnyValidator = (<TConditionAny>condition).validator;

//             valid = true;

//             if(anyValidator.include)
//             {
//               valid = false;
//               for(const include of anyValidator.include)
//               {
//                 //console.log('partial compare',include, value, partialDeepCompare(include, value))
//                 valid = valid || partialDeepCompare(include, value);
//               }
//             }
//             if(anyValidator.exclude)
//             {

//               for(const exclude of anyValidator.exclude)
//               {
//                 valid = valid && !partialDeepCompare(exclude, value);
//               }
//             }
//             return valid;
//       }      
//     }
//     return false;
//   }

//   /**
//    * test a comparable value against a range object
//    * @param ranges 
//    * @param value 
//    * @returns 
//    */
//   private testRange(ranges: {min?: number,max?: number,inside?: boolean}[], value: number) 
//   {
//     return testNumber(ranges, value);
//   }

//   /**
//    * Recusrive function to find a nammable element possibly nested in an Template "node"
//    * @param tElement
//    * @param name 
//    * @param path 
//    * @param templatePath 
//    * @returns {namable: TNamable, path: string[], templatePath: (number | string)[]} | undefined
//    */
//   findNammable(tElement: TElement, name: string, path: string[] = [], templatePath: (number | string)[] = []): {namable: TNamable, path: string[], templatePath: (number | string)[]} | undefined
//   {
//     // if element is namable and has the right name
//     if(this.nammable[tElement.type] && (<TNamable>tElement).name === name)
//     {
//       // return all data imediatly
//       path.push(name)
//       return {namable: <TNamable>tElement, path: path, templatePath: path};
//     }
//     // else if element is encapsulable (has other elements encapsulable is the only one yet)
//     if(ENCAPSULABLE[tElement.type])
//     {
//       // if namable
//       if(this.nammable[tElement.type] && (tElement.type==='group' && !(<TGroup>tElement).useParent))
//       {
//         // push in path element name
//         path.push((<TNamable>tElement).name)
//       }
//       // push in path children path
//       templatePath.push('children');

//       // for every child
//       for(const [id, child] of (<TEncapsulable>tElement).children.entries())
//       {
//         // push id
//         templatePath.push(id);
//         // call recusrive
//         const res = this.findNammable(child, name, path, templatePath);
//         // if object was found return it imediatly
//         if(res !== undefined)
//         {
//           return res;
//         }
//         // else pop template path
//         templatePath.pop();
//       }
//       // pop children path
//       templatePath.pop();

//       // if has a name pop it from the path
//       if(this.nammable[tElement.type])
//       {
//         path.pop();
//       }
//     }
//     return undefined;
//   }



//   flatItems(tElement: TElement, items: TItem[] = [])
//   {
//     // if element is namable and has the right name
//     if(tElement?.type === 'item')
//     {
//       items.push(<TItem>tElement)
//       {
//         return items;
//       }
//     }
//     // else if element is encapsulable (has other elements encapsulable is the only one yet)
//     if(ENCAPSULABLE[tElement.type] && !NAMABLE[tElement.type])
//     {
//       // for every child
//       for(const child of (<TEncapsulable>tElement).children)
//       {
//         this.flatItems(child, items)
//       }
//     }
//     return items;
//   }


//   isTemplateValid(te: TElement[] | {children: TElement[]}) 
//   {
//     return isTemplateForm(te);
//   }

// }
