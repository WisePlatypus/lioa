import { Injectable, inject } from "@angular/core";
import { Firestore, addDoc, collection, getDocs, query, doc, updateDoc } from "@angular/fire/firestore";




@Injectable({
    providedIn: 'root'
})
export class ClientService 
{
    public firestore: Firestore = inject(Firestore)


    async getClients(): Promise<Client[]>
    {
        return (
            await getDocs(query(collection(this.firestore, 'clients')))
        ).docs.map((client) => { return { ...<Client>client.data(), _id: client.id } });
    }

    async createClient(client: Client) 
    {
        if (client._id)
        {
            delete (client._id)
        }
        const docRef = await addDoc(collection(this.firestore, 'clients'), client);
        console.log("Document written with ID: ", docRef.id);
    }

    async editClient(client: Client) 
    {
        const id = client._id;

        delete (client._id)

        return await updateDoc(doc(this.firestore, 'clients', id), <any>client);
    }
}

export interface Client
{
    adresse: {
        code: number,
        rue: string,
        ville: string,
    }
    nom: string,
    prenom: string,
    type: 'business' | 'privé',
    email: string,
    telephone: string,
    _id?: string,
}