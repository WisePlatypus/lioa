import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MbscModule } from '@mobiscroll/angular';
import { TranslateModule } from '@ngx-translate/core';
import { NrsUtilModule } from '@nororljos/nrs-angular-util';
import { NrsDirectivesModule } from '@nororljos/nrs-directives';
import { NrsModalModule } from '@nororljos/nrs-modal';
import { NrsPipesModule } from '@nororljos/nrs-pipes';
import { createTranslateLoader } from '@nororljos/nrs-angular-util';
import { ButtonComponent } from './components/button/button.component';
import { CheckGroupInputComponent } from './components/check-group-input/check-group-input.component';
import { CheckInputComponent } from './components/check-input/check-input.component';
import { DateInputComponent } from './components/date-input/date-input.component';
import { DateRangeInputComponent } from './components/date-range-input/date-range-input.component';
import { DropDownListComponent } from './components/drop-down-list/drop-down-list.component';
import { EditableDropDownComponent } from './components/editable-drop-down/editable-drop-down.component';
import { IpInputComponent } from './components/ip-input/ip-input.component';
import { ListSelectionInputComponent } from './components/list-selection-input/list-selection-input.component';
import { MobiDateInputComponent } from './components/mobi-date-input/mobi-date-input.component';
import { MobiDateRangeInputComponent } from './components/mobi-date-range-input/mobi-date-range-input.component';
import { ModalDropDownComponent } from './components/modal-drop-down/modal-drop-down.component';
import { NbInputComponent } from './components/nb-input/nb-input.component';
import { RangeInputComponent } from './components/range-input/range-input.component';
import { SearchInputComponent } from './components/search-input/search-input.component';
import { SuggestDropDownComponent } from './components/suggest-drop-down/suggest-drop-down.component';
import { TemplateArrayComponent } from './components/template-array/template-array.component';
import { TemplateElementComponent } from './components/template-element/template-element.component';
import { TemplateFormComponent } from './components/template-form/template-form.component';
import { TemplateGroupComponent } from './components/template-group/template-group.component';
import { TemplateItemComponent } from './components/template-item/template-item.component';
import { TemplateLayoutComponent } from './components/template-layout/template-layout.component';
import { TextAreaComponent } from './components/text-area/text-area.component';
import { TextInputComponent } from './components/text-input/text-input.component';
import { TranslatablePipe } from './pipes/translatable.pipe';
import { ArrayDirective } from './validators/array.directive';
import { BooleanDirective } from './validators/boolean.directive';
import { DateRangeDirective } from './validators/date-range.directive';
import { DateDirective } from './validators/date.directive';
import { IpDirective } from './validators/ip.directive';
import { NumberDirective } from './validators/number.directive';
import { ToolTipComponent } from './components/tool-tip/tool-tip.component';
import { RegExpModalComponent } from './components/reg-exp-modal/reg-exp-modal.component';
import { TextDisplayComponent } from './components/text-display/text-display.component';
import { ListDisplayComponent } from './components/list-display/list-display.component';
import { ToggleInputComponent } from './components/toggle-input/toggle-input.component';

@NgModule({
  declarations: [
    TemplateFormComponent,
    TemplateElementComponent,
    TemplateGroupComponent,
    TemplateLayoutComponent,
    TemplateItemComponent,
    CheckInputComponent,
    RangeInputComponent,
    ButtonComponent,
    DateRangeInputComponent,
    DropDownListComponent,
    EditableDropDownComponent,
    NbInputComponent,
    TextInputComponent,
    IpInputComponent,
    TemplateArrayComponent,
    IpDirective,
    NumberDirective,
    DateDirective,
    DateRangeDirective,
    BooleanDirective,
    ArrayDirective,
    DateInputComponent,
    CheckGroupInputComponent,
    ModalDropDownComponent,
    SearchInputComponent,
    SuggestDropDownComponent,
    TextAreaComponent,
    ListSelectionInputComponent,
    MobiDateRangeInputComponent,
    MobiDateInputComponent,
    TranslatablePipe,
    ToolTipComponent,
    RegExpModalComponent,
    TextDisplayComponent,
    ListDisplayComponent,
    ToggleInputComponent,
  ],
  imports: [
    NrsDirectivesModule,
    NrsModalModule,
    NrsPipesModule,
    NrsUtilModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    MbscModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateModule,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    }),
  ],
  exports: [
    TemplateFormComponent,
    TemplateElementComponent,
    TemplateGroupComponent,
    TemplateLayoutComponent,
    TemplateItemComponent,
    CheckInputComponent,
    RangeInputComponent,
    ButtonComponent,
    DateRangeInputComponent,
    DateInputComponent,
    DropDownListComponent,
    EditableDropDownComponent,
    NbInputComponent,
    TextInputComponent,
    IpInputComponent,
    SearchInputComponent,
    SuggestDropDownComponent,
    TextAreaComponent,
    ListSelectionInputComponent,
    MobiDateRangeInputComponent,
    MobiDateInputComponent,
    TranslatablePipe,
    ToolTipComponent,
    TextDisplayComponent,
    RegExpModalComponent,
    ListDisplayComponent,
    ToggleInputComponent,
  ],
  providers: [],
})
export class NrsFormsModule {}
