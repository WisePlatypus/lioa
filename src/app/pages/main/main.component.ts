import { Component, inject } from '@angular/core';
import { Auth, signOut } from '@angular/fire/auth';

@Component({
  selector: 'app-main',
  standalone: false,
  templateUrl: './main.component.html',
  styleUrl: './main.component.scss'
})
export class MainComponent
{
  private auth = inject(Auth);

  links: {label: string, link: string}[] = [
    {
      label: 'Clients',
      link: 'clients'
    },
    {
      label: 'Commandes',
      link: 'commandes'
    },
  ]
  
  logout()
  {
    signOut(this.auth);
  }


}
