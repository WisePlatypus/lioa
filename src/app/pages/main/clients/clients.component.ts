import { Component, inject } from '@angular/core';
import { Client, ClientService } from '../../../forms/services/client.service';
import { ClientFormComponent } from '../../../components/client-form/client-form.component';

@Component({
  selector: 'app-clients',
  standalone: true,
  imports: [
    ClientFormComponent,
  ],
  templateUrl: './clients.component.html',
  styleUrl: './clients.component.scss'
})
export class ClientsComponent 
{
  private clientsServ = inject(ClientService);

  clients: Client[] = [];

  get selectedClient(): Client
  {
    return this.clients[this.selected];
  }

  selected: number = -1;

  async ngOnInit(): Promise<void>
  {
    this.clients = await this.clientsServ.getClients();
  }

  select(client: Client, index: number)
  {
    this.selected = index;
  }
}
