import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { MatButtonModule } from '@angular/material/button';
import { Route, RouterModule, RouterOutlet } from '@angular/router';
import { ClientsComponent } from './clients/clients.component';



@NgModule({
  declarations: [
    MainComponent,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: MainComponent,
        children: [
          {
            path: 'clients',
            loadComponent: () => import('./clients/clients.component').then(m => m.ClientsComponent),
          },
          {
            path: 'commandes',
            loadComponent: () => import('./commandes/commandes.component').then(m => m.CommandesComponent),
          },
          {
            // Do we want to display something upon login such as tutorial welcome etc?? depending on first login?
            path: '',
            redirectTo: 'clients',
            pathMatch: 'full',
          },
        ]
      }
    ]),
    RouterOutlet,
    MatButtonModule,
    CommonModule
  ]
})
export class MainModule { }
