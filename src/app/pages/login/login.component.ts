import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { LoginFormComponent } from '../../components/login/login-form/login-form.component';
import { AngularFireAuthModule } from "@angular/fire/compat/auth";
import { Auth, signInWithEmailAndPassword } from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    CommonModule,
    LoginFormComponent,
    AngularFireAuthModule,
],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent 
{
  private auth = inject(Auth);

  onSubmit(logininfo: any)
  {
    signInWithEmailAndPassword(this.auth, logininfo.username, logininfo.password);
  }
}
